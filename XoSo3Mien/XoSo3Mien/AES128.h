//
//  AES128.h
//  VNMART
//
//  Created by Nguyen Xuan Tien on 9/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AES128 : NSObject {
	
}
+ (void) initialize;
+ (void) setKey:(NSString*)key_;
+ (void) setIV:(NSString*)iv_;
+ (NSString *)AES128Encrypt:(NSString *)data;
+ (NSString *)AES128Decrypt:(NSString *)data;
@end
