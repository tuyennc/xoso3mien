//
//  AES128.m
//  VNMART
//
//  Created by Nguyen Xuan Tien on 9/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AES128.h"
#import <CommonCrypto/CommonCryptor.h>
#import "AppDelegate.h"
#import "Base64.h"
@implementation AES128

static NSString *key =@"SYOH9SK1RPC3J8S5";
static NSString *initVec = @"SYOH9SK1RPC3J8S5";
+ (void) initialize{
	[Base64 initialize];
}
+ (void) setKey:(NSString*)key_{
	key=key_;
}
+ (void) setIV:(NSString*)iv_{
	initVec=iv_;
}

NSString *result1111;
+ (NSString *)AES128Encrypt:(NSString *)data{
	
	const void *vplainText;
	size_t plainTextBufferSize;
	NSData *plainTextData = [data dataUsingEncoding: NSUTF8StringEncoding]; 
	plainTextBufferSize = [plainTextData length]; 
	vplainText = [plainTextData bytes];
	uint8_t *bufferPtr = NULL;
	size_t bufferPtrSize = 0;
	size_t movedBytes = 0;
	bufferPtrSize =(plainTextBufferSize + kCCBlockSizeAES128);
	bufferPtr = malloc( bufferPtrSize * sizeof(uint8_t));
	memset((void *)bufferPtr, 0x0, bufferPtrSize);
	
	const void *vkey = (const void *)[key UTF8String];
	const void *vinitVec = (const void *)[initVec UTF8String];
	
	CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt,
										  kCCAlgorithmAES128,
										  kCCOptionPKCS7Padding ,
										  vkey, 
										  kCCKeySizeAES128,
										  vinitVec, 
										  vplainText,
										  plainTextBufferSize,
										  (void *)bufferPtr,
										  plainTextBufferSize+kCCBlockSizeAES128,
										  &movedBytes);
	
	if (cryptStatus== kCCSuccess)
	{
		result1111=[Base64 encode:(const void *)bufferPtr length:(NSUInteger)movedBytes];
        free(bufferPtr);
        
	}else{
        result1111 =@"False";
        free(bufferPtr);
    }
	return result1111;
}


NSData *myData;
+ (NSString *)AES128Decrypt:(NSString *)data{
	const void *vplainText;
	size_t plainTextBufferSize;	
	NSData *plainTextData =[Base64 decode:data];
	
	plainTextBufferSize = [plainTextData length]; 
	vplainText = [plainTextData bytes];
	uint8_t *bufferPtr = NULL;
	size_t bufferPtrSize = 0;
	size_t movedBytes = 0;
	bufferPtrSize =(plainTextBufferSize + kCCBlockSizeAES128);
	bufferPtr = malloc( bufferPtrSize * sizeof(uint8_t));
	memset((void *)bufferPtr, 0x0, bufferPtrSize);
	const void *vkey = (const void *)[key UTF8String];
	const void *vinitVec = (const void *)[initVec UTF8String];
	
	CCCryptorStatus cryptStatus = CCCrypt(kCCDecrypt,
										  kCCAlgorithmAES128,
										  kCCOptionPKCS7Padding ,
										  vkey, 
										  kCCKeySizeAES128,
										  vinitVec, 
										  vplainText,
										  plainTextBufferSize,//[plainText length],
										  (void *)bufferPtr,
										  plainTextBufferSize+kCCBlockSizeAES128,//[plainText length]+kCCBlockSizeAES128,
										  &movedBytes);
	
	
	if (cryptStatus == kCCSuccess)
	{		
		myData = [NSData dataWithBytes:(const void *)bufferPtr length:(NSUInteger)movedBytes];		
		result1111=[[NSString alloc] initWithData:[NSData dataWithBytes:(const void *)bufferPtr length:(NSUInteger)movedBytes] encoding:NSUTF8StringEncoding];
        free(bufferPtr);
	}else{
        result1111=@"False";
        free(bufferPtr);
    }
	
	return result1111;
}
@end
