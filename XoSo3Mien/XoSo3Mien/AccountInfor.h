//
//  AccountInfor.h
//  XoSo3Mien
//
//  Created by Tuyen on 1/29/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol AccountInforDelegate<NSObject>
@optional

-(void)closePopUp;

@end

@interface AccountInfor : UIViewController
@property (strong, nonatomic) id<AccountInforDelegate> delegate;
@property (strong, nonatomic) IBOutlet UITextView *tvInfor;
@property (strong, nonatomic) IBOutlet UIButton *btnDangXuat;
@property (strong, nonatomic) IBOutlet UILabel *lblTittle;

@end
