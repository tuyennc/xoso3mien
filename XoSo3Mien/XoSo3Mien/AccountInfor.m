//
//  AccountInfor.m
//  XoSo3Mien
//
//  Created by Tuyen on 1/29/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import "AccountInfor.h"

@interface AccountInfor ()

@end

@implementation AccountInfor
@synthesize delegate = _delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (IBAction)logOUt:(id)sender {
    NSUserDefaults *df =[NSUserDefaults standardUserDefaults];
    [df setObject:@"0" forKey:@"USER_INFOR"];
    [df synchronize];
    [[AppDelegate sharedDelegate] setIsLogin:NO];
    if (_delegate && [_delegate respondsToSelector:@selector(closePopUp)]) {
        [_delegate closePopUp];
    }
     [[NSNotificationCenter defaultCenter] postNotificationName:@"UPDATE_IMAGE_LOGIN" object:self];
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    
    _btnDangXuat.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:17];
       _lblTittle.font = [UIFont fontWithName:XSFontCondensed size:17];
    _btnDangXuat.layer.cornerRadius = 7;
    
    _tvInfor.font = [UIFont fontWithName:XSFontCondensed size:17];
    NSUserDefaults *df = [NSUserDefaults standardUserDefaults];
    
    //            id:46045
    //            <br/>name:× L&ecirc; ViÇt Th¯ng
    //            <br/>email:vietthangif@gmail.com
    //            <br/>username:vietthangif
    //            <br/>mobile:841688676023
    //            <br/>gender:1
    //            <br/>birthday:12/07/1989
 
    NSString *strKQ =[df objectForKey:@"USER_INFOR"];
    NSArray *arrInfor = [strKQ componentsSeparatedByString:@"<br/>"];
    
//    strKQ = [strKQ stringByReplacingOccurrencesOfString:@"<br/>"
//                                         withString:@""];
//
    NSString *userNAme =    [[[arrInfor objectAtIndex:3] componentsSeparatedByString:@":"] objectAtIndex:1];
     NSString *email =    [[[arrInfor objectAtIndex:2] componentsSeparatedByString:@":"] objectAtIndex:1];
     NSString *sdt =    [[[arrInfor objectAtIndex:4] componentsSeparatedByString:@":"] objectAtIndex:1];
     NSString *ngaysinh =    [[[arrInfor objectAtIndex:6] componentsSeparatedByString:@":"] objectAtIndex:1];
    _tvInfor.text = [NSString stringWithFormat:@"Tên đăng nhâp : %@\n Email : %@\n Mobile : %@\nNgày Sinh :%@",userNAme,email,sdt,ngaysinh] ;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
