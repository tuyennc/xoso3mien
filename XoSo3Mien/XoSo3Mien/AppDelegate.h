//
//  AppDelegate.h
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) NSString *serverZ;
@property (nonatomic, strong) NSString *appID;
@property (nonatomic, assign) int portZ;
@property (nonatomic, assign)    MBProgressHUD *hudRequest;
@property (nonatomic, retain)  NSMutableDictionary *plistDict;
@property (nonatomic, assign)  BOOL isLogin;
@property (nonatomic, retain)  NSString *inforUser;
@property (nonatomic, retain)  NSString *numberToSend;
@property (nonatomic, retain) UIViewController *currentApp;
+ (AppDelegate*) sharedDelegate;
@end
