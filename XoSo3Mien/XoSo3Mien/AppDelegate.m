//
//  AppDelegate.m
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import "AppDelegate.h"
#import "StartUpScreen.h"
#import "QuaThuVC.h"

@implementation AppDelegate

@synthesize hudRequest = _hudRequest;
@synthesize plistDict = _plistDict;
@synthesize isLogin = _isLogin;
@synthesize inforUser = _inforUser;
@synthesize numberToSend = _numberToSend;

@synthesize currentApp = _currentApp;


#pragma public method
+ (AppDelegate*) sharedDelegate
{
    return (AppDelegate*)[[UIApplication sharedApplication] delegate];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
   
    
   
    
 
    
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
//        
////        [application setStatusBarStyle:UIStatusBarStyleLightContent];
//        
//        self.window.clipsToBounds =YES;
//        UIView *addStatusBar = [[UIView alloc] init];
//        addStatusBar.frame = CGRectMake(0, 0, 320, 20);
//        addStatusBar.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1]; //You can give your own color pattern
//        [self.window.rootViewController.view addSubview:addStatusBar];
//        
//        self.window.frame =  CGRectMake(0,20,self.window.frame.size.width,self.window.frame.size.height-20);
//    }

    NSUserDefaults *df =[NSUserDefaults standardUserDefaults];
    if ([df objectForKey:@"USER_INFOR"] == nil || [[df objectForKey:@"USER_INFOR"] isEqualToString:@"0"]) {
        _isLogin = NO;
    }
    else{
        _isLogin = YES;
    }
    
      NSString* pathSound = [[NSBundle mainBundle] pathForResource:@"Sound" ofType:@"plist"];
     _plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:pathSound];
//    NSLog(@"tuyen %@",[plistDict objectForKey:@"01"]) ;
    
    StartUpScreen * controller = [[StartUpScreen alloc] initWithNibName:@"StartUpScreen" bundle:nil];
//    QuaThuVC * controller = [[QuaThuVC alloc] initWithNibName:@"QuaThuVC" bundle:nil];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = controller;
    [self.window makeKeyAndVisible];
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
