//
//  BienDoView.h
//  XoSo3Mien
//
//  Created by Tuyen on 2/15/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BienDoView : UIView<UITableViewDataSource,UITableViewDelegate>

@property (retain, nonatomic) NSString  *maVungChon;
@property (retain, nonatomic) NSDictionary *dictKetQuaxS;
@property (strong, nonatomic) IBOutlet UITableView *tblKetQua;

@property (strong, nonatomic) IBOutlet UITextView *tfView;
@property (nonatomic,assign) int   vitri1;
@property (nonatomic,assign) int   vitri2;
@property (nonatomic,retain) NSString   *nameTinh;
@property (nonatomic,retain) NSString   *capso;
@property (nonatomic,assign) int   songay;
@end
