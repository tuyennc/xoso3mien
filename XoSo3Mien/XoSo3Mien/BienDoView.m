//
//  BienDoView.m
//  XoSo3Mien
//
//  Created by Tuyen on 2/15/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import "BienDoView.h"

@implementation BienDoView
{
      
}
@synthesize dictKetQuaxS = _dictKetQuaxS;
@synthesize vitri1 = _vitri1;
@synthesize vitri2 = _vitri2;
@synthesize capso = _capso;
@synthesize songay = _songay;
@synthesize nameTinh = _nameTinh;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
    }
    return self;
}
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    _tblKetQua.delegate = self;
    _tblKetQua.dataSource = self;
       if (IS_OS_7_OR_LATER) {
        [_tblKetQua setSeparatorInset:UIEdgeInsetsZero];
    }
    [_tblKetQua setSeparatorColor:[UIColor grayColor]];
    
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
#pragma mark
#pragma mark Tableview Delegate

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"";
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    [label setFont:[UIFont fontWithName:XSFontCondensed size:17]];
   
    
           [label setText:[NSString stringWithFormat:@"Kết Quả Xổ Số %@ - %@",_nameTinh, [_dictKetQuaxS objectForKey:@"date"]]];
    
    
    
    
    label.textAlignment = NSTextAlignmentCenter;
    
    [view addSubview:label];
    view.backgroundColor= RGB(216, 216, 216);
    return view;
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

        if ([_maVungChon isEqualToString:@"XSTD"]) {
            return 8;
        }
        else
            return 9;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
  
        if ([_maVungChon isEqualToString:@"XSTD"]) {
            if (indexPath.row == 3||indexPath.row == 5) {
                return  40;
            }
            else
                return 30;
        }
        else
        {
            if (indexPath.row == 4) {
                return  40;
            }
            else
                return 30;
        }
        
        
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor whiteColor];
    }
    [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
  
        float heithCell = 30;
        
        if ([_maVungChon isEqualToString:@"XSTD"]) {
            if (indexPath.row == 3||indexPath.row == 5) {
                heithCell = 40;
            }
            
        }
        else
        {
            if (indexPath.row == 4) {
                heithCell = 40;
            }
        }
        
        UILabel *lblName = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, heithCell)];
        lblName.textAlignment = NSTextAlignmentLeft;
        lblName.textColor = RGB(51, 51, 51);
        lblName.font = [UIFont fontWithName:XSFontCondensed size:17];
        
        
        UILabel *lblKetQua = [[UILabel alloc] initWithFrame:CGRectMake(80, 0, 320-110, heithCell)];
        lblKetQua.textAlignment = NSTextAlignmentCenter;
        lblKetQua.textColor = RGB(51, 51, 51);
        lblKetQua.numberOfLines = 2;
        lblKetQua.lineBreakMode = NSLineBreakByWordWrapping;
        lblKetQua.font =[UIFont fontWithName:XSFontCondensed size:17];
    int giai = 0;
    int giai2 = 0;
    if ([_maVungChon isEqualToString:@"XSTD"]) {

     giai = [ self detectGiaiMB:_vitri1];
     giai2 = [ self detectGiaiMB:_vitri2];
    }
    else
    {
        giai = [ self detectGiaiMC:_vitri1];
        giai2 = [ self detectGiaiMC:_vitri2];
 
    }
        switch (indexPath.row) {
            case 0:
            {
                lblName.text = @"   Giải đặc biệt";
                lblKetQua.text = [_dictKetQuaxS objectForKey:@"DB"];
                lblName.textColor = RGB(255, 66, 0);
                lblKetQua.textColor = RGB(255, 66, 0);
                if (indexPath.row ==giai ) {
                    [self toMau:_maVungChon withVT1:_vitri1  withLabel:lblKetQua];
                }
                if ( indexPath.row ==giai2) {
                    [self toMau:_maVungChon withVT2:_vitri2  withLabel:lblKetQua];
                }

               
            }
                break;
            case 1:
            {
                lblName.text = @"   Giải nhất";
                lblKetQua.text = [_dictKetQuaxS objectForKey:@"G1"];
                if (indexPath.row ==giai ) {
                    [self toMau:_maVungChon withVT1:_vitri1  withLabel:lblKetQua];
                }
                if ( indexPath.row ==giai2) {
                    [self toMau:_maVungChon withVT2:_vitri2  withLabel:lblKetQua];
                }

            }
                break;
            case 2:
            {
                lblName.text = @"   Giải nhì";
                lblKetQua.text = [_dictKetQuaxS objectForKey:@"G2"];
                if (indexPath.row ==giai ) {
                    [self toMau:_maVungChon withVT1:_vitri1  withLabel:lblKetQua];
                }
                if ( indexPath.row ==giai2) {
                    [self toMau:_maVungChon withVT2:_vitri2  withLabel:lblKetQua];
                }

            }
                break;
            case 3:
            {
                lblName.text = @"   Giải ba";
                lblKetQua.text = [_dictKetQuaxS objectForKey:@"G3"];
                if (indexPath.row ==giai ) {
                    [self toMau:_maVungChon withVT1:_vitri1  withLabel:lblKetQua];
                }
                if ( indexPath.row ==giai2) {
                    [self toMau:_maVungChon withVT2:_vitri2  withLabel:lblKetQua];
                }

            }
                break;
            case 4:
            {
                lblName.text = @"   Giải tư";
                lblKetQua.text = [_dictKetQuaxS objectForKey:@"G4"];
                if (indexPath.row ==giai ) {
                    [self toMau:_maVungChon withVT1:_vitri1  withLabel:lblKetQua];
                }
                if ( indexPath.row ==giai2) {
                    [self toMau:_maVungChon withVT2:_vitri2  withLabel:lblKetQua];
                }

            }
                break;
            case 5:
            {
                lblName.text = @"   Giải năm";
                lblKetQua.text = [_dictKetQuaxS objectForKey:@"G5"];
                if (indexPath.row ==giai ) {
                    [self toMau:_maVungChon withVT1:_vitri1  withLabel:lblKetQua];
                }
                if ( indexPath.row ==giai2) {
                    [self toMau:_maVungChon withVT2:_vitri2  withLabel:lblKetQua];
                }

            }
                break;
            case 6:
            {
                lblName.text = @"   Giải sáu";
                lblKetQua.text = [_dictKetQuaxS objectForKey:@"G6"];
                if (indexPath.row ==giai ) {
                    [self toMau:_maVungChon withVT1:_vitri1  withLabel:lblKetQua];
                }
                if ( indexPath.row ==giai2) {
                    [self toMau:_maVungChon withVT2:_vitri2  withLabel:lblKetQua];
                }

            }
                break;
            case 7:
            {
                lblName.text = @"   Giải bảy";
                lblKetQua.text = [_dictKetQuaxS objectForKey:@"G7"];
                if (indexPath.row ==giai ) {
                    [self toMau:_maVungChon withVT1:_vitri1  withLabel:lblKetQua];
                }
                if ( indexPath.row ==giai2) {
                    [self toMau:_maVungChon withVT2:_vitri2  withLabel:lblKetQua];
                }

            }
                break;
            case 8:
            {
                lblName.text = @"   Giải tám";
                lblKetQua.text = [_dictKetQuaxS objectForKey:@"G8"];
                if (indexPath.row ==giai ) {
                    [self toMau:_maVungChon withVT1:_vitri1  withLabel:lblKetQua];
                }
                if ( indexPath.row ==giai2) {
                    [self toMau:_maVungChon withVT2:_vitri2  withLabel:lblKetQua];
                }

            }
                break;
                
            default:
                break;
        }
    
   
    
    
        [cell.contentView addSubview:lblName];
        [cell.contentView addSubview:lblKetQua];
        
    
    
    return cell;
}

-(int)detectGiaiMB:(int)vitri
{
    int giai = 0;
    if (vitri<=4) {
        giai = 0;
    }
    else if (vitri<=9)
    {
        giai =1;
    }
    else if (vitri<=19)
    {
        giai =2;
    }
    else if (vitri<=49)
    {
        giai =3;
    }
    else if (vitri<=65)
    {
        giai =4;
    }
    else if (vitri<=89)
    {
        giai =5;
    }
    else if (vitri<=98)
    {
        giai =6;
    }
    else if (vitri<=106)
    {
        giai =7;
    }
    return giai;
}

-(void)toMauTungViTRi:(UILabel*)label withgiai:(int)giai withVt:(int)vt1
{
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: label.attributedText];
    switch (giai) {
        case 0://db
        {
            [text addAttribute: NSForegroundColorAttributeName value: [UIColor blueColor] range: NSMakeRange(vt1, 1)];
            
        }
            break;
        case 1:
        {
            int vtNew = vt1-5;
            
            
            [text addAttribute: NSForegroundColorAttributeName value: [UIColor blueColor] range: NSMakeRange(vtNew, 1)];
            
        }
            break;
        case 2:
        {
            int vtNew = vt1-10;
            if (vtNew<=4) {
                vtNew = vtNew;
            }
            else if(vtNew<=9) {
                vtNew = vtNew +1;
            }
            else if(vtNew<=14) {
                vtNew = vtNew +2;
            }
            
            [text addAttribute: NSForegroundColorAttributeName value: [UIColor blueColor] range: NSMakeRange(vtNew, 1)];
            
        }
            break;
        case 3:
        {
            int vtNew = vt1-20;
            if (vtNew<=4) {
                vtNew = vtNew;
            }
            else if(vtNew<=9) {
                vtNew = vtNew +1;
            }
            else if(vtNew<=14) {
                vtNew = vtNew +2;
            }
            else if(vtNew<=19) {
                vtNew = vtNew +3;
            }
            else if(vtNew<=24) {
                vtNew = vtNew +4;
            }
            else if(vtNew<=29) {
                vtNew = vtNew +5;
            }
            [text addAttribute: NSForegroundColorAttributeName value: [UIColor blueColor] range: NSMakeRange(vtNew, 1)];
            
        }
            break;
        case 4:
        {
            int vtNew = vt1-50;
            if (vtNew<=3) {
                vtNew = vtNew;
            }
            else if(vtNew<=7) {
                vtNew = vtNew +1;
            }
            else if(vtNew<=11) {
                vtNew = vtNew +2;
            }
            else if(vtNew<=15) {
                vtNew = vtNew +3;
            }
            [text addAttribute: NSForegroundColorAttributeName value: [UIColor blueColor] range: NSMakeRange(vtNew, 1)];
            
        }
            break;
        case 5:
        {
            int vtNew = vt1-66;
            if (vtNew<=3) {
                vtNew = vtNew;
            }
            else if(vtNew<=7) {
                vtNew = vtNew +1;
            }
            else if(vtNew<=11) {
                vtNew = vtNew +2;
            }
            else if(vtNew<=15) {
                vtNew = vtNew +3;
            }
            else if(vtNew<=19) {
                vtNew = vtNew +4;
            }
            else if(vtNew<=23) {
                vtNew = vtNew +5;
            }
            
            [text addAttribute: NSForegroundColorAttributeName value: [UIColor blueColor] range: NSMakeRange(vtNew, 1)];
            
        }
            break;
        case 6:
        {
            int vtNew = vt1-90;
            if (vtNew<=2) {
                vtNew = vtNew;
            }
            else if(vtNew<=5) {
                vtNew = vtNew +1;
            }
            else if(vtNew<=8) {
                vtNew = vtNew +2;
            }
            
            [text addAttribute: NSForegroundColorAttributeName value: [UIColor blueColor] range: NSMakeRange(vtNew, 1)];
            
        }
            break;
        case 7:
        {
            int vtNew = vt1-99;
            if (vtNew<=1) {
                vtNew = vtNew;
            }
            else if(vtNew<=3) {
                vtNew = vtNew +1;
            }
            else if(vtNew<=5) {
                vtNew = vtNew +2;
            }
            
            [text addAttribute: NSForegroundColorAttributeName value: [UIColor blueColor] range: NSMakeRange(vtNew, 1)];
            
        }
            break;
            
        default:
            break;
    }
    

    [label setAttributedText: text];

}

-(int)detectGiaiMC:(int)vitri
{
    int giai = 0;
    if (vitri<=5) {
        giai = 0;
    }
    else if (vitri<=10)
    {
        giai =1;
    }
    else if (vitri<=15)
    {
        giai =2;
    }
    else if (vitri<=25)
    {
        giai =3;
    }
    else if (vitri<=60)
    {
        giai =4;
    }
    else if (vitri<=64)
    {
        giai =5;
    }
    else if (vitri<=76)
    {
        giai =6;
    }
    else if (vitri<=79)
    {
        giai =7;
    }
    else if (vitri<=81)
    {
        giai =8;
    }
    return giai;
}
-(void)toMauTungViTRiMC:(UILabel*)label withgiai:(int)giai withVt:(int)vt1
{
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: label.attributedText];
    switch (giai) {
        case 0://db
        {
            [text addAttribute: NSForegroundColorAttributeName value: [UIColor blueColor] range: NSMakeRange(vt1, 1)];
            
        }
            break;
        case 1:
        {
            int vtNew = vt1-6;
            
            
            [text addAttribute: NSForegroundColorAttributeName value: [UIColor blueColor] range: NSMakeRange(vtNew, 1)];
            
        }
            break;
        case 2:
        {
            int vtNew = vt1-11;
            
            [text addAttribute: NSForegroundColorAttributeName value: [UIColor blueColor] range: NSMakeRange(vtNew, 1)];
            
        }
            break;
        case 3:
        {
            int vtNew = vt1-16;
            if (vtNew<=4) {
                vtNew = vtNew;
            }
            else if(vtNew<=9) {
                vtNew = vtNew +1;
            }
            
            [text addAttribute: NSForegroundColorAttributeName value: [UIColor blueColor] range: NSMakeRange(vtNew, 1)];
            
        }
            break;
        case 4:
        {
            int vtNew = vt1-26;
            if (vtNew<=4) {
                vtNew = vtNew;
            }
            else if(vtNew<=9) {
                vtNew = vtNew +1;
            }
            else if(vtNew<=14) {
                vtNew = vtNew +2;
            }
            else if(vtNew<=19) {
                vtNew = vtNew +3;
            }
            else if(vtNew<=24) {
                vtNew = vtNew +4;
            }
            else if(vtNew<=29) {
                vtNew = vtNew +5;
            }
            else if(vtNew<=34) {
                vtNew = vtNew +6;
            }
            [text addAttribute: NSForegroundColorAttributeName value: [UIColor blueColor] range: NSMakeRange(vtNew, 1)];
            
        }
            break;
        case 5:
        {
            int vtNew = vt1-61;
           
            
            [text addAttribute: NSForegroundColorAttributeName value: [UIColor blueColor] range: NSMakeRange(vtNew, 1)];
            
        }
            break;
        case 6:
        {
            int vtNew = vt1-65;
            if (vtNew<=3) {
                vtNew = vtNew;
            }
            else if(vtNew<=7) {
                vtNew = vtNew +1;
            }
            else if(vtNew<=10) {
                vtNew = vtNew +2;
            }
            
            [text addAttribute: NSForegroundColorAttributeName value: [UIColor blueColor] range: NSMakeRange(vtNew, 1)];
            
        }
            break;
        case 7:
        {
            int vtNew = vt1-77;
          
            
            [text addAttribute: NSForegroundColorAttributeName value: [UIColor blueColor] range: NSMakeRange(vtNew, 1)];
            
        }
            break;
        case 8:
        {
            int vtNew = vt1-80;
            
            
            [text addAttribute: NSForegroundColorAttributeName value: [UIColor blueColor] range: NSMakeRange(vtNew, 1)];
            
        }
            break;
            
        default:
            break;
    }
    
    
    [label setAttributedText: text];
    
}

-(void)toMau:(NSString*)maVung withVT1:(int)vt1  withLabel:(UILabel*)label
{
//    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: label.attributedText];
   
    if ([maVung isEqualToString:@"XSTD"]) {
        
        int giai = [ self detectGiaiMB:vt1];
    
        [self toMauTungViTRi:label withgiai:giai withVt:vt1];
        
    }
    else
    {
        int giai = [ self detectGiaiMC:vt1];
        
        [self toMauTungViTRiMC:label withgiai:giai withVt:vt1];

    }
//    [label setAttributedText: text];

}
-(void)toMau:(NSString*)maVung withVT2:(int)vt2  withLabel:(UILabel*)label
{

    
    if ([maVung isEqualToString:@"XSTD"]) {
        
     
        int giai2 = [ self detectGiaiMB:vt2];
     
        [self toMauTungViTRi:label withgiai:giai2 withVt:vt2];
        
        
        
        
    }
    else
    {
        int giai2 = [ self detectGiaiMC:vt2];
        
        [self toMauTungViTRiMC:label withgiai:giai2 withVt:vt2];
        
    }
    
}

@end
