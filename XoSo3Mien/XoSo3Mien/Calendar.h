//
//  Calendar.h
//  VietCalendar
//
//  Created by tuyennc on 22/11/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Calendar : NSObject
-(int)jdFromDate:(int) dd mm:(int)mm yy:(int)yy;
- (NSArray *) convertSolar2Lunar:(int) dd mm:(int) mm yy:(int) yy timeZone:(double) timeZone ;
- (NSArray *)convertLunar2Solar:(int) lunarDay lunarMonth:(int) lunarMonth  lunarYear:(int) lunarYear lunarLeap:(int) lunarLeap  timeZone:(double) timeZone ;
- (NSArray *)showCanChiWithDayLunar:(int)dayLunar monthLunar:(int)monthLunar yearLunar:(int)yearLunar nhuan:(int)nhuan;
- (NSString*)timeHoangDaoWithDaySolar:(int)day monthSolar:(int)month yearSolar:(int)year ;
- (int)monthNhuanYearLunar:(int)year;
- (int)numberOfDaysLunar:(int)month1  year:(int)year nhuan:(int)nhuan monthNhuan:(int)monthNhuan;
- (int)numberOfDaysLunar:(int)month1  year:(int)year nhuan:(int)nhuan ;
- (int)numberDaysOfMonthSodar:(int)month year:(int)year;
- (int)calculatorDayBadOrGoodWithDaySolar:(int)day monthSolar:(int)month yearSolar:(int)year;

- (NSString *)canChiHourWithSolar:(int)daySolar monthSolar:(int)monthSolar yearSolar:(int)yearSolar hour:(int)hour;
- (NSString *)getTruc:(int)idTruc;
- (NSMutableArray*) getImageCanChiWithLunarDate:(NSDate *) date andNhuan:(int) iNhuan;
- (int)numberSaoWithSolarDate:(NSDate *) aDate;
@end
