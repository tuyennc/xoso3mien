//
//  Calendar.m
//  VietCalendar
//
//  Created by tuyennc on 22/11/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Calendar.h"

@interface Calendar()

- (int) returnChi_ThangWithMonthLunar:(int) aMonth;
- (int) returnChi_HourWithHour:(int) aHour;

@end

@implementation Calendar

-(int) INT:(double) d {
    return (int)floor(d);
}
    // Hungpb Added
//- (int)numberSaoWithSolarDate:(NSDate *)aDate
//{
//    int jd2= [self  jdFromDate:aDate.day mm:aDate.month yy:aDate.year];
//    int sao = jd2%28 + 1;
//    switch (sao)
//    {
//        case 1:
//            return 12;
//            break;
//        case 2:
//            return 13;
//            break;
//        case 3:
//            return 14;
//            break;
//        case 4:
//            return 15;
//            break;
//        case 5:
//            return 16;
//            break;
//        case 6:
//            return 17;
//            break;
//        case 7:
//            return 18;
//            break;
//        case 8:
//            return 19;
//            break;
//        case 9:
//            return 20;
//            break;
//        case 10:
//            return 21;
//            break;
//        case 11:
//            return 22;
//            break;
//        case 12:
//            return 23;
//            break;
//        case 13:
//            return 24;
//            break;
//        case 14:
//            return 25;
//            break;
//        case 15:
//            return 26;
//            break;
//        case 16:
//            return 27;
//            break;
//        case 17:
//            return 28;
//            break;
//        case 18:
//            return 1;
//            break;
//        case 19:
//            return 2;
//            break;
//        case 20:
//            return 3;
//            break;
//        case 21:
//            return 4;
//            break;
//        case 22:
//            return 5;
//            break;
//        case 23:
//            return 6;
//            break;
//        case 24:
//            return 7;
//            break;
//        case 25:
//            return 8;
//            break;
//        case 26:
//            return 9;
//            break;
//        case 27:
//            return 10;
//            break;
//        case 28:
//            return 11;
//            break;
//            
//    }
//    return 1;
//    
//}

-(int)jdFromDate:(int) dd mm:(int)mm yy:(int)yy  {
    int a = (14 - mm) / 12;
    int y = yy+4800-a;
    int m = mm+12*a-3;
    int jd = dd + (153*m+2)/5 + 365*y + y/4 - y/100 + y/400 - 32045;
    if (jd < 2299161) {
        jd = dd + (153*m+2)/5 + 365*y + y/4 - 32083;
    }
    //jd = jd - 1721425;
    return jd;
}
-(NSArray *)jdToDate:(int)jd {
    int a, b, c;
    if (jd > 2299160) { // After 5/10/1582, Gregorian calendar
        a = jd + 32044;
        b = (4*a+3)/146097;
        c = a - (b*146097)/4;
    } else {
        b = 0;
        c = jd + 32082;
    }
    int d = (4*c+3)/1461;
    int e = c - (1461*d)/4;
    int m = (5*e+2)/153;
    int day = e - (153*m+2)/5 + 1;
    int month = m + 3 - 12*(m/10);
    int year = b*100 + d - 4800 + m/10;
    NSArray *arr = [[NSArray alloc] initWithObjects:[NSString stringWithFormat:@"%d",day],[NSString stringWithFormat:@"%d",month],[NSString stringWithFormat:@"%d",year],nil];
    //NSLog(@"ngay duong %@",arr);
    return arr;
}
-(double) SunLongitudeAA98:(double) jdn {
    double T = (jdn - 2451545.0 ) / 36525; // Time in Julian centuries from 2000-01-01 12:00:00 GMT
    double T2 = T*T;
    double dr = M_PI/180; // degree to radian
    double M = 357.52910 + 35999.05030*T - 0.0001559*T2 - 0.00000048*T*T2; // mean anomaly, degree
    double L0 = 280.46645 + 36000.76983*T + 0.0003032*T2; // mean longitude, degree
    double DL = (1.914600 - 0.004817*T - 0.000014*T2)*sin(dr*M);
    DL = DL + (0.019993 - 0.000101*T)*sin(dr*2*M) + 0.000290*sin(dr*3*M);
    double L = L0 + DL; // true longitude, degree
    L = L - 360*([self INT:(L/360)]); // Normalize to (0, 360)
    return L;
}
-(double) SunLongitude:(double)jdn {
    //return CC2K.sunLongitude(jdn);
    return [self SunLongitudeAA98:jdn];//SunLongitudeAA98(jdn);
}
-(double)NewMoonAA98:(int) k {
    double T = k/1236.85; // Time in Julian centuries from 1900 January 0.5
    double T2 = T * T;
    double T3 = T2 * T;
    double dr = M_PI/180;
    double Jd1 = 2415020.75933 + 29.53058868*k + 0.0001178*T2 - 0.000000155*T3;
    Jd1 = Jd1 + 0.00033*sin((166.56 + 132.87*T - 0.009173*T2)*dr); // Mean new moon
    double M = 359.2242 + 29.10535608*k - 0.0000333*T2 - 0.00000347*T3; // Sun's mean anomaly
    double Mpr = 306.0253 + 385.81691806*k + 0.0107306*T2 + 0.00001236*T3; // Moon's mean anomaly
    double F = 21.2964 + 390.67050646*k - 0.0016528*T2 - 0.00000239*T3; // Moon's argument of latitude
    double C1=(0.1734 - 0.000393*T)*sin(M*dr) + 0.0021*sin(2*dr*M);
    C1 = C1 - 0.4068*sin(Mpr*dr) + 0.0161*sin(dr*2*Mpr);
    C1 = C1 - 0.0004*sin(dr*3*Mpr);
    C1 = C1 + 0.0104*sin(dr*2*F) - 0.0051*sin(dr*(M+Mpr));
    C1 = C1 - 0.0074*sin(dr*(M-Mpr)) + 0.0004*sin(dr*(2*F+M));
    C1 = C1 - 0.0004*sin(dr*(2*F-M)) - 0.0006*sin(dr*(2*F+Mpr));
    C1 = C1 + 0.0010*sin(dr*(2*F-Mpr)) + 0.0005*sin(dr*(2*Mpr+M));
    double deltat;
    if (T < -11) {
        deltat= 0.001 + 0.000839*T + 0.0002261*T2 - 0.00000845*T3 - 0.000000081*T*T3;
    } else {
        deltat= -0.000278 + 0.000265*T + 0.000262*T2;
    };
    double JdNew = Jd1 + C1 - deltat;
    return JdNew;
}
-(double) NewMoon:(int)k {
    //return CC2K.newMoonTime(k);
    return [self NewMoonAA98:k];//NewMoonAA98(k);
}
-(double) getSunLongitude:(int)dayNumber timeZone:(double)timeZone {
    return [self SunLongitude:(dayNumber - 0.5 - timeZone/24)];//SunLongitude(dayNumber - 0.5 - timeZone/24);
}
-(int) getNewMoonDay:(int)k timeZone:(double)timeZone {
    double jd = [self NewMoon:k];//NewMoon(k);
    return [self INT:(jd + 0.5 + timeZone/24)];
}
-(int) getLunarMonth11:(int)yy timeZone:(double)timeZone {
    double off =[self jdFromDate:31 mm:12 yy:yy] -2415021.076998695 ; //jdFromDate(31, 12, yy) - 2415021.076998695;
    int k =[self INT:(off / 29.530588853)];
    int nm =[self getNewMoonDay:k timeZone:timeZone];// getNewMoonDay(k, timeZone);
    int sunLong = [self INT:([self getSunLongitude:nm timeZone:timeZone]/30)];
    if (sunLong >= 9) {
        nm = [self getNewMoonDay:k-1 timeZone:timeZone];// getNewMoonDay(k-1, timeZone);
    }
    return nm;
}
-(int) getLeapMonthOffset:(int) a11 timeZone:(double) timeZone {
    int k = [self INT:(0.5 + (a11 - 2415021.076998695) / 29.530588853)];
    int last; // Month 11 contains point of sun longutide 3*PI/2 (December solstice)
    int i = 1; // We start with the month following lunar month 11
    int arc = [self INT:([self getSunLongitude:[self getNewMoonDay:(k+i) timeZone:timeZone] timeZone:timeZone]/30)];//getSunLongitude(getNewMoonDay(k+i, timeZone), timeZone)/30)];
    do {
        last = arc;
        i++;
        arc =[self INT:([self getSunLongitude:[self getNewMoonDay:(k+i) timeZone:timeZone] timeZone:timeZone]/30)];  // INT(getSunLongitude(getNewMoonDay(k+i, timeZone), timeZone)/30);
    } while (arc != last && i < 14);
    return i-1;
}
-(int)monthNhuanYearLunar:(int)year{
    
   
   int balance = year%19;    
    if (balance==0 || balance==3 || balance==6 || balance== 9 ||balance == 11 || balance== 14 || balance == 17) {
        //if year is "nhuan"
        for (int j=1; j<13; j++) {        
            // int month = j;
            int existNhuan = 0;
            for (int week=0; week<6   ; week++) 
            { 
                for (int d=0; d<7; d++) 
                {                
                    NSArray *arr = [[NSArray alloc] init];
                    arr =[self convertSolar2Lunar:d mm:j yy:year timeZone:7];                
                    int m2 = [[arr objectAtIndex:1] intValue];   
                    int nhuan = [[arr objectAtIndex:3] intValue];  
                    if (nhuan==1) {
                        existNhuan ++;
                        return m2;                       
                        break;
                    } 
                }
                if (existNhuan>0) {
                    break;
                }
                
            }
            if (existNhuan>0) {
                break;
            }
        }
   }
   else         
        return 0;
    
    return 0;
    
}
- (int)numberDaysOfMonthSodar:(int)month year:(int)year{
    int banlanceYear = year % 4;
    if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
        return 31;
    }    
    else {
        if (month != 2) {
            return 30;
        }
        else {
            if (banlanceYear == 0) {
                return 29;
            }
            else {
                return 28;
            }
        }
    }        
    
}

-(int)numberOfDaysLunar:(int)month1  year:(int)year nhuan:(int)nhuan monthNhuan:(int)monthNhuan{
//    int monthNhuan = [self monthNhuanYearLunar:year];
//    int monthNhuan = 0;
//    NSArray *arrDateStart = [[NSArray alloc] init ];
    NSArray *arrEndStart ;
  
   NSArray *arrDateStart  = [self convertLunar2Solar:1 lunarMonth:month1 lunarYear:year lunarLeap:nhuan timeZone:7];
    int day1Solar = [[arrDateStart objectAtIndex:0] intValue];
    int month1lodar = [[arrDateStart objectAtIndex:1] intValue];
    int year1Solar = [[arrDateStart objectAtIndex:2] intValue];
  // NSLog(@"ngay 1 %d ,%d,%d",day1Solar,month1lodar,year1Solar);
    if (monthNhuan == 0) {
        arrEndStart =  [self convertLunar2Solar:1 lunarMonth:(month1+1) lunarYear:year lunarLeap:nhuan timeZone:7];
    }
    else {
        if (monthNhuan == month1 && nhuan == 0) {
            arrEndStart =  [self convertLunar2Solar:1 lunarMonth:month1 lunarYear:year lunarLeap:1 timeZone:7];
            
        }
        else {
//            if (monthNhuan == month1 && nhuan==1) {
                arrEndStart =  [self convertLunar2Solar:1 lunarMonth:(month1+1) lunarYear:year lunarLeap:0 timeZone:7];
//            }
//            else{
//                arrEndStart =  [self convertLunar2Solar:1 lunarMonth:(month1+1) lunarYear:year lunarLeap:0 timeZone:7];
//            }
        }
        
        }
    int day2Solar = [[arrEndStart objectAtIndex:0] intValue];
    int month2Solar = [[arrEndStart objectAtIndex:1] intValue];
    int year2Solar = [[arrEndStart objectAtIndex:2] intValue];
//    NSLog(@"ngay 2 %d ,%d,%d",day2Solar,month2Solar,year2Solar);
//    Distance between 2 days
    int sn = 0;
    if (year2Solar >= year1Solar)
    {
        if (day1Solar == day2Solar && month1lodar == month2Solar)
        {            
            for (int i = year1Solar; i < year2Solar; i++)
            {
                if ((i % 4 == 0 && i % 100 != 0) || i % 400 == 0)
                {
                    sn = sn + 366;
                }
                else if ((i % 4 != 0 && i % 100 == 0) || i % 400 != 0)
                {
                    sn = sn + 365;                    
                }
            }
        }
        
        else
        {
            if (day1Solar != day2Solar || month1lodar != month2Solar)
            {
                
                for (int i = year1Solar; i < year2Solar; i++)
                {
                    if ((i % 4 == 0 && i % 100 != 0) || i % 400 == 0)
                    {
                        sn = sn + 366;                      
                    }
                    else if ((i % 4 != 0 && i % 100 == 0) || i % 400 != 0)
                    {
                        sn = sn + 365;                        
                    }
                }
            }
            if (month1lodar >= month2Solar)
            {
                for (int i = month2Solar; i < month1lodar; i++)
                {                    
                    if (i == 2 && ((year1Solar % 4 == 0 && year1Solar % 100 != 0) || year1Solar % 400 == 0))
                    {
                        sn = sn - 29;
                    }
                    else if (i == 2 && ((year1Solar % 4 != 0 && year1Solar % 100 == 0) || year1Solar % 400 != 0))
                    {
                        sn = sn -28;                    }
                    
                    if ((i == 4 || i == 6 || i == 9 || i == 11 )&& i != 2)
                    {
                        sn = sn - 30;
                    }
                    else if (i != 2)
                        sn = sn - 31;  
                }
            }
            else
            {
                for (int i = month1lodar; i < month2Solar; i++)
                {                    
                    if (i == 2 && ((year1Solar % 4 == 0 && year1Solar % 100 != 0) || year1Solar % 400 == 0))
                    {
                        sn = sn + 29;
                    }
                    else if (i == 2 && ((year1Solar % 4 != 0 && year1Solar % 100 == 0) || year1Solar % 400 != 0))
                    {
                        sn = sn + 28;
                    }
                    
                    if ((i == 4 || i == 6 || i == 9 || i == 11) && i != 2)
                    {
                        sn = sn + 30;
                    }
                    else if (i != 2)
                        sn = sn + 31;
                }
            }
        }
        sn = sn + (day2Solar - day1Solar);
 
        if (month1lodar > 2 && ((year1Solar % 4 == 0 && year1Solar % 100 != 0) || year1Solar % 400 == 0) && year2Solar > year1Solar)
            sn = sn - 1;
        else if (month2Solar > 2 && ((year2Solar % 4 == 0 && year2Solar % 100 != 0) || year2Solar % 400 == 0) && year2Solar > year1Solar)
            sn = sn + 1;
    }
    if (month1==11 &&year == 2012) {
        sn=30;
    }
//    NSLog(@"so ngay thang %d ,la %d",month1,sn);
    return sn;
}
-(int)numberOfDaysLunar:(int)month1  year:(int)year nhuan:(int)nhuan {
      int monthNhuan = [self monthNhuanYearLunar:year];    
    //    NSArray *arrDateStart = [[NSArray alloc] init ];
    NSArray *arrEndStart ;
    NSArray *arrDateStart  = [self convertLunar2Solar:1 lunarMonth:month1 lunarYear:year lunarLeap:nhuan timeZone:7];
    int day1Solar = [[arrDateStart objectAtIndex:0] intValue];
    int month1lodar = [[arrDateStart objectAtIndex:1] intValue];
    int year1Solar = [[arrDateStart objectAtIndex:2] intValue];
    // NSLog(@"ngay 1 %d ,%d,%d",day1Solar,month1lodar,year1Solar);
    if (monthNhuan == 0) {
        arrEndStart =  [self convertLunar2Solar:1 lunarMonth:(month1+1) lunarYear:year lunarLeap:nhuan timeZone:7];
    }
    else {
        if (monthNhuan == month1 && nhuan == 0) {
            arrEndStart =  [self convertLunar2Solar:1 lunarMonth:month1 lunarYear:year lunarLeap:1 timeZone:7];
        }
        else  {
            arrEndStart =  [self convertLunar2Solar:1 lunarMonth:(month1+1) lunarYear:year lunarLeap:0 timeZone:7];
        }
    }
    int day2Solar = [[arrEndStart objectAtIndex:0] intValue];
    int month2Solar = [[arrEndStart objectAtIndex:1] intValue];
    int year2Solar = [[arrEndStart objectAtIndex:2] intValue];
    //    NSLog(@"ngay 2 %d ,%d,%d",day2Solar,month2Solar,year2Solar);
    //    Distance between 2 days
    int sn = 0;
    if (year2Solar >= year1Solar)
    {
        if (day1Solar == day2Solar && month1lodar == month2Solar)
        {
            for (int i = year1Solar; i < year2Solar; i++)
            {
                if ((i % 4 == 0 && i % 100 != 0) || i % 400 == 0)
                {
                    sn = sn + 366;
                }
                else if ((i % 4 != 0 && i % 100 == 0) || i % 400 != 0)
                {
                    sn = sn + 365;
                }
            }
        }
        
        else
        {
            if (day1Solar != day2Solar || month1lodar != month2Solar)
            {
                
                for (int i = year1Solar; i < year2Solar; i++)
                {
                    if ((i % 4 == 0 && i % 100 != 0) || i % 400 == 0)
                    {
                        sn = sn + 366;
                    }
                    else if ((i % 4 != 0 && i % 100 == 0) || i % 400 != 0)
                    {
                        sn = sn + 365;
                    }
                }
            }
            if (month1lodar >= month2Solar)
            {
                for (int i = month2Solar; i < month1lodar; i++)
                {
                    if (i == 2 && ((year1Solar % 4 == 0 && year1Solar % 100 != 0) || year1Solar % 400 == 0))
                    {
                        sn = sn - 29;
                    }
                    else if (i == 2 && ((year1Solar % 4 != 0 && year1Solar % 100 == 0) || year1Solar % 400 != 0))
                    {
                        sn = sn -28;                    }
                    
                    if ((i == 4 || i == 6 || i == 9 || i == 11 )&& i != 2)
                    {
                        sn = sn - 30;
                    }
                    else if (i != 2)
                        sn = sn - 31;
                }
            }
            else
            {
                for (int i = month1lodar; i < month2Solar; i++)
                {
                    if (i == 2 && ((year1Solar % 4 == 0 && year1Solar % 100 != 0) || year1Solar % 400 == 0))
                    {
                        sn = sn + 29;
                    }
                    else if (i == 2 && ((year1Solar % 4 != 0 && year1Solar % 100 == 0) || year1Solar % 400 != 0))
                    {
                        sn = sn + 28;
                    }
                    
                    if ((i == 4 || i == 6 || i == 9 || i == 11) && i != 2)
                    {
                        sn = sn + 30;
                    }
                    else if (i != 2)
                        sn = sn + 31;
                }
            }
        }
        sn = sn + (day2Solar - day1Solar);
        if (month1lodar > 2 && ((year1Solar % 4 == 0 && year1Solar % 100 != 0) || year1Solar % 400 == 0) && year2Solar > year1Solar)
            sn = sn - 1;
        else if (month2Solar > 2 && ((year2Solar % 4 == 0 && year2Solar % 100 != 0) || year2Solar % 400 == 0) && year2Solar > year1Solar)
            sn = sn + 1;
    }
    
    return sn;
}
-(NSArray *) convertSolar2Lunar:(int) dd mm:(int) mm yy:(int) yy timeZone:(double) timeZone {
    int lunarDay, lunarMonth, lunarYear, lunarLeap;
    int dayNumber =[self jdFromDate:dd mm:mm yy:yy];// jdFromDate(dd, mm, yy);
    int k = [self INT:((dayNumber - 2415021.076998695) / 29.530588853)];
    int monthStart =[self getNewMoonDay:(k+1) timeZone:timeZone];// getNewMoonDay(k+1, timeZone);
    if (monthStart > dayNumber) {
        monthStart =[self getNewMoonDay:k timeZone:timeZone]; //getNewMoonDay(k, timeZone);
    }
    int a11 =[self getLunarMonth11:yy timeZone:timeZone];// getLunarMonth11(yy, timeZone);
    int b11 = a11;
    if (a11 >= monthStart) {
        lunarYear = yy;
        a11 =[self getLunarMonth11:(yy-1) timeZone:timeZone];// getLunarMonth11(yy-1, timeZone);
    } else {
        lunarYear = yy+1;
        b11 =[self getLunarMonth11:(yy+1) timeZone:timeZone];// getLunarMonth11(yy+1, timeZone);
    }
    lunarDay = dayNumber-monthStart+1;
    int diff = [self INT:((monthStart - a11)/29)];
    lunarLeap = 0;
    lunarMonth = diff+11;
    if (b11 - a11 > 365) {
        int leapMonthDiff =[self getLeapMonthOffset:a11 timeZone:timeZone];// getLeapMonthOffset(a11, timeZone);
        if (diff >= leapMonthDiff) {
            lunarMonth = diff + 10;
            if (diff == leapMonthDiff) {
                lunarLeap = 1;
            }
        }
    }
    if (lunarMonth > 12) {
        lunarMonth = lunarMonth - 12;
    }
    if (lunarMonth >= 11 && diff < 4) {
        lunarYear -= 1;
    }
    //NSLog(@"lunar date ==== %i /%i/%i ,%i",lunarDay,lunarMonth,lunarYear,lunarLeap);

    NSArray *arr = [[NSArray alloc] initWithObjects:[NSString stringWithFormat:@"%d",lunarDay],[NSString stringWithFormat:@"%d",lunarMonth],[NSString stringWithFormat:@"%d",lunarYear],[NSString stringWithFormat:@"%d",lunarLeap],nil];
    return arr;
}
-(NSArray *)convertLunar2Solar:(int) lunarDay lunarMonth:(int) lunarMonth  lunarYear:(int) lunarYear lunarLeap:(int) lunarLeap  timeZone:(double) timeZone {
    NSArray *arr = [[NSArray alloc] init];
    int a11, b11;
    if (lunarMonth < 11) {
        a11 =[self getLunarMonth11:(lunarYear-1) timeZone:timeZone];// getLunarMonth11(lunarYear-1, timeZone);
        b11 =[self getLunarMonth11:lunarYear timeZone:timeZone];// getLunarMonth11(lunarYear, timeZone);
    } else {
        a11 = [self getLunarMonth11:lunarYear timeZone:timeZone];//getLunarMonth11(lunarYear, timeZone);
        b11 = [self getLunarMonth11:(lunarYear+1) timeZone:timeZone];//getLunarMonth11(lunarYear+1, timeZone);
    }
    int k =[self INT:(0.5 + (a11 - 2415021.076998695) / 29.530588853)];
    int off = lunarMonth - 11;
    if (off < 0) {
        off += 12;
    }
    if (b11 - a11 > 365) {
        int leapOff = [self getLeapMonthOffset:a11 timeZone:timeZone];// getLeapMonthOffset(a11, timeZone);
        int leapMonth = leapOff - 2;
        if (leapMonth < 0) {
            leapMonth += 12;
        }
        if (lunarLeap != 0 && lunarMonth != leapMonth) {
          //  System.out.println("Invalid input!");
            return arr;
        } else if (lunarLeap != 0 || off >= leapOff) {
            off += 1;
        }
    }
    int monthStart =[self getNewMoonDay:(k+off) timeZone:timeZone];// getNewMoonDay(k+off, timeZone);
    return [self jdToDate:(monthStart+lunarDay-1)];//jdToDate(monthStart+lunarDay-1);
}
-(NSString *)Thu:(int)intThu
{ 
    NSString *thu = [[NSString alloc] init];
    switch (intThu) {
        case 0:
            thu =@"Thứ Hai";
            break;
        case 1:
            thu =@"Thứ Ba";
            break;
        case 2:
            thu =@"Thứ Tư";
            break;
        case 3:
            thu =@"Thứ Năm";
            break;
        case 4:
            thu =@"Thứ Sáu";
            break;
        case 5:
            thu =@"Thứ Bảy";
            break;
        case 6:
            thu =@"Chủ Nhật";
        break;    }
    return thu;
}
-(NSString *)StrCan:(int)intCan
{
    NSString *can = [[NSString alloc] init];
    switch (intCan) {
        case 0:
            can = @"Giáp";
            break;
        case 1:
            can = @"Ất";
            break;
        case 2:
            can = @"Bính";
            break;
        case 3:
            can = @"Đinh";
            break;
        case 4:
            can = @"Mậu";
            break;
        case 5:
            can = @"Kỷ";
            break;
        case 6:
            can = @"Canh";
            break;
        case 7:
            can = @"Tân";
            break;
        case 8:
            can = @"Nhâm";
            break;
        case 9:
            can = @"Quý";
            break;
    }
    return can;
}
-(NSString *)strChi:(int)intChi
{    
    NSString *chi = [[NSString alloc]init];
    switch (intChi) {
        case 0:
            chi = @"Tý";
            break;
        case 1:
            chi = @"Sửu";
            break;
        case 2:
            chi = @"Dần";
            break;
        case 3:
            chi = @"Mão";
            break;
        case 4:
            chi = @"Thìn";
            break;
        case 5:
            chi = @"Tỵ";
            break;
        case 6:
            chi = @"Ngọ";
            break;
        case 7:
            chi = @"Mùi";
            break;
        case 8:
            chi = @"Thân";
            break;
        case 9:
            chi = @"Dậu";
            break;
        case 10:
            chi = @"Tuất";
            break;
        case 11:
            chi = @"Hợi";
            break;
    }
    return chi;
}

- (NSString *)getTruc:(int)idTruc
{
    NSString *strTen = @"";
    switch (idTruc) 
    {
        case 0:
            strTen = @"Trừ ";
            break;
        case 1:
            strTen = @"Mãn ";
            break;
        case 2:
            strTen = @"Bình ";
            break;
        case 3:
            strTen = @"Định ";
            break;
        case 4:
            strTen = @"Chấp ";
            break;
        case 5:
            strTen = @"Phá ";
            break;
        case 6:
            strTen = @"Nguy ";
            break;
        case 7:
            strTen = @"Thành ";
            break;
        case 8:
            strTen = @"Thu ";
            break;
        case 9:
            strTen = @"Khai ";
            break;
        case 10:
            strTen = @"Bế ";
            break;
        case 11:
            strTen = @"Kiến ";
            break;
        default:
            break;
    }
    return strTen;
}

-(NSString *)strChiThang:(int)intChi
{    
    NSString *chi = [[NSString alloc]init];
    switch (intChi) {
        case 11:
            chi = @"Tý";
            break;
        case 12:
            chi = @"Sửu";
            break;
        case 1:
            chi = @"Dần";
            break;
        case 2:
            chi = @"Mão";
            break;
        case 3:
            chi = @"Thìn";
            break;
        case 4:
            chi = @"Tỵ";
            break;
        case 5:
            chi = @"Ngọ";
            break;
        case 6:
            chi = @"Mùi";
            break;
        case 7:
            chi = @"Thân";
            break;
        case 8:
            chi = @"Dậu";
            break;
        case 9:
            chi = @"Tuất";
            break;
        case 10:
            chi = @"Hợi";
            break;
    }
    return chi;
}
- (NSArray *)showCanChiWithDayLunar:(int)dayLunar monthLunar:(int)monthLunar yearLunar:(int)yearLunar nhuan:(int)nhuan
{
    NSArray *arr = [[NSArray alloc] init ];
    arr = [self convertLunar2Solar:dayLunar lunarMonth:monthLunar lunarYear:yearLunar lunarLeap:nhuan timeZone:7];
    int jd2= [self  jdFromDate:[[arr objectAtIndex:0] intValue] mm:[[arr objectAtIndex:1] intValue] yy:[[arr objectAtIndex:2] intValue]];//:[[arr objectAtIndex:0] intValue]:[[arr objectAtIndex:1] intValue]:[[arr objectAtIndex:2] intValue]];
    int intCanDay = (jd2+9)%10; 
    NSString *canDay = [self StrCan:intCanDay]; 
    int intChiDay = (jd2+1)%12;
    NSString *chiDay = [self strChi:intChiDay];     
    int thu= jd2%7;
    
    
    NSString *strThu = [self Thu:thu];
    //thang 
    
    int intCanMonth = (yearLunar*12 + monthLunar +3)%10;
    NSString *canMonth = [self StrCan:intCanMonth]; 
    //int intChiMonth = month; 
    NSString *chiMonth = [self strChiThang:monthLunar]; 
    
    
    
    //Nam
    int intCanYear =(yearLunar+6)%10;  
    NSString *canYear = [self StrCan:intCanYear]; 
    int intChiYear =(yearLunar+8)%12 ; 
    NSString *chiYear = [self strChi:intChiYear];
    
    //Gio
    NSArray *arrCanchi = [[NSArray alloc] initWithObjects:strThu,canDay,chiDay,canMonth,chiMonth,canYear,chiYear,nil];
    
    return arrCanchi;
}

- (NSString*)timeHoangDaoWithDaySolar:(int)day monthSolar:(int)month yearSolar:(int)year {
    
    int jd2= [self jdFromDate:day mm:month yy:year ];
    int intChiDay = (jd2+1)%12;
    switch (intChiDay) {
        case 0:
            return Chi_Ty_Ngo;
            break;
        case 1:
            return Chi_Suu_Mui;
            break;
        case 2:
            return Chi_Dan_Than;
            break;
        case 3:
            return Chi_Mao_Dau;
            break;
        case 4:
            return Chi_Thin_Tuat;
            break;
        case 5:
            return Chi_Ti_Hoi;
            break;
        case 6:
            return Chi_Ty_Ngo;
            break;
        case 7:
            return Chi_Suu_Mui;
            break;
        case 8:
            return Chi_Dan_Than;
            break;
        case 9:
            return Chi_Mao_Dau;
            break;
        case 10:
            return Chi_Thin_Tuat;
            break;
        case 11:
            return Chi_Ti_Hoi;
            break;
            default:
            return @"";
            
    }
}

-(int)calculatorDayBadOrGoodWithDaySolar:(int)day monthSolar:(int)month yearSolar:(int)year
{
    NSArray *arrDateLunar = [self convertSolar2Lunar:day mm:month yy:year timeZone:7];
    int monthLunar = [[arrDateLunar objectAtIndex:1] intValue]; 
      // NSLog(@"monthLunar%d",monthLunar);
    int jd2= [self  jdFromDate:day mm:month yy:year];
    int intChiDay = (jd2+1)%12;
    
 //   NSLog(@"intChiDay%d",intChiDay);
    int cMonth = monthLunar % 6;
    
    int result;
    switch (cMonth) 
    {
        case 0: 
            if( intChiDay==10 || intChiDay==11 || intChiDay==3 || intChiDay==5)
            {
                result = 0;
            }
            else
            {
                if(intChiDay==4 || intChiDay==1 || intChiDay==9 || intChiDay==7)
                {
                    result = 1;
                }
                else
                    result = 2;
            }
            break;
            
        
        case 1:
            if( intChiDay==0  || intChiDay==1 || intChiDay== 5 || intChiDay==7)
            {
                result = 0;
            }
            else
            {
                if(intChiDay==6 || intChiDay==3 || intChiDay==11 || intChiDay==9)
                {
                    result = 1;
                }
                else
                    result = 2;
                    
            }
            break;
            
        
        case 2:
            if( intChiDay==2 || intChiDay==3 || intChiDay==7 || intChiDay==9)
            {
                result = 0;
            }
            else
            {
                if(intChiDay==8 || intChiDay==5 || intChiDay==1 || intChiDay==3)
                {
                    result = 1;
                }
                else
                    result = 2;
            }
            break;
            
        
        case 3:
            if( intChiDay==4 || intChiDay==11 || intChiDay==9 || intChiDay==5)
            {
                result = 0;
            }
            else
            {
                if(intChiDay==10 || intChiDay==7 || intChiDay==1 || intChiDay==11)
                {
                    result = 1;
                }
                else
                    result = 2;
                    
            }
            break;
            
        
        case 4:
            if( intChiDay==6 || intChiDay==7 || intChiDay==1 || intChiDay==9)
            {
                result = 0;
            }
            else
            {
                if(intChiDay==0 || intChiDay==9 || intChiDay==5 || intChiDay==3)
                {
                    result = 1;
                }
                else
                    result = 2;
            }
            break;
            
        
        case 5:
            if( intChiDay==8 || intChiDay==9 || intChiDay==1 || intChiDay==3)
            {
                result = 0;
            }
            else
            {
                if(intChiDay==2 || intChiDay==11 || intChiDay==7 || intChiDay==5)
                {
                    result = 1;
                }
                else
                    result = 2;
            }
            break;
            
        default:
            break;
    }
    
    return result;
}

-(NSString *)canChiHourWithSolar:(int)daySolar monthSolar:(int)monthSolar yearSolar:(int)yearSolar hour:(int)hour{
    
    int jd2= [self  jdFromDate:daySolar  mm:monthSolar yy:yearSolar];
    int canDay = (jd2+9)%10 + 1; 
      // NSLog(@"can canDay %d",canDay);
    int canHour ; 
    int curr;
    if (canDay ==5 ||canDay == 10 ) {
          curr = 9 ;
    }
    else
        curr = (canDay%5)*2-1 ;
    
    for (int j = 1; j <=12; j++) {
         canHour = curr%10  ;
        // NSLog(@"so== %d",curr%10);
       if (hour == j) {
           break;
       }       
        curr++;
    }    
   // NSLog(@"can hour %d",canHour);
    if (canHour==0) {
        canHour =10;
    }
    NSString *strCanHour =[NSString stringWithFormat:@"%@ %@",[self StrCan:canHour-1],[self strChi:[self returnChi_HourWithHour:hour] - 1]];
//    NSLog(@"can hour %d",canHour);s
    return strCanHour;
}

- (NSMutableArray*) getImageCanChiWithLunarDate:(NSDate *) date andNhuan:(int) iNhuan
{
    NSArray *arr = [[NSArray alloc] init ];
//    arr = [self convertLunar2Solar:date.day lunarMonth:date.month lunarYear:date.year lunarLeap:iNhuan timeZone:7];
//    int jd2= [self  jdFromDate:[[arr objectAtIndex:0] intValue] mm:[[arr objectAtIndex:1] intValue] yy:[[arr objectAtIndex:2] intValue]];
//    int iChiHour = [self returnChi_HourWithHour:date.hour];
//    int iChiDay = (jd2+1)%12 + 1;
//    int iChiThang = [self returnChi_ThangWithMonthLunar:date.month];
//    int iChiNam = (date.year + 8)%12 +1;
//    
//    NSString *strHourImageName = [NSString stringWithFormat:@"%d.png",iChiHour];
//    NSString *strDayImageName = [NSString stringWithFormat:@"%d.png",iChiDay];
//    NSString *strMonthImageName = [NSString stringWithFormat:@"%d.png",iChiThang];
//    NSString *strYearImageName = [NSString stringWithFormat:@"%d.png",iChiNam];
//    NSMutableArray *arrResult = [[NSMutableArray alloc] initWithObjects:strHourImageName,strDayImageName,
//                                 strMonthImageName,strYearImageName, nil];
    return arr;
}

- (int) returnChi_ThangWithMonthLunar:(int) aMonth
{
    int iResult = 0;
    switch (aMonth) {
        case 11:
            iResult = 1;
            break;
        case 12:
            iResult = 2;
            break;
        case 1:
            iResult = 3;
            break;
        case 2:
            iResult = 4;
            break;
        case 3:
            iResult = 5;
            break;
        case 4:
            iResult = 6;
            break;
        case 5:
            iResult = 7;
            break;
        case 6:
            iResult = 8;
            break;
        case 7:
            iResult = 9;
            break;
        case 8:
            iResult = 10;
            break;
        case 9:
            iResult = 11;
            break;
        case 10:
            iResult = 12;
            break;
    }
    return iResult;
}

- (int) returnChi_HourWithHour:(int) aHour
{
    int iResult = 0;
    switch (aHour)
    {
        case 0:
        case 23:
            iResult = 1;
            break;
        case 1:
        case 2:
            iResult = 2;
            break;
        case 3:
        case 4:
            iResult = 3;
            break;
        case 5:
        case 6:
            iResult = 4;
            break;
        case 7:
        case 8:
            iResult = 5;
            break;
        case 9:
        case 10:
            iResult = 6;
            break;
        case 11:
        case 12:
            iResult = 7;
            break;
        case 13:
        case 14:
            iResult = 8;
            break;
        case 15:
        case 16:
            iResult = 9;
            break;
        case 17:
        case 18:
            iResult = 10;
            break;
        case 19:
        case 20:
            iResult = 11;
            break;
        case 21:
        case 22:
            iResult = 12;
            break;
        default:
            break;
    }
    return iResult;
}
@end
