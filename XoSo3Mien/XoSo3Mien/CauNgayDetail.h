//
//  CauNgayDetail.h
//  XoSo3Mien
//
//  Created by Tuyen on 2/15/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BienDoView.h"


@interface CauNgayDetail : UIViewController
@property (strong, nonatomic) IBOutlet UIScrollView *scrKetQua;
@property (nonatomic, retain) NSMutableArray *arrKetQua;

@property (nonatomic, retain) NSString *vitri;
@property (nonatomic, retain) NSString *capso;

@property (nonatomic, assign) int songay;
@property (nonatomic, retain) NSString *tenTinh;

@property (strong, nonatomic) IBOutlet UILabel *tittleVC;
@property (strong, nonatomic) IBOutlet UINavigationBar *navTu;
@property (strong, nonatomic) IBOutlet UITextView *tfView;

@end
