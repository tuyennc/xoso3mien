//
//  CauNgayDetail.m
//  XoSo3Mien
//
//  Created by Tuyen on 2/15/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import "CauNgayDetail.h"

@interface CauNgayDetail ()
{
    BienDoView *bienDo;
   BOOL isFirstAppear;
}
@end

@implementation CauNgayDetail
@synthesize arrKetQua = _arrKetQua;
@synthesize vitri = _vitri;
@synthesize capso = _capso;
@synthesize songay = _songay;
@synthesize tenTinh = _tenTinh;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (IBAction)leftBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) viewWillAppear:(BOOL)animated{
    
    
    
    SJLog(@"%s", __FUNCTION__);
    [super viewWillAppear:animated];
    if (IS_OS_7_OR_LATER) {
          if (!isFirstAppear) {
        //        self.navigationController.view.backgroundColor = [UIColor whiteColor];
        //
        UIView *viewSta = [[UIView alloc] initWithFrame:CGRectMake(0, -20, 320, 20)];
        viewSta.backgroundColor = RGB(106, 0, 0);
        
        [self.view addSubview:viewSta];
        //        CGRect screenRect = [[UIScreen mainScreen] bounds];
        //        self.view.frame =  CGRectMake(0,20,self.view.frame.size.width,screenRect.size.height-20);
        for (UIView *sub in [[self view] subviews])
        {
            if (sub.tag !=8888) {
                
                CGRect    tempRect = [sub frame];
                
                tempRect.origin.y += 20.0f; //Height of status bar
                
                [sub setFrame:tempRect];
            }
        }}
        
    }
}
- (void)viewDidLoad
{
//    self.navTu.tintColor = [UIColor whiteColor];
    
	self.navigationController.navigationBar.hidden = YES;
    
    self.navTu.topItem.title = @"Kết Quả Phân Tích";
    
    _tittleVC.font = [UIFont fontWithName:XSFontCondensed size:20];
    
    
    UIImage *imgItem =[ Utility resizeImg:[UIImage imageNamed:@"backNav.png"] withSize:CGSizeMake(17, 17)] ;
    
    self.navTu.topItem.leftBarButtonItem = [UIBarButtonItem barItemWithImage:imgItem target:self action:@selector(leftBtnClicked:)];
    if (IS_OS_7_OR_LATER) {
        self.navTu.barTintColor = RGB(106, 0, 0);
    }
    else
    {
        self.navTu.TintColor = RGB(106, 0, 0);
    }

    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    int i =0;
     NSArray *arrVitri = [_vitri componentsSeparatedByString:@":"];
    _tfView.text = [NSString stringWithFormat:@"Chi tiết phân tích kết quả %d ngày của Xổ Số %@ dự đoán cho cặp số %@ ra trong lần quay tới vị.\nVị trí ghép lên phân tính >> Vị trí 1 : %d,Vị trí 2 : %d",_songay,_tenTinh,_capso,[[arrVitri objectAtIndex:0] integerValue],[[arrVitri objectAtIndex:1] integerValue]];
    _tfView.font = [UIFont fontWithName:XSFontCondensed size:17];

    
    for (NSString *strKQ in _arrKetQua) {
//        XS_CAU_RESP|0|16/02/2014 13:47:55|XSTD|09/02/2014 18:26:47|1:70900|2:02849-83224|3:58935-34303-61732-34516-54045-88482|4:3391-4417-7807-0026|5:9933-0505-7674-1360-6842-4850|6:102-505-962|7:79-21-28-53|8:|0:05148||
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"BienDoView" owner:self options:nil];
         BienDoView *bdtemp  = (BienDoView *)[nib objectAtIndex:0];
       
        
        bdtemp.vitri1 = [[arrVitri objectAtIndex:0] integerValue];
        bdtemp.vitri2 =  [[arrVitri objectAtIndex:1] integerValue];
//        bdtemp.capso = _capso ;
//        bdtemp.songay = _songay;
        bdtemp.nameTinh = _tenTinh;
//        bdtemp.vitri1 = 0;
//        bdtemp.vitri2 =  1;

        
        bdtemp.dictKetQuaxS = [self convertSocket2Http:strKQ];
        bdtemp.maVungChon = [bdtemp.dictKetQuaxS objectForKey:@"code"];
        float heighView =([[bdtemp.dictKetQuaxS objectForKey:@"code"] isEqualToString:@"XSTD"])?290:310;
        bdtemp.frame = CGRectMake(0, i*heighView, 210,heighView );
        bdtemp.userInteractionEnabled = YES;
        [_scrKetQua addSubview:bdtemp];
        _scrKetQua.scrollEnabled = YES;
        [_scrKetQua setContentSize:CGSizeMake(320,heighView*(i+1) )];
        i++;
      
    }
}
- (NSDictionary*)convertSocket2Http:(NSString*)resuft
{
    SJLog(@"%s", __FUNCTION__);
    NSMutableDictionary *dictConvert=[[NSMutableDictionary alloc] init];
    NSArray *arr = [resuft componentsSeparatedByString:@"|"];
    NSString *giai1 = [[arr objectAtIndex:5] substringFromIndex:2];
    NSString *giai2 = [[arr objectAtIndex:6] substringFromIndex:2];
    NSString *giai3 = [[arr objectAtIndex:7] substringFromIndex:2];
    NSString *giai4 = [[arr objectAtIndex:8] substringFromIndex:2];
    NSString *giai5 = [[arr objectAtIndex:9] substringFromIndex:2];
    NSString *giai6 = [[arr objectAtIndex:10] substringFromIndex:2];
    NSString *giai7 = [[arr objectAtIndex:11] substringFromIndex:2];
    NSString *giai8 = [[arr objectAtIndex:12] substringFromIndex:2];
    NSString *giai0 = [[arr objectAtIndex:13] substringFromIndex:2];
    [dictConvert setObject:giai1 forKey:@"G1"];
    [dictConvert setObject:giai2 forKey:@"G2"];
    [dictConvert setObject:giai3 forKey:@"G3"];
    [dictConvert setObject:giai4 forKey:@"G4"];
    [dictConvert setObject:giai5 forKey:@"G5"];
    [dictConvert setObject:giai6 forKey:@"G6"];
    [dictConvert setObject:giai7 forKey:@"G7"];
    [dictConvert setObject:giai8 forKey:@"G8"];
    [dictConvert setObject:giai0 forKey:@"DB"];
    [dictConvert setObject:[arr objectAtIndex:3]  forKey:@"code"];
     [dictConvert setObject:[arr objectAtIndex:4]  forKey:@"date"];
    //    DB = 19584;
    //    G1 = 71078;
    //    G2 = "91300-23097";
    //    G3 = "28683-02264-43174-23728-43189-69145";
    //    G4 = "1933-4639-3068-4792";
    //    G5 = "7590-5482-0069-8087-9955-1515";
    //    G6 = "328-115-863";
    //    G7 = "13-62-33-46";
    //    G8 = "";
    //    code = XSTD;
    //    name = "TH\U1ee6 \U0110\U00d4";
    //
    //    XSSYNC_RESP|0|25/01/2014 19:51:28|XSTD|25/01/2014 18:26:41|1:71078|2:91300-23097|3:28683-02264-43174-23728-43189-69145|4:1933-4639-3068-4792|5:7590-5482-0069-8087-9955-1515|6:328-115-863|7:17-62-33-46|8:|0:19584
    NSDictionary *dic = dictConvert;
    return dic;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
