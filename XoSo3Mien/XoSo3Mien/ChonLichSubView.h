//
//  ChonLichSubView.h
//  XoSo3Mien
//
//  Created by Tuyen on 1/23/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol  ChonLichDelegate <NSObject>

- (void) chonNgay:(NSString*)date;


@end

@interface ChonLichSubView : UIView


@property (strong, nonatomic) IBOutlet UILabel *lblTitleCal;
@property (nonatomic, assign) id<ChonLichDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIView *caclView;

@end
