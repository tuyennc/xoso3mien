//
//  ChonLichSubView.m
//  XoSo3Mien
//
//  Created by Tuyen on 1/23/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import "ChonLichSubView.h"

@implementation ChonLichSubView
{
    int indexCell;
    NSMutableArray *arrDates;
    NSCalendar *calendar;
    NSDate *currentDate;
    UIView *temp;
    
    int dayNow ;
    int monthNow;
    int yearNow;
}
@synthesize delegate = _delegate;
@synthesize caclView = _caclView;


unsigned units = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSWeekdayCalendarUnit;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (IBAction)tets:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(chonNgay:)]) {
        [_delegate chonNgay:@"123"];
    }
}

-(UIView*) createView:(int)day  orgx:(CGFloat)orgx orgy:(CGFloat)orgy monthSodar:(int)m yearSodar:(int)year yearNext:(int)y monthz:(int)monthz cn:(int)cn{
    UIView *uiview = [[UIView alloc] initWithFrame:CGRectMake(orgx, orgy, 30, 30)];
	uiview.backgroundColor = [UIColor clearColor];
    uiview.layer.borderWidth = 0.5;
    uiview.layer.borderColor = [[UIColor whiteColor] CGColor];
    UILabel *lblDaySolar = [[UILabel alloc] initWithFrame:CGRectMake(1, -1, 30, 30)];
	lblDaySolar.textAlignment = NSTextAlignmentCenter;
    lblDaySolar.font = [UIFont fontWithName:XSFontCondensed size:13];
    lblDaySolar.textColor = [UIColor whiteColor];
    if ((m<monthz && y==year) || y<year) {
        [lblDaySolar setBackgroundColor:[UIColor clearColor]];
		lblDaySolar.textColor = [UIColor grayColor];
        [uiview addSubview:lblDaySolar];
        
    }
    else if ((m>monthz && y==year)|| y>year) {
        [lblDaySolar setBackgroundColor:[UIColor clearColor]];
        lblDaySolar.textColor =  [UIColor grayColor];
		[uiview addSubview:lblDaySolar];
    }else {
        
		UIImageView *imgBackground = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0,uiview.frame.size.width, uiview.frame.size.height)];
        UIImageView *imgStar = [[UIImageView alloc]initWithFrame:CGRectMake(32, 0, 11, 11)];
        
		UIImageView *imgEvent = [[UIImageView alloc]initWithFrame:CGRectMake(32, 0, 11, 11)];
        
        [lblDaySolar setBackgroundColor:[UIColor clearColor]];
		
        if ( day == 1 && m!= 1) {
			imgBackground.image = [UIImage imageNamed:@"day_press.png"];
            lblDaySolar.textColor = [UIColor whiteColor];
        }
		if ( day == 1 && m == 1 && y !=2014) {
			imgBackground.image = [UIImage imageNamed:@"day_press.png"];
            lblDaySolar.textColor = [UIColor whiteColor];
        }
         [uiview addSubview:lblDaySolar];
        if (day == dayNow  && m== monthNow && y ==yearNow) {
            
            
            uiview.backgroundColor = RGB(228, 228, 228);
            lblDaySolar.textColor = RGB(106, 0, 0);
            temp = uiview;
        }
        
//        if (cn==0) {
//            lblDaySolar.textColor = [UIColor redColor];
//        }
		[uiview addSubview:imgBackground];
       
		[uiview addSubview:imgEvent];
		[uiview addSubview:imgStar];
		
    }
    
    lblDaySolar.text = [NSString stringWithFormat:@"%d", day];
    
    return uiview;
}

- (void)fillCalendar
{
    //    indexCell = 0;
    arrDates = [[NSMutableArray alloc] init];
    
	calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	// set today
    //	today = [[NSDate alloc] initWithTimeIntervalSinceNow:1];
	NSDateComponents *components = [calendar components:units fromDate:currentDate];
  	[components setDay:1];
    //	currentDate = [calendar dateFromComponents:components];
    
    //    NSDateComponents *components = [calendar components:units fromDate:currentDate];
    
    //	NSLog(@"date fill %@",components);
    // change for the first day
	[components setDay:1];
	// update the component
	components = [calendar components:units fromDate:[calendar dateFromComponents:components]];
	[components setDay:-[components weekday]+2];
	int lessDay = [components day];
	int month = [components month];
	int year = [components year];
	CGFloat orgx=0,orgy=0;
	int m=month;
    int i = 1;
    for (int week=0; week<6 ; week++)
	{
		for (int d=0; d<7; d++)
		{
            int cn=d;
			[components setDay:lessDay];
			int d =[[calendar components:units fromDate:[calendar dateFromComponents:components]] day];
			m =[[calendar components:units fromDate:[calendar dateFromComponents:components]] month];
			int y =[[calendar components:units fromDate:[calendar dateFromComponents:components]] year];
            
            
            NSArray *arrSolar = [[NSArray alloc] initWithObjects:[NSString stringWithFormat:(d<10)?@"0%d":@"%d",d],
                                 [NSString stringWithFormat:(m<10)?@"0%d":@"%d",m],[NSString stringWithFormat:@"%d",y],nil];
            
            NSArray *arr = [[NSArray alloc] init];
            
            //        custom view
            UITapGestureRecognizer *prevMonth =
            [[UITapGestureRecognizer alloc] initWithTarget:self
                                                    action:@selector(prevMonth:)];
            
           
            [prevMonth setNumberOfTapsRequired:1];
            
            UITapGestureRecognizer *nextMonth =
            [[UITapGestureRecognizer alloc] initWithTarget:self
                                                    action:@selector(nextMonth:)];
            [nextMonth setNumberOfTapsRequired:1];
            UITapGestureRecognizer *showInfor =
            [[UITapGestureRecognizer alloc] initWithTarget:self
                                                    action:@selector(showInfors:)];
            [showInfor setNumberOfTapsRequired:1];
            
            
            UIView *view = [self createView:d   orgx:orgx orgy:orgy monthSodar:m yearSodar:year yearNext:y  monthz:month cn:cn];
            //
            
            if ((m<month && y==year) || y<year) {
                [view addGestureRecognizer:prevMonth];
                
			}
            else if ((m>month && y==year)|| y>year) {
				[view addGestureRecognizer:nextMonth];
			}
            else {
                view.tag = i;
                if (d == 23 && m== 1 && y ==2014) {
                    //if day now
                    indexCell = view.tag - 1;
                }
//                NSLog(@"%d",i);
				[view addGestureRecognizer:showInfor];
                [arrDates addObject:arrSolar];
                i++;
			}
			[_caclView addSubview:view];
			orgx+=30;
			lessDay++;
		}
		orgy+=30;
		orgx=0;
		//NSLog(@"leess day %d",lessDay);
        [components setDay:lessDay];
        m =[[calendar components:units fromDate:[calendar dateFromComponents:components]] month];
        //       indexCell = 0;
        
	}
    //    [calViewContainer addSubview:calView];
}

- (IBAction)nextMonth:(id)action{
    
    
	// extracting components from date
	NSDateComponents *components = [calendar components:units fromDate:currentDate];
	components.month = components.month + 1;
    [self setCurrentDate: [calendar dateFromComponents:components]];
    //	currentDate = [calendar dateFromComponents:components];
    //	[self setArrsCurrentDateSodar:1 monthSodar:currentDate.month yearSodar:currentDate.year];
}


- (IBAction)prevMonth:(id)action{
    
    
	// extracting components from date
	NSDateComponents *components = [calendar components:units fromDate:currentDate];
	components.month = components.month - 1;
    [self setCurrentDate: [calendar dateFromComponents:components]];
    //    [self setArrsCurrentDateSodar:1 monthSodar:currentDate.month yearSodar:currentDate.year];
}
- (void)setCurrentDate:(NSDate *)value
{
    currentDate = value;
    [[_caclView subviews]  makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self fillCalendar];
}
-(void)nextYear{
    
    NSDateComponents *components = [calendar components:units fromDate:currentDate];
	components.year = components.year + 1;
    
    [self setCurrentDate: [calendar dateFromComponents:components]];
    //	[self setArrsCurrentDateSodar:1 monthSodar:currentDate.month yearSodar:currentDate.year];
}
-(void)preYear{
    
    
    NSDateComponents *components = [calendar components:units fromDate:currentDate];
	components.year = components.year -1;
    [self setCurrentDate: [calendar dateFromComponents:components]];
    //	[self setArrsCurrentDateSodar:1 monthSodar:currentDate.month yearSodar:currentDate.year];
    
}

- (void)showInfors:(UITapGestureRecognizer *)recognizer
{
    NSString *ngay = [[arrDates objectAtIndex:recognizer.view.tag-1] objectAtIndex:0];
    NSString *thang= [[arrDates objectAtIndex:recognizer.view.tag-1] objectAtIndex:1];
    NSString *nam = [[arrDates objectAtIndex:recognizer.view.tag-1] objectAtIndex:2];
    
    temp.backgroundColor= [UIColor clearColor];
    for (UIView *subV in temp.subviews) {
        if ([subV isKindOfClass:[UILabel class]]) {
            UILabel *lblText = (UILabel*)subV;
            lblText.textColor = [UIColor whiteColor];
        }
    }
    
    UIView *viewSelect = recognizer.view;
    viewSelect.backgroundColor = RGB(228, 228, 228);
    for (UIView *subV in viewSelect.subviews) {
        if ([subV isKindOfClass:[UILabel class]]) {
            UILabel *lblText = (UILabel*)subV;
            lblText.textColor = RGB(106, 0, 0);
        }
    }
    
    temp = viewSelect;
    
    
    NSLog(@"ngay  thang %@",[arrDates objectAtIndex:recognizer.view.tag-1]);
	_lblTitleCal.text = [NSString stringWithFormat:@"Tháng %@ Năm %@",thang,nam ];
    if (_delegate && [_delegate respondsToSelector:@selector(chonNgay:)]) {
        [_delegate chonNgay:[NSString stringWithFormat:@"%@/%@/%@",ngay,thang,nam]];
    }

}




// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    _caclView.backgroundColor = [UIColor clearColor];
    currentDate = [NSDate date];
    calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];

  	NSDateComponents *components = [calendar components:units fromDate:currentDate];
    dayNow = [components day];
    monthNow  = [components month];
    yearNow = [components year];
   
    _lblTitleCal.text =[NSString stringWithFormat:@"Tháng %d Năm %d",[components month],[components year] ];
//    [[self subviews]  makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self fillCalendar];
}


@end
