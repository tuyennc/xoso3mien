//
//  ChonTinhSubView.h
//  XoSo3Mien
//
//  Created by Tuyen on 1/24/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import <UIKit/UIKit.h>



@protocol  ChonTinhDelegate <NSObject>

- (void) maTinhChon:(NSString*)maTinh withName:(NSString*)nameTinh;


@end
@interface ChonTinhSubView : UIView<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, assign) id<ChonTinhDelegate> delegate;
@property (strong, nonatomic) IBOutlet UITableView *tblChonTinh;
@property (nonatomic, assign) int typeRegion;// 0.Mientrung 1.MienNam

@property (nonatomic,retain) NSMutableArray *arrCodeTinh;

@property (nonatomic,retain) NSMutableArray *arrNameTinh;
@end
