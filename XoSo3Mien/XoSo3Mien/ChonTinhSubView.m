//
//  ChonTinhSubView.m
//  XoSo3Mien
//
//  Created by Tuyen on 1/24/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import "ChonTinhSubView.h"

@implementation ChonTinhSubView
{
    NSArray *arrTinh ;
    NSArray *arrMaTinh ;
}
@synthesize delegate = _delegate;
@synthesize typeRegion = _typeRegion;
@synthesize tblChonTinh = _tblChonTinh;
@synthesize arrCodeTinh = _arrCodeTinh;
@synthesize arrNameTinh = _arrNameTinh;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}



- (void)setArrCodeTinh:(NSMutableArray *)arrCodeTinh
{
    _arrCodeTinh = arrCodeTinh;
    
      _tblChonTinh.frame = CGRectMake(0, 0, 92, 40*arrCodeTinh.count);
    [_tblChonTinh reloadData];
}
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    _tblChonTinh.delegate = self;
    _tblChonTinh.dataSource = self;
    self.layer.cornerRadius = 7;
    
    if (IS_OS_7_OR_LATER) {
        
        [_tblChonTinh setSeparatorInset:UIEdgeInsetsZero];
    }
    [_tblChonTinh setSeparatorColor:[UIColor grayColor]];
    

}
-(void)setTypeRegion:(int)typeRegion
{
    _typeRegion = typeRegion;
    if (_typeRegion==0) {
        NSString *dsTinh =[Utility dsTinhMienTrung];
        arrTinh = [dsTinh componentsSeparatedByString:@"#"];
        NSString *dsMaTinh =[Utility dsMaTinhMienTrung];
        arrMaTinh = [dsMaTinh componentsSeparatedByString:@"#"];
    }
    else
    {
        NSString *dsTinh =[Utility dsTinhMienNam];
        arrTinh = [dsTinh componentsSeparatedByString:@"#"];
        NSString *dsMaTinh =[Utility dsMaTinhMienNam];
        arrMaTinh = [dsMaTinh componentsSeparatedByString:@"#"];
    }
    [_tblChonTinh reloadData];
}

#pragma mark
#pragma mark Tableview Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return _arrCodeTinh.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
            return 40;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = RGB(106, 0, 0);
    }
    [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];

    UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, 92-25, 40)];
     lbl.text = [_arrNameTinh objectAtIndex:indexPath.row];
    lbl.font = [UIFont fontWithName:XSFontCondensed size:13];
    lbl.textColor = [UIColor whiteColor];
    
    UIImage *img = [Utility resizeImg:[UIImage imageNamed:@"arr_chontinh.png"] withSize:CGSizeMake(8, 12)];
    cell.imageView.image = img;
//    cell.textLabel.text = [_arrNameTinh objectAtIndex:indexPath.row];
//    cell.textLabel.font = [UIFont systemFontOfSize:9];
//    cell.textLabel.textColor = [UIColor whiteColor];
    [cell.contentView addSubview:lbl];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_delegate && [_delegate respondsToSelector:@selector(maTinhChon:withName:)]) {
        [_delegate maTinhChon:[_arrCodeTinh objectAtIndex:indexPath.row] withName:[_arrNameTinh objectAtIndex:indexPath.row]];
    }
}


@end
