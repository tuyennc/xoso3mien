//
//  ChonVeSoVC.m
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import "ChonVeSoVC.h"
#import "ThemVeSoVC.h"
#import "PUKetQua.h"
#import "DetailChonVe.h"

@interface ChonVeSoVC ()<ThemVeSoDelegate,PUKetQuaDelegate,DangKyDelegate,LogInOutDelegate>
{
    ECSlidingViewController  *slidingViewController;
    LogInOutVC   *logInOut;
   
    ThemVeSoVC *themVs;
    PUKetQua *popUpKetQua;
    ASIHTTPRequest *request;
    NSDictionary *dictCurrnet;
     DangKiVC     *dangKi;
      AccountInfor   *accInfor;
   int randomY;
}
@property (nonatomic,retain) NSMutableArray *arrVeSo;
@end



@implementation ChonVeSoVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.slidingViewController resetTopView];
    self.navTu.topItem.title = @"";
    _tittleVC.text =@"Chọn Vé Số";
     _tittleVC.font = [UIFont fontWithName:XSFontCondensed size:20];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    int minY = 1;
    int maxY = 3;
    int rangeY = maxY - minY;
    randomY = (arc4random() % rangeY) + minY;
    if (randomY==1) {
        NSURL *url   = [[NSBundle mainBundle] URLForResource:@"anhdong" withExtension:@"gif"];
        _imgAddV.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    }
    else
        _imgAddV.image = [UIImage imageNamed:@"banner-ole-mobile.jpg"];
    
    NSUserDefaults *ndf = [NSUserDefaults standardUserDefaults];
    _arrVeSo = [[NSMutableArray alloc]initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"ArrVeSo"]];
//   _arrVeSo = [[ndf objectForKey:@"ArrVeSo"] copy];
    if (_arrVeSo==nil) {
        _arrVeSo = [[NSMutableArray alloc] init];

    }
       // Do any additional setup after loading the view from its nib.
    UIImage *imgItem =[ Utility resizeImg:[UIImage imageNamed:@"iconmenu.png"] withSize:CGSizeMake(30, 30*30/50)] ;
    
    self.navTu.topItem.leftBarButtonItem = [UIBarButtonItem barItemWithImage:imgItem target:self action:@selector(leftBtnClicked:)];
    if (IS_OS_7_OR_LATER) {
        self.navTu.barTintColor = RGB(106, 0, 0);
    }
    else
    {
        self.navTu.TintColor = RGB(106, 0, 0);
    }
    
    //    ecsliding
//    slidingViewController = [[ECSlidingViewController alloc] init];
//    MenuVC *menuVC   = [[MenuVC alloc] initWithNibName:@"MenuVC" bundle:nil];
//    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
//    self.slidingViewController.underLeftViewController  = menuVC;
//    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
//
    
    self.tblVeSo.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 1.0f)] ;
    self.tblVeSo.tableHeaderView.backgroundColor = [UIColor lightGrayColor];
    self.tblVeSo.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 1.0f)] ;
    self.tblVeSo.tableFooterView.backgroundColor = [UIColor lightGrayColor];
    //    self.tblMenu.tableFooterView.hidden = YES;
    
    if (IS_OS_7_OR_LATER) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        [self setNeedsStatusBarAppearanceUpdate];
        UIView *viewSta = [[UIView alloc] initWithFrame:CGRectMake(0, -20, 320, 20)];
        viewSta.backgroundColor = RGB(106, 0, 0);
        
        [self.view addSubview:viewSta];
        [self.tblVeSo setSeparatorInset:UIEdgeInsetsZero];
    }
    self.tblVeSo.separatorColor =  [UIColor lightGrayColor];
    
    _btnThemVeSo.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:17];
    _btnThemVeSo.layer.cornerRadius = 10;
    //    login
    UIButton *btnSelectDate=[UIButton buttonWithType:UIButtonTypeCustom];
    btnSelectDate.frame = CGRectMake(0, 0,30,30);
    
    if ([[AppDelegate sharedDelegate] isLogin]) {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"Logged.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    else
    {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"LogInOut.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    [btnSelectDate addTarget:self action:@selector(pushViewLogIO) forControlEvents:UIControlEventTouchUpInside];
    self.navTu.topItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSelectDate];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateImageLogin)
                                                 name:@"UPDATE_IMAGE_LOGIN"
                                               object:nil];
    
    
    
    
}
- (IBAction)leftBtnClicked:(id)sender
{
    [[[UIApplication sharedApplication]keyWindow]endEditing:YES];
    [self.slidingViewController anchorTopViewTo:ECRight];
}
#pragma mark
#pragma mark loginout delete

-(void)dangKi
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    dangKi  = [[DangKiVC alloc] initWithNibName:@"DangKiVC" bundle:nil]  ;
   dangKi.delegate = self;
    [self presentPopupViewController:dangKi animationType:MJPopupViewAnimationFade];
    
}
-(void)closePopUp
{
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
    
}
-(void)updateImageLogin
{
    UIButton *btnSelectDate=[UIButton buttonWithType:UIButtonTypeCustom];
    btnSelectDate.frame = CGRectMake(0, 0,30,30);
    
    if ([[AppDelegate sharedDelegate] isLogin]) {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"Logged.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    else
    {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"LogInOut.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    [btnSelectDate addTarget:self action:@selector(pushViewLogIO) forControlEvents:UIControlEventTouchUpInside];
    self.navTu.topItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSelectDate];
}

-(void)pushViewLogIO
{
    if ([[AppDelegate sharedDelegate ] isLogin]==NO) {
    logInOut  = [[LogInOutVC alloc] initWithNibName:@"LogInOutVC" bundle:nil]  ;
    logInOut.delegate = self;
    [self presentPopupViewController:logInOut animationType:MJPopupViewAnimationFade];
    
}
else
{
    accInfor  = [[AccountInfor alloc] initWithNibName:@"AccountInfor" bundle:nil]  ;
    accInfor.delegate = self;
    [self presentPopupViewController:accInfor animationType:MJPopupViewAnimationFade];
    
}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark
#pragma mark - tableview delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arrVeSo.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
       return 30;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor whiteColor];
    }
    [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    cell.backgroundColor = RGB(231, 231, 231);
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 20, 20)];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
     UIImage *imgItem =[ Utility resizeImg:[UIImage imageNamed:@"mn_chonve.png"] withSize:CGSizeMake(20, 20)] ;
    imgView.image = imgItem;
    
    
    UILabel *lblVeSo = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, 40, 30)];
    lblVeSo.backgroundColor = [UIColor clearColor];
//    lblVeSo.textColor = [UIColor yellowColor];
    lblVeSo.textAlignment = NSTextAlignmentCenter;
    lblVeSo.font = [UIFont fontWithName:XSFontCondensed size:13];
    lblVeSo.text = [[_arrVeSo objectAtIndex:indexPath.row] objectForKey:@"veso"];
    
    
    UILabel *lblngay = [[UILabel alloc] initWithFrame:CGRectMake(70, 0, 100, 30)];
    lblngay.backgroundColor = [UIColor clearColor];
//    lblngay.textColor = [UIColor yellowColor];
    lblngay.textAlignment = NSTextAlignmentCenter;
    lblngay.font = [UIFont fontWithName:XSFontCondensed size:13];
    lblngay.text = [NSString stringWithFormat:@"%@-%@",[[_arrVeSo objectAtIndex:indexPath.row] objectForKey:@"ngay"],[[_arrVeSo objectAtIndex:indexPath.row] objectForKey:@"tentinh"]];
    
    UILabel *lblTr = [[UILabel alloc] initWithFrame:CGRectMake(170, 0, 80, 30)];
    lblTr.backgroundColor = [UIColor clearColor];
//    lblTr.textColor = [UIColor yellowColor];
    lblTr.font = [UIFont fontWithName:XSFontCondensed size:13];
    lblTr.text = [NSString stringWithFormat:@"%@",[[_arrVeSo objectAtIndex:indexPath.row] objectForKey:@"isTrung"]];
  lblTr.textAlignment = NSTextAlignmentCenter;
    
    
    if ([[[_arrVeSo objectAtIndex:indexPath.row] objectForKey:@"isTrung"] isEqualToString:@"Trúng"]) {
         lblTr.textColor = RGB(106, 0, 0);
         lblngay.textColor = RGB(106, 0, 0);
         lblVeSo.textColor = RGB(106, 0, 0);
    }
    
    UIButton *btnChiTiet = [[UIButton alloc] initWithFrame:CGRectMake(250, 1, 40, 28)];
    btnChiTiet.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:13];
    [btnChiTiet setTitle:@"Chi tiết" forState:UIControlStateNormal];
     [btnChiTiet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
     btnChiTiet.tag = indexPath.row;
    btnChiTiet.backgroundColor = RGB(26, 26, 26);
    
    
    UIButton *btnXoa = [[UIButton alloc] initWithFrame:CGRectMake(291, 1, 29, 28)];
    [btnXoa setTitle:@"Xoá" forState:UIControlStateNormal];
      [btnXoa setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
 btnXoa.backgroundColor = RGB(26, 26, 26);
    btnXoa.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:13];
    btnXoa.tag = indexPath.row;
    [btnXoa addTarget:self action:@selector(xoaVeSo:) forControlEvents:UIControlEventTouchUpInside];
       [btnChiTiet addTarget:self action:@selector(xemChiTiet:) forControlEvents:UIControlEventTouchUpInside];
    [cell.contentView addSubview:btnChiTiet];
    [cell.contentView addSubview:btnXoa];
    [cell.contentView addSubview:lblVeSo];
     [cell.contentView addSubview:lblngay];
     [cell.contentView addSubview:lblTr];
    [cell.contentView addSubview:imgView];
    return cell;
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row){
  
        [self performSelector:@selector(selectRowAtIndexPath) withObject:nil afterDelay:0.1];
    }
}




- (void) selectRowAtIndexPath {
    NSIndexPath* ipath = [NSIndexPath indexPathForRow: _arrVeSo.count-1 inSection:0];
    [_tblVeSo scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionBottom  animated: NO];
    
}


- (void)xoaVeSo:(UIButton*)btn
{
    [_arrVeSo removeObjectAtIndex:btn.tag];
    [self saveNSUser];
    [_tblVeSo reloadData];
}

- (IBAction)themVeso:(id)sender {
   themVs = [[ThemVeSoVC alloc] initWithNibName:@"ThemVeSoVC" bundle:nil];
    themVs.delegate = self;
    [self presentPopupViewController:themVs animationType:MJPopupViewAnimationFade];
    
}


#pragma mark 
#pragma mark  THEMVESO delegate




-(void)themVeSo:(NSString *)veSo withDate:(NSString *)ngay withMatinh:(NSString *)maTinh withTentinh:(NSString *)tenTinh withTrung:(NSMutableArray *)isTrung withKetQua:(NSDictionary*)dictKetQua;
{
    
   
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    popUpKetQua  = [[PUKetQua alloc] initWithNibName:@"PUKetQua" bundle:nil]  ;
   
        NSDictionary *dict;
    if(isTrung)
    {
     dict   = [[NSDictionary alloc] initWithObjectsAndKeys:veSo,@"veso",ngay,@"ngay",maTinh,@"tinh",tenTinh,@"tentinh",@"Trúng",@"isTrung",dictKetQua,@"ketqua",isTrung,@"arrTrung", nil];
      
        NSString *str =[isTrung objectAtIndex:0];
        for (int i = 1; i< isTrung.count;i++)
        {
            str = [str stringByAppendingString:[NSString stringWithFormat:@" và %@",[isTrung objectAtIndex:i]]];
        }
          popUpKetQua.strKetQua = [NSString stringWithFormat:@"Xin chúc mừng, bạn đã trúng giải %@ của đài %@ mở thưởng ngày %@ !",str,tenTinh,ngay ];
    }
    else
    {
        dict = [[NSDictionary alloc] initWithObjectsAndKeys:veSo,@"veso",ngay,@"ngay",maTinh,@"tinh",tenTinh,@"tentinh",@"Không trúng",@"isTrung",dictKetQua,@"ketqua",isTrung,@"arrTrung", nil];
        popUpKetQua.strKetQua = [NSString stringWithFormat:@"Rất tiếc, bạn không trúng giải nào của đài %@ mở thưởng ngày %@ !",tenTinh,ngay ];
       
    }
    dictCurrnet = dict;
    [_arrVeSo addObject:dict];
    [self saveNSUser];
    [_tblVeSo reloadData];
    
    
    popUpKetQua.delegate = self;
    [self presentPopupViewController:popUpKetQua animationType:MJPopupViewAnimationFade];

}
- (void)saveNSUser
{
   
    NSUserDefaults *ndf = [NSUserDefaults standardUserDefaults];
    [ndf setObject:_arrVeSo   forKey:@"ArrVeSo"];
    [ndf synchronize ];
}
//#pragma mark
//#pragma mark  PopupKetQua delegate
//
//-(void)closePopUp
//{
//    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
//}

-(void)xemChiTiet
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];

    DetailChonVe *detailChon = [[DetailChonVe alloc] initWithNibName:@"DetailChonVe" bundle:nil];
    detailChon.tenTinh = [dictCurrnet objectForKey:@"tentinh"];
    detailChon.isTrung = [dictCurrnet objectForKey:@"arrTrung"];
    detailChon.veSo =  [dictCurrnet objectForKey:@"veso"];
    detailChon.dictKetQUa =[dictCurrnet objectForKey:@"ketqua"];
    detailChon.ngayVe = [dictCurrnet objectForKey:@"ngay"];
    [self.navigationController pushViewController:detailChon animated:YES];
}
-(void)xemChiTiet:(UIButton*)btn

{
    

    DetailChonVe *detailChon = [[DetailChonVe alloc] initWithNibName:@"DetailChonVe" bundle:nil];
    detailChon.tenTinh = [[_arrVeSo objectAtIndex:btn.tag] objectForKey:@"tentinh"];
    detailChon.isTrung = [[_arrVeSo objectAtIndex:btn.tag] objectForKey:@"arrTrung"];
    detailChon.veSo =  [[_arrVeSo objectAtIndex:btn.tag] objectForKey:@"veso"];
    detailChon.dictKetQUa =[[_arrVeSo objectAtIndex:btn.tag] objectForKey:@"ketqua"];
    detailChon.ngayVe = [[_arrVeSo objectAtIndex:btn.tag] objectForKey:@"ngay"];
    [self.navigationController pushViewController:detailChon animated:YES];
}
- (IBAction)quangCao:(id)sender {
    if (randomY==1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:LINK_OLIMPIA]];
    }
    else
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:LINK_OLE]];
}

@end
