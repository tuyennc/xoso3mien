//
//  Constant.h
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#ifndef XoSo3Mien_Constant_h
#define XoSo3Mien_Constant_h

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES) 

#define RGB(r, g, b)                    [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a)                [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

#define DS_NOT_VIETTEL @"8985"
#define DS_VIETTEL_XS @"8185"
#define DS_VIETTEL_CX @"8585"
#define LINK_OLE @"http://ole.vn"
#define LINK_OLIMPIA @"http://olympia.sms.vn"

#define DS_VIETTEL_NOT_MB 8285

#define XSFontRegular @"MyriadPro-Regular"
#define XSFontItalic @"MyriadPro-It"
#define XSFontBold @"MyriadPro-Bold"
#define XSFontBoldItalic @"MyriadPro-BoldIt"
#define XSFontSemiboldItalic @"MyriadPro-SemiboldIt"
#define XSFontCondensed @"MyriadPro-Cond"
#define XSFontCondensedItalic @"MyriadPro-CondIt"
#define XSiFontBoldCondensed @"MyriadPro-BoldCond"
#define XSiFontBoldCondensedItalic @"MyriadPro-BoldCondIt"
#define XSFontSemiBold @"MyriadPro-Semibold"



//CanChi
#define     CanChiTy                @"Tý"
#define     CanChiSuu               @"Sửu"
#define     CanChiDan               @"Dần"
#define     CanChiMao               @"Mão"
#define     CanChiThin              @"Thìn"
#define     CanChiTi                @"Tỵ"
#define     CanChiNgo               @"Ngọ"
#define     CanChiMui               @"Mùi"
#define     CanChiThan              @"Thân"
#define     CanChiHoi               @"Hợi"
#define     CanChiDau               @"Dậu"
#define     CanChiTuat              @"Tuất"



#define  Chi_Ty_Ngo                 @"Tý(23-1), Sửu(1-3), Mão(5-7), Ngọ(11-13), Thân(15-17), Dậu(17-19)"
#define  Chi_Suu_Mui                @"Dần(3-5), Mão(5-7), Thân(15-17), Tuất(19-21), Hơi(21-23), Tỵ(9-11)"
#define  Chi_Dan_Than               @"Tý(23-1), Sửu(1-3), Thìn(7-9), Tỵ(9-11), Mùi(13-15), Tuất(19-21)"
#define  Chi_Mao_Dau                @"Tý(23-1), Dần(3-5), Mão(5-7), Ngọ(11-13), Mùi(13-15), Dậu(17-19)"
#define  Chi_Thin_Tuat              @"Dần(3-5), Thìn(7-9), Tỵ(9-11), Thân(15-17), Dậu(17-19), Hơi(21-23)"
#define  Chi_Ti_Hoi                 @"Sửu(1-3), Thìn(7-9), Ngọ(11-13), Mùi(13-15), Tuất(19-21), Hơi(21-23)"

#endif
