//
//  Contast.h
//  XoSo3Mien
//
//  Created by Tuyen on 2/18/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contast : NSObject
@property (nonatomic, retain) NSString *lastName;
@property (nonatomic, retain) NSString *firstName;
@property (nonatomic, retain) NSString *number;
@property (nonatomic, retain) UIImage *imgAvartar;
@end
