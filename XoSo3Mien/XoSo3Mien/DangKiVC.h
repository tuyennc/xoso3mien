//
//  DangKiVC.h
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import <UIKit/UIKit.h>



@protocol DangKyDelegate<NSObject>
@optional
- (void) taoAC;
-(void)closePopUp;

@end

@interface DangKiVC : UIViewController

@property (strong, nonatomic) id<DangKyDelegate> delegate;
@property (strong, nonatomic) IBOutlet UILabel *lblError;
@property (strong, nonatomic) IBOutlet UITextField *txtHo;
@property (strong, nonatomic) IBOutlet UITextField *txtUserName;
@property (strong, nonatomic) IBOutlet UITextField *txtMatkhau;
@property (strong, nonatomic) IBOutlet UITextField *txtReMk;
@property (strong, nonatomic) IBOutlet UITextField *txtSdt;
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UIButton *btnTaoTK;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

@end
