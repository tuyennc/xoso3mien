//
//  DangKiVC.m
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import "DangKiVC.h"

#import "AES128.h"

@interface DangKiVC ()
{
     ASIHTTPRequest *request;
}
@end

@implementation DangKiVC

@synthesize delegate = _delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)requestLogin:(NSString*)userName withPass:(NSString*)pass
{
    [[AppDelegate sharedDelegate].hudRequest show:YES];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://id.inet.vn/checkLoginInfoApp?user=%@&pass=%@",userName,pass]];
    NSLog(@"requset :%@",url);
    request = [ASIHTTPRequest requestWithURL:url];
    
    [request setCompletionBlock:^{
        
        
        
        [[AppDelegate sharedDelegate].hudRequest hide:YES];
        // Use when fetching text data
        NSString *responseString = [request responseString];
        NSLog(@"%@",responseString);
        
        if ([responseString isEqualToString:@"0\r\n"]) {
            [[AppDelegate sharedDelegate] setIsLogin:NO];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Tên đăng nhập hoặc mật khẩu của bạn không đúng !" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            NSUserDefaults *df =[NSUserDefaults standardUserDefaults];
            [df setObject:@"0" forKey:@"USER_INFOR"];
            [df synchronize];
        }
        else
        {
            
            [[AppDelegate sharedDelegate] setIsLogin:YES];
            NSUserDefaults *df =[NSUserDefaults standardUserDefaults];
            [df setObject:responseString forKey:@"USER_INFOR"];
            [df synchronize];
            if (_delegate && [_delegate respondsToSelector:@selector(closePopUp)]) {
                [_delegate closePopUp];
            }
             [[NSNotificationCenter defaultCenter] postNotificationName:@"UPDATE_IMAGE_LOGIN" object:self];
        }
    }];
    [request setFailedBlock:^{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Không thể kết nối" delegate:self cancelButtonTitle:@"Kết nối lại" otherButtonTitles:@"Huỷ", nil] ;
        [alert show];
        //        [UIHelper showMessage:@"Không thể kết nối" withTitle:@""];
    }];
    [request startAsynchronous];
    
}
#pragma mark
#pragma mark Request ket qua
- (void)requestDangki:(NSString*)userName  withPass:(NSString*)pass withFullName:(NSString*)fullName withEmail:(NSString*)email withMobie:(NSString*)mobie
{
    [[AppDelegate sharedDelegate].hudRequest show:YES];
    
    
    
    
    
    
    NSString *str =[NSString stringWithFormat:@"http://id.inet.vn/checkRegister?fullname=%@&user=%@&email=%@&mobile=%@&pass=%@",fullName,userName,email,mobie,pass];
    

    
    
    NSURL *url = [NSURL URLWithString:[str stringByAddingPercentEscapesUsingEncoding:
                                       NSUTF8StringEncoding]];
    NSLog(@"requset :%@",url);
    request = [ASIHTTPRequest requestWithURL:url];
    
    [request setCompletionBlock:^{
        
        
        
        [[AppDelegate sharedDelegate].hudRequest hide:YES];
        
        // Use when fetching text data
        NSString *responseString = [request responseString];
        NSLog(@"%@",responseString);
//        fullname=nguyencongtuyen&user=nguyencongtuyen&email=phamngocmai.94@gmail.com&mobile=09782532665&pass=HjevLxaHs3KwCgv3jZg1Ug==
//        fullname=truongtamphong&user=nguoidingoaipho&email=hanyu279@yahoo.com&mobile=0978323232&pass=HjevLxaHs3KwCgv3jZg1Ug==
        
        
        if ([responseString integerValue]>0) {
            [self requestLogin:userName withPass:pass];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Đăng ký không thành công" message:@"Vui lòng nhập chính xác các thông tin" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
      
        
    }];
    [request setFailedBlock:^{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Không thể kết nối" delegate:self cancelButtonTitle:@"Kết nối lại" otherButtonTitles:@"Huỷ", nil] ;
        [alert show];
        //        [UIHelper showMessage:@"Không thể kết nối" withTitle:@""];
    }];
    [request startAsynchronous];
    
}



- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:NO];
     [UIView animateWithDuration:0.3f animations:^{self.view.transform = CGAffineTransformMakeTranslation(0,0);}];
}
- (IBAction)taoTaiKhoan:(id)sender {
    
    if ([_txtHo.text length]==0) {
       _lblError.text = @"Điền họ tên";
        [_lblError becomeFirstResponder];
    }else if ([_txtUserName.text length]==0){
         _lblError.text = @"Điền tên đăng nhập";
        [_txtUserName becomeFirstResponder];
    } else if ([_txtMatkhau.text length]<8){
        _lblError.text = @"Mật khẩu phải ít nhất 8 kí tự";
        [_txtMatkhau becomeFirstResponder];
    }else if (![_txtReMk.text isEqualToString:_txtMatkhau.text]){
        _lblError.text = @"Mật khẩu không khớp nhau";
        [_txtReMk becomeFirstResponder];
    }else if ([_txtSdt.text length]==0){
        _lblError.text = @"Nhập số điện thoại";
        [_txtSdt becomeFirstResponder];
    }else if ([_txtEmail.text length]==0){
        _lblError.text = @"Nhập email";
        [_txtEmail becomeFirstResponder];
    }
    else if (![self checkIsValidEmail:_txtEmail.text]){
        _lblError.text = @"Email không hợp lệ";
        [_txtEmail becomeFirstResponder];
    }
    else {
        
        [self requestCreateAC];
    }
    
}

- (void) requestCreateAC
{
//    if (_delegate && [_delegate respondsToSelector:@selector(taoAC)]) {
//        [_delegate taoAC];
//    }
//    [AES128 initialize];
//    [self requestLogin:_lblTruyCao.text withPass:[AES128 AES128Encrypt:_tfMK.text]];
    
    [AES128 initialize];
    [self requestDangki:_txtUserName.text withPass:[AES128 AES128Encrypt:_txtMatkhau.text] withFullName:_txtHo.text withEmail:_txtEmail.text withMobie:_txtSdt.text];
}

-(BOOL) checkIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField.tag>3) {
        [UIView animateWithDuration:0.3f animations:^{self.view.transform = CGAffineTransformMakeTranslation(0,-100);}];
    }
    
     return YES;

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _lblError.font = [UIFont fontWithName:XSFontCondensed size:15];
     _txtEmail.font = [UIFont fontWithName:XSFontCondensed size:15];
     _txtHo.font = [UIFont fontWithName:XSFontCondensed size:15];
     _txtSdt.font = [UIFont fontWithName:XSFontCondensed size:15];
     _txtUserName.font = [UIFont fontWithName:XSFontCondensed size:15];
     _lblTitle.font = [UIFont fontWithName:XSFontCondensed size:15];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
