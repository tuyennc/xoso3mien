//
//  KetQuaVC.h
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECSlidingViewController.h"


@interface DetailChonVe : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *lblngay;
@property (strong, nonatomic) IBOutlet UILabel *lblTenTinh;

@property (strong, nonatomic) IBOutlet UIView *viewChua;
@property (strong, nonatomic) IBOutlet UILabel *tittleVC;
@property (strong, nonatomic) IBOutlet UINavigationBar *navTu;

@property (strong, nonatomic) IBOutlet UIButton *btnMienBac;
@property (strong, nonatomic) IBOutlet UIButton *btnMienTrung;
@property (strong, nonatomic) IBOutlet UIButton *btnMienNam;
@property (strong, nonatomic) IBOutlet UIImageView *imgSelectBut;
@property (strong, nonatomic) IBOutlet UITableView *tblKetQua;


@property (strong, nonatomic) IBOutlet UIButton *btnKQXS;
@property (strong, nonatomic) IBOutlet UIButton *btnLOTO;
@property (strong, nonatomic) IBOutlet UIButton *btnXSDT;
@property (strong, nonatomic) NSString *maTinhChon;


@property (strong, nonatomic) NSString *tenTinh;
@property (strong, nonatomic) NSString *ngayVe;
@property (strong, nonatomic) NSString *veSo;
@property (strong, nonatomic) NSDictionary *dictKetQUa;
@property (strong, nonatomic) NSMutableArray *isTrung;
@property (strong, nonatomic) IBOutlet UIView *viewTRung;
@property (strong, nonatomic) IBOutlet UILabel *detailVeso;

@property (strong, nonatomic) IBOutlet UILabel *veSoCuaBan;


@end
