//
//  DoSoVC.h
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DoSoVC : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *tittleVC;
@property (strong, nonatomic) IBOutlet UINavigationBar *navTu;

@property (strong, nonatomic) IBOutlet UIButton *btnTinh;
@property (strong, nonatomic) IBOutlet UITextField *tfCoupe;
@property (strong, nonatomic) IBOutlet UIButton *btnDoso;

@property (strong, nonatomic) IBOutlet UITableView *tblTinh;
@property (strong, nonatomic) IBOutlet UITableView *tblKQ;

@property (strong, nonatomic) IBOutlet UIButton *btnNhanh;
@property (strong, nonatomic) IBOutlet UIButton *btnDacBiet;
@property (strong, nonatomic) IBOutlet UIImageView *imgSelectBut;

@property (strong, nonatomic) IBOutlet UIButton *btnSendSMS;
@property (strong, nonatomic) IBOutlet UIButton *btnSendSMSTK;

- (IBAction)sendSMS:(id)sender ;
- (IBAction)thongKeFrom3:(id)sender ;
@property (strong, nonatomic) IBOutlet UILabel *lblGiaCuoc1;

@end
