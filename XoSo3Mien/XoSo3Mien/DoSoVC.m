//
//  DoSoVC.m
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import "DoSoVC.h"

@interface DoSoVC ()
{
    ECSlidingViewController  *slidingViewController;
    LogInOutVC   *logInOut;
    NSString *maVungChon;
    
    NSArray *arrTinh ;
    NSArray *arrMaTinh ;
    ASIHTTPRequest *request;
    BOOL isDacbiet;
    NSMutableDictionary *ketQua ;
     DangKiVC     *dangKi;
      AccountInfor   *accInfor;
}
@end

@implementation DoSoVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.slidingViewController resetTopView];
    self.navTu.topItem.title = @"";
    _tittleVC.text =@"Dò Số";
    _tittleVC.font = [UIFont fontWithName:XSFontCondensed size:20];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _btnDacBiet.backgroundColor =RGB(106, 0, 0);
    _btnNhanh.backgroundColor = RGB(181, 28, 28);
    
    
    _btnNhanh.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:17];
     _btnDacBiet.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:17];
    ketQua = [[NSMutableDictionary alloc] init];
    
    // Do any additional setup after loading the view from its nib.
    UIImage *imgItem =[ Utility resizeImg:[UIImage imageNamed:@"iconmenu.png"] withSize:CGSizeMake(30, 30*30/50)] ;
    
    self.navTu.topItem.leftBarButtonItem = [UIBarButtonItem barItemWithImage:imgItem target:self action:@selector(leftBtnClicked:)];
    if (IS_OS_7_OR_LATER) {
        UIView *viewSta = [[UIView alloc] initWithFrame:CGRectMake(0, -20, 320, 20)];
        viewSta.backgroundColor = RGB(106, 0, 0);
        
        [self.view addSubview:viewSta];
        self.navTu.barTintColor = RGB(106, 0, 0);
    }
    else
    {
        self.navTu.TintColor = RGB(106, 0, 0);
    }
    [Utility buttonXoSo:_btnTinh];
    [_btnTinh setTitle:@"+  Miền Bắc" forState:UIControlStateNormal];
    
    _tfCoupe.layer.borderColor = RGB(106, 0, 0).CGColor;
    _tfCoupe.layer.borderWidth = 1;
    _tfCoupe.font = [UIFont fontWithName:XSFontCondensed size:17];
    _tfCoupe.layer.cornerRadius = 10;
    _btnDoso.layer.cornerRadius = 10;
    _btnDoso.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:17];
    
    if (IS_OS_7_OR_LATER) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        [self setNeedsStatusBarAppearanceUpdate];
        [_tblTinh setSeparatorInset:UIEdgeInsetsZero];
        [_tblKQ setSeparatorInset:UIEdgeInsetsZero];
    }
    [_tblTinh setSeparatorColor:[UIColor grayColor]];
    arrMaTinh = [[Utility dsMaTinhCaNuoc] componentsSeparatedByString:@"#"];
    arrTinh = [[Utility dsTinhCaNuoc] componentsSeparatedByString:@"#"];
    maVungChon = @"XSTD";
    
    
    
    //    ecsliding
    slidingViewController = [[ECSlidingViewController alloc] init];
    MenuVC *menuVC   = [[MenuVC alloc] initWithNibName:@"MenuVC" bundle:nil];
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    self.slidingViewController.underLeftViewController  = menuVC;
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
//    sms
    
    [_btnSendSMS setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [_btnSendSMS setContentVerticalAlignment:UIControlContentVerticalAlignmentTop];
    [_btnSendSMS setTitleEdgeInsets:UIEdgeInsetsMake(6.0f, 30.0f, 0.0f, 0.0f)];
    
    
    _btnSendSMS.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:13];
    
    [_btnSendSMSTK setTitle:@"Thống kê" forState:UIControlStateNormal];
    [_btnSendSMSTK setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [_btnSendSMSTK setContentVerticalAlignment:UIControlContentVerticalAlignmentTop];
    [_btnSendSMSTK setTitleEdgeInsets:UIEdgeInsetsMake(10.0f, 30.0f, 0.0f, 0.0f)];
    
    [_btnSendSMS setTitle: [NSString stringWithFormat:@"Nhận KQXS %@",@"Miền Bắc"] forState:UIControlStateNormal];
    _btnSendSMSTK.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:17];
    _lblGiaCuoc1.text = [NSString stringWithFormat:@"%@ (1.000đ/ngày)",[Utility getToday]];
    _lblGiaCuoc1.font = [UIFont fontWithName:XSFontCondensed size:13];
    //    login
    UIButton *btnSelectDate=[UIButton buttonWithType:UIButtonTypeCustom];
    btnSelectDate.frame = CGRectMake(0, 0,30,30);
    
    if ([[AppDelegate sharedDelegate] isLogin]) {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"Logged.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    else
    {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"LogInOut.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    [btnSelectDate addTarget:self action:@selector(pushViewLogIO) forControlEvents:UIControlEventTouchUpInside];
    self.navTu.topItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSelectDate];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateImageLogin)
                                                 name:@"UPDATE_IMAGE_LOGIN"
                                               object:nil];
    
    
   
}
- (IBAction)leftBtnClicked:(id)sender
{
    [[[UIApplication sharedApplication]keyWindow]endEditing:YES];
    [self.slidingViewController anchorTopViewTo:ECRight];
}
#pragma mark
#pragma mark loginout delete

-(void)dangKi
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    dangKi  = [[DangKiVC alloc] initWithNibName:@"DangKiVC" bundle:nil]  ;
   dangKi.delegate = self;
    [self presentPopupViewController:dangKi animationType:MJPopupViewAnimationFade];
    
}
-(void)closePopUp
{
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
    
}
-(void)updateImageLogin
{
    UIButton *btnSelectDate=[UIButton buttonWithType:UIButtonTypeCustom];
    btnSelectDate.frame = CGRectMake(0, 0,30,30);
    
    if ([[AppDelegate sharedDelegate] isLogin]) {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"Logged.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    else
    {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"LogInOut.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    [btnSelectDate addTarget:self action:@selector(pushViewLogIO) forControlEvents:UIControlEventTouchUpInside];
    self.navTu.topItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSelectDate];
}

-(void)pushViewLogIO
{
    if ([[AppDelegate sharedDelegate ] isLogin]==NO) {
        logInOut  = [[LogInOutVC alloc] initWithNibName:@"LogInOutVC" bundle:nil]  ;
        logInOut.delegate = self;
        [self presentPopupViewController:logInOut animationType:MJPopupViewAnimationFade];
        
    }
    else
    {
        accInfor  = [[AccountInfor alloc] initWithNibName:@"AccountInfor" bundle:nil]  ;
        accInfor.delegate = self;
        [self presentPopupViewController:accInfor animationType:MJPopupViewAnimationFade];
        
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark
#pragma mark Request Thong ke
- (void)requestDoso
{
    
    if (_tfCoupe.text.length!=2) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Nhập đầy đủ cặp số" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        [[AppDelegate sharedDelegate].hudRequest show:YES];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://appsrv.sms.vn/app/xs?action=doso&code=%@&couple=%d",maVungChon,[_tfCoupe.text integerValue]]];
        NSLog(@"requset :%@",url);
        request = [ASIHTTPRequest requestWithURL:url];
        //       [ketQua removeAllObjects];
        
        [request setCompletionBlock:^{
            
            //     {"items":
            [[AppDelegate sharedDelegate].hudRequest hide:YES];
            // Use when fetching text data
            NSString *responseString = [request responseString];
            NSLog(@"%@",responseString);
            //          NSData *responseData = [request responseData];
            ketQua = [[responseString JSONValue] objectForKey:@"items"];
            if (![ketQua isKindOfClass:[NSString class]]) {
                
                [self.tblKQ reloadData];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Chưa có dữ liệu cho ngày này" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
                [alert show];
            }
            
        }];
        [request setFailedBlock:^{
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Không thể kết nối" delegate:self cancelButtonTitle:@"Kết nối lại" otherButtonTitles:@"Huỷ", nil] ;
            [alert show];
            //        [UIHelper showMessage:@"Không thể kết nối" withTitle:@""];
        }];
        [request startAsynchronous];
    }
}

#pragma mark
#pragma mark IBOutlet

- (IBAction)showTinh:(id)sender {
     [self.view endEditing:YES];
    _tblTinh.hidden = NO;
}

- (IBAction)Dòsố:(id)sender {
    
}


- (IBAction)tkdb:(id)sender {
    
    isDacbiet = YES;
//    _tittleVC.text =@"Thống Kê Đặc Biệt";
    
    //    isTKNhanh = NO;
    _btnDacBiet.backgroundColor = RGB(181, 28, 28);
    _btnNhanh.backgroundColor =RGB(106, 0, 0);
    _imgSelectBut.frame =CGRectMake(236, 0, 9, 9);
    [_tblKQ reloadData];
}

- (IBAction)tkloto:(id)sender {
    isDacbiet = NO;
//    _tittleVC.text =@"Dò số";
    
    //    isTKNhanh = YES;
    _btnDacBiet.backgroundColor = RGB(106, 0, 0);
    _btnNhanh.backgroundColor = RGB(181, 28, 28);
    _imgSelectBut.frame = CGRectMake(71, 0, 9, 9);
    [_tblKQ reloadData];
}
- (IBAction)doso:(id)sender {
    [self.view endEditing:YES];
    [self requestDoso];
}

#pragma mark
#pragma mark Tableview Delegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == _tblKQ) {
        if (isDacbiet==NO) {
            return (ketQua.count!=0)?4:0;

        }
        else
           return (ketQua.count!=0)?3:0;
           }
    else
    {
        return 1;
    }
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"";
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView==_tblKQ) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
        /* Create custom view to display section header... */
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
        [label setFont:[UIFont fontWithName:XSFontCondensed size:17]];
        int dau = [[_tfCoupe.text substringToIndex:1] integerValue];
           int cuoi = [[_tfCoupe.text substringFromIndex:1] integerValue];
        int tong = dau + cuoi;
        if (isDacbiet==NO) {
            if (section==0) {
                [label setText:@""];
            }
            else if (section ==1)
            {
                [label setText:[NSString stringWithFormat:@"Đầu %d",dau]];
            }
            else if (section ==2)
            {
                [label setText:[NSString stringWithFormat:@"Đít %d",cuoi]];
            }
            else if (section ==3)
            {
                [label setText:[NSString stringWithFormat:@"Tổng %d",tong]];
            }

        }
        else
        {
            if (section ==0)
            {
                [label setText:[NSString stringWithFormat:@"Đầu %d",dau]];
            }
            else if (section ==1)
            {
                [label setText:[NSString stringWithFormat:@"Đít %d",cuoi]];

            }
            else if (section ==2)
            {
               [label setText:[NSString stringWithFormat:@"Tổng %d",tong]];
            }

        }
        
        label.textAlignment = NSTextAlignmentCenter;
        
        [view addSubview:label];
        view.backgroundColor= RGB(216, 216, 216);
        return view;
    }
    return nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView==_tblKQ ) {
        if (isDacbiet==NO && section==0) {
            return 0;
        }
        else
        return 30;
    }
    else
        return 0;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==_tblTinh)
        
    {
        return arrMaTinh.count;
    }
    else
    {
        if (isDacbiet==NO) {
            if (section==0) {
                return 3;
            }
            else
                return 2;

        }
        else
            return 2;
           }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tblTinh)
    {
        return 40;
    }
    else
        return 30;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cellds4343";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
     if (cell == nil) {
    
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = RGB(243, 243, 243);
    
  }
    [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    if (tableView==_tblTinh) {
        UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, 92, 40)];
        lbl.text = [arrTinh objectAtIndex:indexPath.row];
        lbl.font = [UIFont fontWithName:XSFontCondensed size:17];
        lbl.textColor = [UIColor whiteColor];
        cell.backgroundColor = RGB(106, 0, 0);
        UIImage *img = [Utility resizeImg:[UIImage imageNamed:@"arr_chontinh.png"] withSize:CGSizeMake(8, 12)];
        cell.imageView.image = img;
        [cell.contentView addSubview:lbl];
    }
    else
    {
        
        
        UILabel *lblName = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 160, 30)];
        lblName.font = [UIFont fontWithName:XSFontCondensed size:17];
        lblName.textAlignment = NSTextAlignmentLeft;
        lblName.backgroundColor = RGB(243, 243, 243);
        lblName.textColor = RGB(39, 39, 39);
        
        UILabel *lblValue = [[UILabel alloc] initWithFrame:CGRectMake(160, 0, 160, 30)];
        lblValue.font = [UIFont fontWithName:XSFontCondensed size:17];
        lblValue.textAlignment = NSTextAlignmentLeft;
        lblValue.backgroundColor = RGB(243, 243, 243);
        lblValue.textColor = RGB(39, 39, 39);
        
        
        CALayer *bottomBorder = [CALayer layer];
        
        bottomBorder.frame = CGRectMake(0.0f, 0,1 , 30);
        
        bottomBorder.backgroundColor = [UIColor lightGrayColor].CGColor;
        [lblValue.layer addSublayer:bottomBorder];
        if (isDacbiet==NO) {
        
        if (indexPath.section==0) {
            if (indexPath.row == 0) {
                lblName.text = @"   Số lần xuất hiện";
                lblValue.text = [NSString stringWithFormat:@"   %@ lần", [ketQua objectForKey:@"countxh"]];
                
            }
            else  if (indexPath.row == 1) {
                lblName.text = @"   Ngày về gần nhất";
                lblValue.text = [NSString stringWithFormat:@"   %@", [ketQua objectForKey:@"date"]];
            }
            else  if (indexPath.row == 2) {
                lblName.text = @"   Nhịp";
                lblValue.text = [NSString stringWithFormat:@"   %@", [ketQua objectForKey:@"nhip"]];
            }
        }
        else if (indexPath.section==1)
        {
            if (indexPath.row == 0) {
                
                lblName.text = @"   Tổng xố lần lô xuất hiên";
                lblValue.text = [NSString stringWithFormat:@"   %@ con", [ketQua objectForKey:@"xhdau30"]];
                
            }
            else
            {
                NSString *str =[NSString stringWithFormat:@"   %@", [ketQua objectForKey:@"lastdatedau30"]];
                lblName.text = @"   Ngày hôm qua";
                lblValue.text = [str substringToIndex:str.length-2];
            }
            
        }
        else if (indexPath.section==2)
        {
            if (indexPath.row == 0) {
                lblName.text = @"   Tổng xố lần lô xuất hiên";
                lblValue.text = [NSString stringWithFormat:@"   %@ con", [ketQua objectForKey:@"xhduoi30"]];
                
            }
            else
            {
                NSString *str = [NSString stringWithFormat:@"   %@", [ketQua objectForKey:@"lastdateduoi30"]];
                lblName.text = @"   Ngày hôm qua";
                
                lblValue.text = [str substringToIndex:str.length-2];
            }
        }
        else if (indexPath.section==3)
        {
            if (indexPath.row == 0) {
                lblName.text = @"   Tổng xố lần lô xuất hiên";
                lblValue.text = [NSString stringWithFormat:@"   %@ con", [ketQua objectForKey:@"xhtong30"]];
                
            }
            else
            {
                NSString *str =[NSString stringWithFormat:@"   %@", [ketQua objectForKey:@"lastdatetong30"]];
                lblName.text = @"   Ngày hôm qua";
                
                lblValue.text = [str substringToIndex:str.length-2];
            }
        }
        }
        else
        {
            if (indexPath.section==0) {
                if (indexPath.row == 0) {
                    
                    lblName.text = @"   Số ngày chưa ra";
                    lblValue.text = [NSString stringWithFormat:@"   %@ ngày", [ketQua objectForKey:@"dauchuara"]];
                    
                }
                else
                {
                    NSString *str =[NSString stringWithFormat:@"   %@", [ketQua objectForKey:@"daungaygannhat"]];
                    lblName.text = @"   Ngày về gần nhất";
                    lblValue.text = str;
                }            }
            else if (indexPath.section==1)
            {
                if (indexPath.row == 0) {
                    
                    lblName.text = @"   Số ngày chưa ra";
                    lblValue.text = [NSString stringWithFormat:@"   %@ ngày", [ketQua objectForKey:@"duoichuara"]];
                    
                }
                else
                {
                    NSString *str =[NSString stringWithFormat:@"   %@", [ketQua objectForKey:@"duoingaygannhat"]];
                    lblName.text = @"   Ngày về gần nhất";
                    lblValue.text = str;
                }
                
            }
            else if (indexPath.section==2)
            {
                if (indexPath.row == 0) {
                    
                    lblName.text = @"   Số ngày chưa ra";
                    lblValue.text = [NSString stringWithFormat:@"   %@ ngày", [ketQua objectForKey:@"tongchuara"]];
                    
                }
                else
                {
                    NSString *str =[NSString stringWithFormat:@"   %@", [ketQua objectForKey:@"tongngaygannhat"]];
                    lblName.text = @"   Ngày về gần nhất";
                    lblValue.text = str;
                }
            }
        }
        [cell.contentView addSubview:lblName];
        [cell.contentView addSubview:lblValue];
        //        {"nhip":"xxxxxxxx1x2x11xxxxxxxxxxxxxxx1","dauchuara":"0","xhduoi30":"80","countxh":"6","xhtong30":"71","tongngaygannhat":"05\/02\/2014","lastdatetong30":"57, ","tongchuara":"2","code":"XSTD","lastdateduoi30":"31, 91, ","date":"26\/01\/2014","duoingaygannhat":"24\/01\/2014","daungaygannhat":"07\/02\/2014","duoichuara":"10","name":"","couple":"11","lastdatedau30":"16,17, 18","xhdau30":"86"}}
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    maVungChon = [arrMaTinh objectAtIndex:indexPath.row];
    _tblTinh.hidden = YES;
    [_btnTinh setTitle:[NSString stringWithFormat:@"+  %@",[arrTinh objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
    [_btnSendSMS setTitle: [NSString stringWithFormat:@"Nhận KQXS %@",[arrTinh objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
    if ([maVungChon isEqualToString:@"XSBDH"] || [maVungChon isEqualToString:@"XSTD"]) {
        _lblGiaCuoc1.text = [NSString stringWithFormat:@"%@ (1.000đ/ngày)",[Utility getToday]];
        
    }
    else
    {
        _lblGiaCuoc1.text = [NSString stringWithFormat:@"%@ (2.000đ/ngày)",[Utility getToday]];
    }
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 2) ? NO : YES;
}

- (IBAction)sendSMS:(id)sender {
    
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
    controller.delegate = self;
    
    if([MFMessageComposeViewController canSendText])
    {
        NSString *toNumber = nil;
        if ([Utility isViettel]) {
            toNumber = DS_VIETTEL_XS;
            if ([maVungChon isEqualToString:@"XSTD"]) {
                controller.body = [NSString stringWithFormat:@"%@",@"XSMB"];
            }
            else
                controller.body = [NSString stringWithFormat:@"%@",maVungChon];
            controller.recipients = [NSArray arrayWithObjects:toNumber, nil];
            
        }
        else
        {
            toNumber = DS_NOT_VIETTEL;
            NSString *strOr = [Utility dsMaTinhMienTrung];
            if ([strOr rangeOfString:maVungChon].location!=NSNotFound) {
                controller.body = [NSString stringWithFormat:@"DK %@",@"XSMT"];
                
            }
            else
            {
                controller.body = [NSString stringWithFormat:@"DK %@",@"XSMN"];
            }
            if ([maVungChon isEqualToString:@"XSTD"]) {
                controller.body = [NSString stringWithFormat:@"DK %@",@"XSMB"];
            }
            if ([maVungChon isEqualToString:@"XSBDH"]) {
                controller.body = [NSString stringWithFormat:@"DK %@",@"XSBDH"];
            }
            controller.recipients = [NSArray arrayWithObjects:toNumber, nil];
            
        }
        controller.messageComposeDelegate = self;
        [self presentModalViewController:controller animated:YES];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
	switch (result) {
		case MessageComposeResultCancelled:
			NSLog(@"Cancelled");
			break;
		case MessageComposeResultFailed:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Không gửi được tin nhắn" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
        }
			break;
		case MessageComposeResultSent:
        {UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thành công" message:@"Gửi tin nhắn thành công" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
        }
			break;
		default:
			break;
	}
    
	[self dismissModalViewControllerAnimated:YES];
}

#pragma ---
#pragma mark

- (IBAction)thongKeFrom3:(id)sender {
    
    ThongKeVC *mainView = [[ThongKeVC alloc] initWithNibName:@"ThongKeVC" bundle:nil];            CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = mainView;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
@end
