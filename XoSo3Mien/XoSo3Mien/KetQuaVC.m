//
//  KetQuaVC.m
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import "KetQuaVC.h"
#import "MenuVC.h"
#import "LogInOutVC.h"
#import "AccountInfor.h"
#import "ChonLichSubView.h"
#import "ChonTinhSubView.h"
#import <AVFoundation/AVFoundation.h>

@interface KetQuaVC ()<LogInOutDelegate,ChonLichDelegate,ChonTinhDelegate,SlaveDelegate,AccountInforDelegate,DangKyDelegate,AVAudioPlayerDelegate, UINavigationControllerDelegate,MFMessageComposeViewControllerDelegate>
{
    ECSlidingViewController  *slidingViewController;
    LogInOutVC   *logInOut;
    AccountInfor   *accInfor;
    DangKiVC     *dangKi;
    ASIHTTPRequest *request;
    
    ChonLichSubView *chonLichView;
    ChonTinhSubView *chonTinhView;
    NSDictionary *dictKetQuaxS;
    NSDictionary *dictKetQuaxSDT;
    NSString *maTinhChon;
    
      NSString *nameTinhChon;
    BOOL isPlaying;
    
    
    NSString  *ngayChon;
    NSString  *maVungChon;
    UITapGestureRecognizer *gestureTab;
    
    
    NSMutableArray *arrLOTO;
    
     NSMutableArray *arrDSTinh;
    NSMutableArray *arrDSNameTinh;
    int typeLoc ;// 0 :ket qua -  1: loto - 2: dientaon
    
   AVAudioPlayer*soundTask;
     AVQueuePlayer *queuePlayer;
    
    NSMutableArray *arrSoundQueue;
 
}
@end

@implementation KetQuaVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateImageLogin)
                                                 name:@"UPDATE_IMAGE_LOGIN"
                                               object:nil];

    

    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    _lblGiaCuoc1.text = [NSString stringWithFormat:@"%@ (1.000đ/ngày)",[Utility getToday]];
    _lblGiaCuoc1.font = [UIFont fontWithName:XSFontCondensed size:13];
    _lblGiaCuoc2.text = [NSString stringWithFormat:@"%@ (1.000đ/ngày)",[Utility getToday]];
    _lblGiaCuoc2.font = [UIFont fontWithName:XSFontCondensed size:13];
    
    if (IS_OS_7_OR_LATER) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        [self setNeedsStatusBarAppearanceUpdate];
        [_tblKetQua setSeparatorInset:UIEdgeInsetsZero];
    }
    [_tblKetQua setSeparatorColor:[UIColor grayColor]];

    arrSoundQueue = [[NSMutableArray alloc] init];
    _btnKQXS.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:17];
    _btnLOTO.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:17];
    _btnXSDT.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:17];
    _btnMienBac.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:20];
    _btnMienTrung.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:20];

    _btnMienNam.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:20];

    _lblngay.font = [UIFont fontWithName:XSFontCondensed size:15];
    _lblTenTinh.font = [UIFont fontWithName:XSFontCondensed size:14];
    [_btnSendSMS setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [_btnSendSMS setContentVerticalAlignment:UIControlContentVerticalAlignmentTop];
    [_btnSendSMS setTitleEdgeInsets:UIEdgeInsetsMake(6.0f, 30.0f, 0.0f, 0.0f)];
    

    _btnSendSMS.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:13];
    
    [_btnSendSMSTK setTitle:@"Thống kê" forState:UIControlStateNormal];
    [_btnSendSMSTK setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [_btnSendSMSTK setContentVerticalAlignment:UIControlContentVerticalAlignmentTop];
    [_btnSendSMSTK setTitleEdgeInsets:UIEdgeInsetsMake(10.0f, 30.0f, 0.0f, 0.0f)];
    
    
    _btnSendSMSTK.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:17];
    
    //    navbar
//    UIImage *imgItem = [UIImage imageNamed:@"iconmenu.png"];
//    CGSize asad =asd.size;
    UIImage *imgItem =[ Utility resizeImg:[UIImage imageNamed:@"iconmenu.png"] withSize:CGSizeMake(30, 30*30/50)] ;
    
    self.navTu.topItem.leftBarButtonItem = [UIBarButtonItem barItemWithImage:imgItem target:self action:@selector(leftBtnClicked:)];
     if (IS_OS_7_OR_LATER) {
    self.navTu.barTintColor = RGB(106, 0, 0);
     }
    else
    {
        self.navTu.TintColor = RGB(106, 0, 0);
    }
    
    //    ecsliding
    slidingViewController = [[ECSlidingViewController alloc] init];
    MenuVC *menuVC   = [[MenuVC alloc] initWithNibName:@"MenuVC" bundle:nil];
//    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    self.slidingViewController.underLeftViewController  = menuVC;
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
//    add subview
    
    gestureTab = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideViewChua:)];
   
    [_viewChua setUserInteractionEnabled:YES];
    [_viewChua addGestureRecognizer:gestureTab];
    _viewChua.hidden = YES;
    
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ChonLichSubView" owner:self options:nil];
    chonLichView = (ChonLichSubView *)[nib objectAtIndex:0];
    chonLichView.frame = CGRectMake(0, 85, 210, 239);
    chonLichView.userInteractionEnabled = YES;
    [self.viewChua addSubview:chonLichView];
    chonLichView.delegate = self;
    
    
    NSArray *nibTinh = [[NSBundle mainBundle] loadNibNamed:@"ChonTinhSubView" owner:self options:nil];
    
   chonTinhView = (ChonTinhSubView *)[nibTinh objectAtIndex:0];
    chonTinhView.frame = CGRectMake(228, 79, 92, 200);
    chonTinhView.userInteractionEnabled = YES;
    [self.viewChua addSubview:chonTinhView];
    chonTinhView.delegate = self;
    
    _lblngay.text = [NSString stringWithFormat:@"Ngày %@",[Utility getToday]];
    ngayChon =[Utility getToday] ;
    maTinhChon = @"XSTD";
    maVungChon = @"MB";
    typeLoc = 0;
      _btnMienBac.backgroundColor = RGB(181, 28, 28);
     [_btnKQXS setTitleColor:RGB(106, 0, 0) forState:UIControlStateNormal];
    [self requestXSdate:ngayChon withRegion:maVungChon];
    arrDSTinh = [[NSMutableArray alloc] init];
     arrDSNameTinh = [[NSMutableArray alloc] init];
    
    
    SlaveServerConnect *slaveConnect=[[SlaveServerConnect alloc] init];
//    if (slaveConnect==NULL) {
//        slaveConnect=[SlaveServerConnect alloc];
        [Utility setSlaveConect:slaveConnect];
//    }
    slaveConnect.delegate = self;

    [slaveConnect connect:nil];
    
}

- (void)hideViewChua:(UITapGestureRecognizer*)tap
{
      _viewChua.hidden = YES;
}

- (IBAction)leftBtnClicked:(id)sender
{
    [[[UIApplication sharedApplication]keyWindow]endEditing:YES];
    [self.slidingViewController anchorTopViewTo:ECRight];
}
-(void)pushViewLogIO
{
    if ([[AppDelegate sharedDelegate ] isLogin]==NO) {
        logInOut  = [[LogInOutVC alloc] initWithNibName:@"LogInOutVC" bundle:nil]  ;
        logInOut.delegate = self;
        [self presentPopupViewController:logInOut animationType:MJPopupViewAnimationFade];

    }
    else
    {
        accInfor  = [[AccountInfor alloc] initWithNibName:@"AccountInfor" bundle:nil]  ;
        accInfor.delegate = self;
        [self presentPopupViewController:accInfor animationType:MJPopupViewAnimationFade];
        
    }
   }
- (void)viewWillAppear:(BOOL)animated {
    
//    self.navTu.tintColor = [UIColor whiteColor];
    [super viewWillAppear:animated];
	self.navigationController.navigationBar.hidden = YES;
}

- (void) viewWillDisappear:(BOOL)animated{
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
    [super viewWillDisappear:animated];
  }
-(void) viewDidAppear:(BOOL)animated{
    
     SJLog(@"%s", __FUNCTION__);
    [super viewDidAppear:animated];
    self.navTu.topItem.title = @"";
    _tittleVC.text =@"Kết quả sổ xố miền bắc";
   _tittleVC.font = [UIFont fontWithName:XSFontCondensed size:20];

    
    
    UIButton *btnSelectDate=[UIButton buttonWithType:UIButtonTypeCustom];
    btnSelectDate.frame =CGRectMake(0, 0,30,30);
    
    if ([[AppDelegate sharedDelegate] isLogin]) {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"Logged.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    else
    {
    [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"LogInOut.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    [btnSelectDate addTarget:self action:@selector(pushViewLogIO) forControlEvents:UIControlEventTouchUpInside];
    self.navTu.topItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSelectDate];
    
    if (IS_OS_7_OR_LATER) {

        self.navigationController.view.backgroundColor = [UIColor whiteColor];
   
        UIView *viewSta = [[UIView alloc] initWithFrame:CGRectMake(0, -20, 320, 20)];
        viewSta.backgroundColor = RGB(106, 0, 0);
        
        [self.view addSubview:viewSta];
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        self.view.frame =  CGRectMake(0,20,self.view.frame.size.width,screenRect.size.height-20);
        
    }
    
//    [self.slidingViewController resetTopView];
    
    
    //    btn(162) tren .co mau :(106.0.0) .181.28.28 btn select
    
    
}

#pragma mark 
#pragma mark Request ket qua
- (void)requestXSdate:(NSString*)date withRegion:(NSString*)region
{
    
    [[AppDelegate sharedDelegate].hudRequest show:YES];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://appsrv.sms.vn/app/xs?action=getkqxsbyregion&date=%@&region=%@",date,region]];
    NSLog(@"requset :%@",url);
    request = [ASIHTTPRequest requestWithURL:url];
    
    [request setCompletionBlock:^{
        
        
        
          [[AppDelegate sharedDelegate].hudRequest hide:YES];
        // Use when fetching text data
        NSString *responseString = [request responseString];
        NSLog(@"%@",responseString);
        //          NSData *responseData = [request responseData];
        NSDictionary *responseDict = [[responseString JSONValue] objectForKey:@"items"];
        if (![responseDict isKindOfClass:[NSString class]]) {
            NSMutableArray *arrResulft = [responseDict objectForKey:@"item"];
            [arrDSTinh removeAllObjects];
            [arrDSNameTinh removeAllObjects];
            for (NSDictionary *dict in arrResulft) {
                [arrDSTinh addObject:[dict objectForKey:@"code"]];
                 [arrDSNameTinh addObject:[dict objectForKey:@"name"]];
            }
            
            maTinhChon = [[arrResulft objectAtIndex:0] objectForKey:@"code"];
            int hegth =40*arrDSTinh.count;
            chonTinhView.frame = CGRectMake(228, 85, 92,hegth );//40*arrDSTinh.count
            
            chonTinhView.arrNameTinh = arrDSNameTinh;
            chonTinhView.arrCodeTinh = arrDSTinh;
          
            dictKetQuaxS = [arrResulft objectAtIndex:0];
            _lblngay.text = [NSString stringWithFormat:@"Ngày %@",[responseDict objectForKey:@"date"]];
            _lblTenTinh.text = [NSString stringWithFormat:@"%@",[dictKetQuaxS objectForKey:@"name"]];
              [_btnSendSMS setTitle: [NSString stringWithFormat:@"Nhận KQXS %@",[dictKetQuaxS objectForKey:@"name"]] forState:UIControlStateNormal];
            
            _tittleVC.text =[NSString stringWithFormat:@"%@",[dictKetQuaxS objectForKey:@"name"]];
            
            arrLOTO = [self getLOTO:dictKetQuaxS];
            
          
            [self.tblKetQua reloadData];
        }
        else
        {
          
        
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Chưa có dữ liệu cho ngày này" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
            [alert show];
        }
      
           }];
    [request setFailedBlock:^{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Không thể kết nối" delegate:self cancelButtonTitle:@"Kết nối lại" otherButtonTitles:@"Huỷ", nil] ;
        [alert show];
        //        [UIHelper showMessage:@"Không thể kết nối" withTitle:@""];
    }];
    [request startAsynchronous];

}

- (void)requestXSdate:(NSString*)date withTinh:(NSString*)tinh
{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://appsrv.sms.vn/app/xs?action=getkqxs&sdate=%@&edate=%@&code=%@",date,date,tinh]];
     NSLog(@"requset :%@",url);
    request = [ASIHTTPRequest requestWithURL:url];
    
    [request setCompletionBlock:^{
        // Use when fetching text data
        NSString *responseString = [request responseString];
        NSLog(@"%@",responseString);
        //          NSData *responseData = [request responseData];
        NSDictionary *responseDict = [[responseString JSONValue] objectForKey:@"items"];
        if (![responseDict isKindOfClass:[NSString class]]) {
        NSMutableArray *arrResulft = [responseDict objectForKey:@"item"];
        dictKetQuaxS = [arrResulft objectAtIndex:0];
            _lblTenTinh.text = [NSString stringWithFormat:@"%@",[responseDict objectForKey:@"name"]];
            [_btnSendSMS setTitle: [NSString stringWithFormat:@"Nhận kết quả %@",[responseDict objectForKey:@"name"]] forState:UIControlStateNormal];
      _tittleVC.text = [NSString stringWithFormat:@"%@",[responseDict objectForKey:@"name"]];
         _lblngay.text = [NSString stringWithFormat:@"Ngày %@",[dictKetQuaxS objectForKey:@"date"]];
        arrLOTO = [self getLOTO:dictKetQuaxS];
        
        
        [self.tblKetQua reloadData];
        }
        else
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Chưa có dữ liệu cho ngày này" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
            [alert show];
        }
        

    }];
    [request setFailedBlock:^{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Không thể kết nối" delegate:self cancelButtonTitle:@"Kết nối lại" otherButtonTitles:@"Huỷ", nil] ;
        [alert show];
        //        [UIHelper showMessage:@"Không thể kết nối" withTitle:@""];
    }];
    [request startAsynchronous];
    
}

- (void)requestXSDT:(NSString*)date
{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://appsrv.sms.vn/app/xs?action=getxsdttt&date=%@",date]];
     NSLog(@"requset :%@",url);
    request = [ASIHTTPRequest requestWithURL:url];
    
    [request setCompletionBlock:^{
        // Use when fetching text data
        NSString *responseString = [request responseString];
        NSLog(@"%@",responseString);
        //          NSData *responseData = [request responseData];
        NSDictionary *responseDict =(NSDictionary*) [[responseString JSONValue] objectForKey:@"items"];
   
        dictKetQuaxSDT = responseDict;
        if (![responseDict isKindOfClass:[NSString class]]) {
            [self.tblKetQua reloadData];
            
        }
        else
        {
//                dictKetQuaxSDT = nil;
               [self.tblKetQua reloadData];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Chưa có dữ liệu cho ngày này" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
            [alert show];
        }

            }];
    [request setFailedBlock:^{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Không thể kết nối" delegate:self cancelButtonTitle:@"Kết nối lại" otherButtonTitles:@"Huỷ", nil] ;
        [alert show];
        //        [UIHelper showMessage:@"Không thể kết nối" withTitle:@""];
    }];
    [request startAsynchronous];
    
}

-(NSMutableArray*)getLOTO:(NSDictionary*)dict
{
    NSMutableArray* arrLoto = [[NSMutableArray alloc] init];
  
  NSString *strGiai = @"";
    strGiai = [strGiai stringByAppendingString:[NSString stringWithFormat:@"%@-",[dictKetQuaxS objectForKey:@"DB"]]];
   strGiai = [strGiai stringByAppendingString:[NSString stringWithFormat:@"%@-",[dictKetQuaxS objectForKey:@"G1"]]];
   strGiai = [strGiai stringByAppendingString:[NSString stringWithFormat:@"%@-",[dictKetQuaxS objectForKey:@"G2"]]];
    strGiai = [strGiai stringByAppendingString:[NSString stringWithFormat:@"%@-",[dictKetQuaxS objectForKey:@"G3"]]];
    strGiai = [strGiai stringByAppendingString:[NSString stringWithFormat:@"%@-",[dictKetQuaxS objectForKey:@"G4"]]];
    strGiai = [strGiai stringByAppendingString:[NSString stringWithFormat:@"%@-",[dictKetQuaxS objectForKey:@"G5"]]];
    strGiai = [strGiai stringByAppendingString:[NSString stringWithFormat:@"%@-",[dictKetQuaxS objectForKey:@"G6"]]];
    strGiai = [strGiai stringByAppendingString:[NSString stringWithFormat:@"%@-",[dictKetQuaxS objectForKey:@"G7"]]];
    strGiai = [strGiai stringByAppendingString:[NSString stringWithFormat:@"%@-",[dictKetQuaxS objectForKey:@"G8"]]];
//    strGiai = [strGiai stringByAppendingString:[NSString stringWithFormat:@"%@-",[dictKetQuaxS objectForKey:@"G8"]]];

    
   NSArray *arr =  [strGiai componentsSeparatedByString:@"-"];
    for (NSString *str in arr) {
        if (str.length==5) {
            [arrLoto addObject:[str substringFromIndex:3]];

        }
         else if(str.length==4)
         {
              [arrLoto addObject:[str substringFromIndex:2]];
         }
         else if(str.length==3)
         {
             [arrLoto addObject:[str substringFromIndex:1]];
         }
        else if(str.length==2)
        {
           [arrLoto addObject:str];
        }
             }
    
    return arrLoto;
}
- (NSString*)getDauLoto:(NSString*)strNumber
{
    NSString *resuft = @"";
    for (NSString*strHai in arrLOTO) {
        if (strHai.length==2) {
            if ([strNumber isEqualToString:[strHai substringToIndex:1]]) {
                resuft = [resuft stringByAppendingString:[NSString stringWithFormat:@"%@,",strHai]];
            }
        }
        
    }
    if ([resuft isEqualToString:@""]) {
        return @"";
    }
    else
        return [resuft substringToIndex:resuft.length-1];
}

- (NSString*)getDuoiLoto:(NSString*)strNumber
{
    NSString *resuft = @"";
    for (NSString*strHai in arrLOTO) {
        if (strHai.length==2) {
            if ([strNumber isEqualToString:[strHai substringFromIndex:1]]) {
                resuft = [resuft stringByAppendingString:[NSString stringWithFormat:@"%@,",strHai]];
            }
        }
        
    }
    if ([resuft isEqualToString:@""]) {
        return @"";
    }
    else
        return [resuft substringToIndex:resuft.length-1];
}


- (NSString*)chenSo:(NSString*)strNumber
{
//    3|2|3|2|3|-|3|2323-3232
    NSString *strG1 = @"";
    for (int i = 0 ; i< strNumber.length; i++) {
        NSString *charSub = [strNumber substringWithRange:NSMakeRange(i, 1)];
            charSub = [NSString stringWithFormat:@"%@|",charSub];
        strG1 = [strG1 stringByAppendingString:charSub];
    }
    return [strG1 substringToIndex:strG1.length-1];
}

-(void)getSound
{
    
    [arrSoundQueue removeAllObjects];
    NSString *str = @"Xs_kqxs|";
    str = [str stringByAppendingString:[NSString stringWithFormat:@"%@|",maTinhChon]];
    
    NSArray *arrNgay = [ngayChon componentsSeparatedByString:@"/"];
   
        str = [str stringByAppendingString:[NSString stringWithFormat:@"N%@|",[arrNgay objectAtIndex:0]]];
     str = [str stringByAppendingString:[NSString stringWithFormat:@"T%@|",[arrNgay objectAtIndex:1]]];
   
    
     NSString *g1 = [dictKetQuaxS objectForKey:@"G1"];
     NSString *g2 = [dictKetQuaxS objectForKey:@"G2"];
     NSString *g3 = [dictKetQuaxS objectForKey:@"G3"];
     NSString *g4 = [dictKetQuaxS objectForKey:@"G4"];
     NSString *g5 = [dictKetQuaxS objectForKey:@"G5"];
     NSString *g6 = [dictKetQuaxS objectForKey:@"G6"];
     NSString *g7 = [dictKetQuaxS objectForKey:@"G7"];
     NSString *g8 = [dictKetQuaxS objectForKey:@"G8"];
     NSString *gDB = [dictKetQuaxS objectForKey:@"DB"];
   
    if (g1.length>0) {
        str = [str stringByAppendingString:[NSString stringWithFormat:@"G1|%@|-|",[self chenSo:g1]]];
    
    }
    if (g2.length>0 ) {
        str = [str stringByAppendingString:[NSString stringWithFormat:@"G2|%@|-|",[self chenSo:g2]]];

    }
    if (g3.length>0 ) {
          str = [str stringByAppendingString:[NSString stringWithFormat:@"G3|%@|-|",[self chenSo:g3]]];
    }
    if (g4.length>0 ) {
         str = [str stringByAppendingString:[NSString stringWithFormat:@"G4|%@|-|",[self chenSo:g4]]];
    }
    if (g5.length>0) {
         str = [str stringByAppendingString:[NSString stringWithFormat:@"G5|%@|-|",[self chenSo:g5]]];
    }
    if (g6.length>0) {
        str = [str stringByAppendingString:[NSString stringWithFormat:@"G6|%@|-|",[self chenSo:g6]]];

    }
    if (g7.length>0 ) {
         str = [str stringByAppendingString:[NSString stringWithFormat:@"G7|%@|-|",[self chenSo:g7]]];
    }
    
    if (g8.length>0) {
       str = [str stringByAppendingString:[NSString stringWithFormat:@"G8|%@|-|",[self chenSo:g8]]];
    }
   
    if (gDB.length>0) {
        str = [str stringByAppendingString:[NSString stringWithFormat:@"DB|%@|",[self chenSo:gDB]]];
        
    }
   
    NSLog(@"chuoi final %@",str);
    
   str = [str stringByReplacingOccurrencesOfString:@"-" withString:@"beep1"];
//     NSLog(@"chuoi final %@",str);
    NSArray *arrStrSound = [[str substringToIndex:str.length-1] componentsSeparatedByString:@"|"];
    
    
    int i = 0;
    for (NSString *soundName in arrStrSound) {
        i++;
        
      
        AVPlayerItem *firstVideoItem = [[AVPlayerItem playerItemWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:  [[AppDelegate sharedDelegate].plistDict objectForKey:soundName] ofType:@"mp3"]]] copy];
        [arrSoundQueue addObject:firstVideoItem];
    }
    
    }

#pragma mark
#pragma mark slaveDelegete

- (NSDictionary*)convertSocket2Http:(NSString*)resuft
{
     SJLog(@"%s", __FUNCTION__);
    NSMutableDictionary *dictConvert=[[NSMutableDictionary alloc] init];
    NSArray *arr = [resuft componentsSeparatedByString:@"|"];
    NSString *giai1 = [[arr objectAtIndex:5] substringFromIndex:2];
    NSString *giai2 = [[arr objectAtIndex:6] substringFromIndex:2];
    NSString *giai3 = [[arr objectAtIndex:7] substringFromIndex:2];
    NSString *giai4 = [[arr objectAtIndex:8] substringFromIndex:2];
    NSString *giai5 = [[arr objectAtIndex:9] substringFromIndex:2];
    NSString *giai6 = [[arr objectAtIndex:10] substringFromIndex:2];
    NSString *giai7 = [[arr objectAtIndex:11] substringFromIndex:2];
    NSString *giai8 = [[arr objectAtIndex:12] substringFromIndex:2];
    NSString *giai0 = [[arr objectAtIndex:13] substringFromIndex:2];
    [dictConvert setObject:giai1 forKey:@"G1"];
    [dictConvert setObject:giai2 forKey:@"G2"];
    [dictConvert setObject:giai3 forKey:@"G3"];
    [dictConvert setObject:giai4 forKey:@"G4"];
    [dictConvert setObject:giai5 forKey:@"G5"];
    [dictConvert setObject:giai6 forKey:@"G6"];
    [dictConvert setObject:giai7 forKey:@"G7"];
    [dictConvert setObject:giai8 forKey:@"G8"];
    [dictConvert setObject:giai0 forKey:@"DB"];
    [dictConvert setObject:[arr objectAtIndex:3]  forKey:@"code"];
//    DB = 19584;
//    G1 = 71078;
//    G2 = "91300-23097";
//    G3 = "28683-02264-43174-23728-43189-69145";
//    G4 = "1933-4639-3068-4792";
//    G5 = "7590-5482-0069-8087-9955-1515";
//    G6 = "328-115-863";
//    G7 = "13-62-33-46";
//    G8 = "";
//    code = XSTD;
//    name = "TH\U1ee6 \U0110\U00d4";
//    
//    XSSYNC_RESP|0|25/01/2014 19:51:28|XSTD|25/01/2014 18:26:41|1:71078|2:91300-23097|3:28683-02264-43174-23728-43189-69145|4:1933-4639-3068-4792|5:7590-5482-0069-8087-9955-1515|6:328-115-863|7:17-62-33-46|8:|0:19584
    NSDictionary *dic = dictConvert;
    return dic;
}

-(void)data_XSSYNC_RESP_Recieved:(NSString *)resuft

{
     SJLog(@"%s", __FUNCTION__);
    NSArray *arr = [resuft componentsSeparatedByString:@"|"];
    if ([maTinhChon isEqualToString:[arr objectAtIndex:3]] && typeLoc != 2) {
//         NSLog(@"tuyen ,%@",resuft);
       dictKetQuaxS =  [self convertSocket2Http:resuft];
         arrLOTO = [self getLOTO:dictKetQuaxS];
        [_tblKetQua reloadData];
    }
     _lblngay.text = [NSString stringWithFormat:@"Ngày %@",[Utility getToday]];
//    NSLog(@"kekeke ,%@",resuft);
    
  

}

#pragma mark
#pragma mark chonLich && chonTinh delegate

- (void) chonNgay:(NSString *)date
{
    chonLichView.hidden = YES;
    _lblngay.text =[NSString stringWithFormat:@"Ngày %@",date];
    ngayChon = date;
    
    if (typeLoc==2) {
        [self requestXSDT:date];
    }
    else
    {
        if ([maTinhChon isEqualToString:@"XSMT"] || [maTinhChon isEqualToString:@"XSMN"] || [maTinhChon isEqualToString:@"XSTD"]) {
            [self requestXSdate:ngayChon withRegion:maVungChon];
            
        }
        else
        {
            [self requestXSdate:ngayChon withTinh:maTinhChon];
        }
    }
}

- (void) maTinhChon:(NSString *)maTinh withName:(NSString *)nameTinh
{
    maTinhChon = maTinh;
    if ([maTinh isEqualToString:@"XSMT"] || [maTinh isEqualToString:@"XSMN"]) {
       [self requestXSdate:ngayChon withRegion:maVungChon];

    }
    else
    {
    [self requestXSdate:ngayChon withTinh:maTinhChon];
    }
    
    if ([maTinh isEqualToString:@"XSBDH"] || [maTinh isEqualToString:@"XSTD"]) {
        _lblGiaCuoc1.text = [NSString stringWithFormat:@"%@ (1.000đ/ngày)",[Utility getToday]];

    }
    else
    {
         _lblGiaCuoc1.text = [NSString stringWithFormat:@"%@ (2.000đ/ngày)",[Utility getToday]];
    }
}
#pragma mark
#pragma mark loginout delete

-(void)dangKi
{
     [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    dangKi  = [[DangKiVC alloc] initWithNibName:@"DangKiVC" bundle:nil]  ;
    dangKi.delegate = self;
    [self presentPopupViewController:dangKi animationType:MJPopupViewAnimationFade];

}
-(void)closePopUp
{
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
   
    
}
-(void)updateImageLogin
{
    UIButton *btnSelectDate=[UIButton buttonWithType:UIButtonTypeCustom];
    btnSelectDate.frame = CGRectMake(0, 0,30,30);
    
    if ([[AppDelegate sharedDelegate] isLogin]) {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"Logged.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    else
    {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"LogInOut.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    [btnSelectDate addTarget:self action:@selector(pushViewLogIO) forControlEvents:UIControlEventTouchUpInside];
    self.navTu.topItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSelectDate];
}


#pragma mark
#pragma mark Avsound delegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    NSLog(@"finide");
}

#pragma mark
#pragma mark IBOulet


- (IBAction)docKetQua:(id)sender {
    

//    SlaveServerConnect *slaveConnect=[[SlaveServerConnect alloc] init];
////    if (slaveConnect==NULL) {
////        slaveConnect=[SlaveServerConnect alloc];
////        [Utility setSlaveConect:slaveConnect];
////    }
//    slaveConnect.delegate = self;
//     NSString *requestContent=[NSString stringWithFormat:@"XSSYNC_REQ|%@|%@",[AppDelegate sharedDelegate].appID ,[Utility getTodayFullMin]];
//   
//      [slaveConnect mainconnect:requestContent];
    
    if (queuePlayer.rate==0) {
        _imgKetQua.image = [UIImage imageNamed:@"btn_ketqua_lick.png"];
    
      [self getSound];
        if (queuePlayer==nil) {
            queuePlayer = [[AVQueuePlayer alloc] initWithItems:[arrSoundQueue copy]];
        }
       
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playerItemDidReachEnd:)
                                                     name:AVPlayerItemDidPlayToEndTimeNotification
                                                   object:[[arrSoundQueue copy] lastObject]];
//        queuePlayer = [AVQueuePlayer queuePlayerWithItems:[arrSoundQueue copy]];
        //    [self.mPlaybackView setPlayer:self.queuePlayer];
       
            [queuePlayer play];
        }
        else
        {
             [queuePlayer pause];
            _imgKetQua.image = [UIImage imageNamed:@"btn_ketqua.png"];

        }
    
   
//}
//    else
//    {
//        queuePlayer.rate =0;
//        [queuePlayer play];
//        
//    }

    
    
    
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
   
    NSLog(@"IT REACHED THE END");
    queuePlayer = nil;
    _imgKetQua.image = [UIImage imageNamed:@"btn_ketqua.png"];


}

- (IBAction)showLich:(id)sender {
    
     [gestureTab setCancelsTouchesInView:YES];
    chonTinhView.hidden = YES;
    chonLichView.hidden = NO;
    _viewChua.hidden = NO;
}

- (IBAction)ngheKetQua:(id)sender {
}

- (IBAction)chonTinh:(id)sender {
     [gestureTab setCancelsTouchesInView:NO];
    if (![maVungChon isEqualToString:@"MB"]) {
//        if ([maVungChon isEqualToString:@"MT"]) {
//             chonTinhView.typeRegion = 0;
//        }
//       else
//            chonTinhView.typeRegion = 1;
        chonLichView.hidden = YES;
        chonTinhView.hidden = NO;
        _viewChua.hidden = NO;
    }
   
}
//---------
- (IBAction)sxMienBac:(id)sender {
    
    _lblGiaCuoc1.text = [NSString stringWithFormat:@"%@ (1.000đ/ngày)",[Utility getToday]];
    _lblGiaCuoc1.font = [UIFont fontWithName:XSFontCondensed size:13];
    _lblGiaCuoc2.text = [NSString stringWithFormat:@"%@ (1.000đ/ngày)",[Utility getToday]];
    _lblGiaCuoc2.font = [UIFont fontWithName:XSFontCondensed size:13];
    
     maVungChon = @"MB";    
    _btnMienBac.backgroundColor = RGB(181, 28, 28);
    _btnMienTrung.backgroundColor = RGB(106, 0, 0);
    _btnMienNam.backgroundColor = RGB(106, 0, 0);
    _imgSelectBut.frame = CGRectMake(45, 0, 11, 11);
    
    if (typeLoc==0) {
        
        [_btnKQXS setTitleColor:RGB(106, 0, 0) forState:UIControlStateNormal];
        [_btnLOTO setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [_btnXSDT setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    }
    else if (typeLoc==1)
    {
        [_btnKQXS setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [_btnLOTO setTitleColor:RGB(106, 0, 0) forState:UIControlStateNormal];
        [_btnXSDT setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    }
    _btnKQXS.frame = CGRectMake(0, 0, 106, 40);
    _btnLOTO.frame = CGRectMake(107, 0, 106, 40);
    _btnXSDT.frame = CGRectMake(212, 0, 106, 40);
    
    _btnXSDT.hidden = NO;
    
  
        [self requestXSdate:ngayChon withRegion:maVungChon];
        
    
 
    
}

- (IBAction)sxMienTrung:(id)sender {
    
    _lblGiaCuoc1.text = [NSString stringWithFormat:@"%@ (2.000đ/ngày)",[Utility getToday]];
    _lblGiaCuoc1.font = [UIFont fontWithName:XSFontCondensed size:13];
    
    _lblGiaCuoc2.text = [NSString stringWithFormat:@"%@ (1.000đ/ngày)",[Utility getToday]];
    _lblGiaCuoc2.font = [UIFont fontWithName:XSFontCondensed size:13];
     maVungChon = @"MT";
    _btnMienBac.backgroundColor = RGB(106, 0, 0);
    _btnMienTrung.backgroundColor = RGB(181, 28, 28);
    _btnMienNam.backgroundColor = RGB(106, 0, 0);
    _imgSelectBut.frame = CGRectMake(155, 0, 11, 11);
    
    if (typeLoc==2) {
        typeLoc=0;
    }

    
    if (typeLoc==0) {
        
        [_btnKQXS setTitleColor:RGB(106, 0, 0) forState:UIControlStateNormal];
        [_btnLOTO setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [_btnXSDT setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    }
    else if (typeLoc==1)
    {
        [_btnKQXS setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [_btnLOTO setTitleColor:RGB(106, 0, 0) forState:UIControlStateNormal];
        [_btnXSDT setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    }
    _btnKQXS.frame = CGRectMake(0, 0, 160, 40);
    _btnLOTO.frame = CGRectMake(160, 0, 160, 40);
    
    
     _btnXSDT.hidden = YES;

    
        [self requestXSdate:ngayChon withRegion:maVungChon];
        
  
}

- (IBAction)sxMienNam:(id)sender {
    _lblGiaCuoc1.text = [NSString stringWithFormat:@"%@ (2.000đ/ngày)",[Utility getToday]];
    _lblGiaCuoc1.font = [UIFont fontWithName:XSFontCondensed size:13];
    _lblGiaCuoc2.text = [NSString stringWithFormat:@"%@ (1.000đ/ngày)",[Utility getToday]];
    _lblGiaCuoc2.font = [UIFont fontWithName:XSFontCondensed size:13];
     maVungChon = @"MN";
    _btnMienBac.backgroundColor = RGB(106, 0, 0);
    _btnMienTrung.backgroundColor = RGB(106, 0, 0);
    _btnMienNam.backgroundColor = RGB(181, 28, 28);
    _imgSelectBut.frame = CGRectMake(261, 0, 11, 11);
    
    if (typeLoc==2) {
        typeLoc=0;
    }
    
    if (typeLoc==0) {
        
        [_btnKQXS setTitleColor:RGB(106, 0, 0) forState:UIControlStateNormal];
        [_btnLOTO setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [_btnXSDT setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    }
    else if (typeLoc==1)
    {
        [_btnKQXS setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [_btnLOTO setTitleColor:RGB(106, 0, 0) forState:UIControlStateNormal];
        [_btnXSDT setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    }
    
    _btnKQXS.frame = CGRectMake(0, 0, 160, 40);
    _btnLOTO.frame = CGRectMake(160, 0, 160, 40);
    
       _btnXSDT.hidden = YES;

   
        [self requestXSdate:ngayChon withRegion:maVungChon];
   
}

//--------
- (IBAction)ketQuaXs:(id)sender {
    
    typeLoc = 0;
    [_tblKetQua setSeparatorColor:[UIColor grayColor]];

    [_btnKQXS setTitleColor:RGB(106, 0, 0) forState:UIControlStateNormal];
    [_btnLOTO setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
   [_btnXSDT setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    
    [_tblKetQua reloadData];
}

- (IBAction)loto:(id)sender {
    
    typeLoc = 1;
    [_tblKetQua setSeparatorColor:[UIColor clearColor]];

    [_btnKQXS setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [_btnLOTO setTitleColor:RGB(106, 0, 0) forState:UIControlStateNormal];
    [_btnXSDT setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];

[_tblKetQua reloadData];
}

- (IBAction)xSDT:(id)sender {
    
    
    typeLoc =2;
      [_tblKetQua setSeparatorColor:[UIColor grayColor]];
    
    [_btnKQXS setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [_btnLOTO setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [_btnXSDT setTitleColor:RGB(106, 0, 0) forState:UIControlStateNormal];

    [self requestXSDT:ngayChon];
    
}

#pragma mark 
#pragma mark Tableview Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (typeLoc==0) {
        if ([maVungChon isEqualToString:@"MB"]) {
            return 8;
        }
        else
            return 9;
    } else if (typeLoc==1)
    {
        return 11;
    }
    else
    {
        return (dictKetQuaxSDT)?6:0;
    }
    
 
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (typeLoc==0) {
        if ([maVungChon isEqualToString:@"MB"]) {
            if (indexPath.row == 3||indexPath.row == 5) {
                return  40;
            }
            else
                return 30;
        }
        else
        {
            if (indexPath.row == 4) {
                return  40;
            }
            else
                return 30;
        }
       
    } else if (typeLoc==1)
    {
        return 30;
    }
    else
    {
        return 40;
    }
    
  
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor whiteColor];
    }
     [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
   
    
    if (typeLoc==0)
    {
    float heithCell = 30;
        
        if ([maVungChon isEqualToString:@"MB"]) {
            if (indexPath.row == 3||indexPath.row == 5) {
                heithCell = 40;
            }

        }
        else
        {
            if (indexPath.row == 4) {
                heithCell = 40;
            }
        }
        
    UILabel *lblName = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, heithCell)];
    lblName.textAlignment = NSTextAlignmentLeft;
    lblName.textColor = RGB(51, 51, 51);
        lblName.font = [UIFont fontWithName:XSFontCondensed size:17];
        
        
   UILabel *lblKetQua = [[UILabel alloc] initWithFrame:CGRectMake(80, 0, 320-110, heithCell)];
   lblKetQua.textAlignment = NSTextAlignmentCenter;
      lblKetQua.textColor = RGB(51, 51, 51);
    lblKetQua.numberOfLines = 2;
    lblKetQua.lineBreakMode = NSLineBreakByWordWrapping;
         lblKetQua.font =[UIFont fontWithName:XSFontCondensed size:17];
        
        
    switch (indexPath.row) {
        case 0:
        {
            lblName.text = @"   Giải đặc biệt";
            lblKetQua.text = [dictKetQuaxS objectForKey:@"DB"];
              lblName.textColor = RGB(255, 66, 0);
            lblKetQua.textColor = RGB(255, 66, 0);
        }
            break;
        case 1:
        {
            lblName.text = @"   Giải nhất";
            lblKetQua.text = [dictKetQuaxS objectForKey:@"G1"];
        }
            break;
        case 2:
        {
            lblName.text = @"   Giải nhì";
            lblKetQua.text = [dictKetQuaxS objectForKey:@"G2"];
        }
            break;
        case 3:
        {
            lblName.text = @"   Giải ba";
            lblKetQua.text = [dictKetQuaxS objectForKey:@"G3"];
        }
            break;
        case 4:
        {
            lblName.text = @"   Giải tư";
            lblKetQua.text = [dictKetQuaxS objectForKey:@"G4"];
        }
            break;
        case 5:
        {
            lblName.text = @"   Giải năm";
            lblKetQua.text = [dictKetQuaxS objectForKey:@"G5"];
        }
            break;
        case 6:
        {
            lblName.text = @"   Giải sáu";
            lblKetQua.text = [dictKetQuaxS objectForKey:@"G6"];
        }
            break;
        case 7:
        {
            lblName.text = @"   Giải bảy";
            lblKetQua.text = [dictKetQuaxS objectForKey:@"G7"];
        }
            break;
        case 8:
        {
            lblName.text = @"   Giải tám";
            lblKetQua.text = [dictKetQuaxS objectForKey:@"G8"];
        }
            break;
            
        default:
            break;
    }
    
    
    [cell.contentView addSubview:lblName];
    [cell.contentView addSubview:lblKetQua];
        
    }
    else if (typeLoc == 1)
    {
//        loto
        UILabel  *lbl1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 60, 40)];
        UILabel  *lbl2 = [[UILabel alloc] initWithFrame:CGRectMake(59.5, 0,100, 40)];
        UILabel  *lbl3 = [[UILabel alloc] initWithFrame:CGRectMake(159, 0, 60, 40)];
        UILabel  *lbl4 = [[UILabel alloc] initWithFrame:CGRectMake(218.5, 0, 100, 40)];
        
        lbl1.textAlignment = NSTextAlignmentCenter;
//        lbl2.textAlignment = NSTextAlignmentCenter;
        lbl3.textAlignment = NSTextAlignmentCenter;
//        lbl4.textAlignment = NSTextAlignmentCenter;
        
        lbl1.font = [UIFont fontWithName:XSFontCondensed size:17];
        lbl2.font = [UIFont fontWithName:XSFontCondensed size:17];
        lbl3.font = [UIFont fontWithName:XSFontCondensed size:17];
        lbl4.font = [UIFont fontWithName:XSFontCondensed size:17];
        
        lbl1.textColor = RGB(51, 51, 51);
        lbl2.textColor =  RGB(51, 51, 51);
        lbl3.textColor =  RGB(51, 51, 51);
        lbl4.textColor =  RGB(51, 51, 51);

        
        lbl1.layer.borderColor = [UIColor lightGrayColor].CGColor;
        lbl1.layer.borderWidth = 0.5;
        
        lbl2.layer.borderColor = [UIColor lightGrayColor].CGColor;
        lbl2.layer.borderWidth = 0.5;
        
        lbl3.layer.borderColor = [UIColor lightGrayColor].CGColor;
        lbl3.layer.borderWidth = 0.5;
        
        lbl4.layer.borderColor = [UIColor lightGrayColor].CGColor;
        lbl4.layer.borderWidth = 0.5;
        
       
        
        if(indexPath.row==0)
        {
            lbl1.text = @"Đầu";
            lbl2.text = @"     LOTO";
            lbl3.text = @"Đuôi";
            lbl4.text = @"     LOTO";
            lbl4.adjustsFontSizeToFitWidth = YES;
//            [lbl4 setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
            [lbl4 setMinimumScaleFactor:9/[UIFont labelFontSize]];
            lbl4.numberOfLines = 1;

        }
        else
        {
            switch (indexPath.row) {
                case 1:
                {
                    lbl4.text =   [self  getDuoiLoto:@"0"];
                    lbl2.text =   [self  getDauLoto:@"0"];
                    lbl1.text = @"0";
                    lbl3.text = @"0";
                }
                    break;
                case 2:
                {
                    lbl4.text =   [self  getDuoiLoto:@"1"];
                    lbl2.text =   [self  getDauLoto:@"1"];
                    lbl1.text = @"1";
                    lbl3.text = @"1";
                }
                    break;
                case 3:
                {
                    lbl4.text =   [self  getDuoiLoto:@"2"];
                    lbl2.text =   [self  getDauLoto:@"2"];
                    lbl1.text = @"2";
                    lbl3.text = @"2";
                }
                    break;
                case 4:
                {
                    lbl4.text =   [self  getDuoiLoto:@"3"];
                    lbl2.text =   [self  getDauLoto:@"3"];
                    lbl1.text = @"3";
                    lbl3.text = @"3";
                }
                    break;
                case 5:
                {
                    lbl4.text =   [self  getDuoiLoto:@"4"];
                    lbl2.text =   [self  getDauLoto:@"4"];
                    lbl1.text = @"4";
                    lbl3.text = @"4";
                }
                    break;
                case 6:
                {
                    lbl4.text =   [self  getDuoiLoto:@"5"];
                    lbl2.text =   [self  getDauLoto:@"5"];
                    lbl1.text = @"5";
                    lbl3.text = @"5";
                }
                    break;
                case 7:
                {
                    lbl4.text =   [self  getDuoiLoto:@"6"];
                    lbl2.text =   [self  getDauLoto:@"6"];
                    lbl1.text = @"6";
                    lbl3.text = @"6";
                }
                    break;
                case 8:
                {
                    lbl4.text =   [self  getDuoiLoto:@"7"];
                    lbl2.text =   [self  getDauLoto:@"7"];
                    lbl1.text = @"7";
                    lbl3.text = @"7";
                }
                    break;
                case 9:
                {
                    lbl4.text =   [self  getDuoiLoto:@"8"];
                    lbl2.text =   [self  getDauLoto:@"8"];
                    lbl1.text = @"8";
                    lbl3.text = @"8";
                }
                    break;
                case 10:
                {
                    lbl4.text =   [self  getDuoiLoto:@"9"];
                    lbl2.text =   [self  getDauLoto:@"9"];
                    lbl1.text = @"9";
                    lbl3.text = @"9";
                }
                    break;
                default:
                    break;
            }
        }
        [cell.contentView addSubview:lbl1];
        [cell.contentView addSubview:lbl2];
        [cell.contentView addSubview:lbl3];
        [cell.contentView addSubview:lbl4];
        
    }
    else
    {
//        dien toan
        UILabel *titleDT = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
        titleDT.textAlignment = NSTextAlignmentLeft;
       titleDT.font= [UIFont fontWithName:XSFontCondensed size:16];

        
        switch (indexPath.row) {
            case 0:
            {
                   titleDT.text = @"Kết quả xổ số điện toán 123";
                [cell.contentView addSubview:titleDT];
            }
                break;
            case 1:
            {
//
                UILabel *lbl123 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
                lbl123.textAlignment = NSTextAlignmentLeft;
            lbl123.font    = [UIFont fontWithName:XSFontCondensed size:16];
 lbl123.textAlignment = NSTextAlignmentCenter;
                lbl123.text = ([dictKetQuaxSDT isKindOfClass:[NSString class]])?@"":[dictKetQuaxSDT objectForKey:@"DT123"];
                [cell.contentView addSubview:lbl123];

            }
                break;
            case 2:
            {
                  titleDT.text = @"Kết quả xổ số điện toán 6X36";
                [cell.contentView addSubview:titleDT];
            }
                break;
            case 3:
            {
//
                UILabel *lbl636 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
                lbl636.textAlignment = NSTextAlignmentLeft;
                lbl636.text = ([dictKetQuaxSDT isKindOfClass:[NSString class]])?@"":[dictKetQuaxSDT objectForKey:@"DT6x36"];
                lbl636.font    = [UIFont fontWithName:XSFontCondensed size:16];
 lbl636.textAlignment = NSTextAlignmentCenter;
                [cell.contentView addSubview:lbl636];

            }
                break;
            case 4:
            {
                  titleDT.text = @"Kết quả xổ số thần tài";
                [cell.contentView addSubview:titleDT];
            }
                break;
            case 5:
            {
               UILabel *lblthanTai = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
                lblthanTai.textAlignment = NSTextAlignmentLeft;
                lblthanTai.text = ([dictKetQuaxSDT isKindOfClass:[NSString class]])?@"":[dictKetQuaxSDT objectForKey:@"TT"];
                  lblthanTai.font    = [UIFont fontWithName:XSFontCondensed size:16];
                lblthanTai.textAlignment = NSTextAlignmentCenter;
                [cell.contentView addSubview:lblthanTai];

            }
                break;
           
            default:
                break;
        }
        
        
    }
  
    return cell;
}



//- (UIStatusBarStyle)preferredStatusBarStyle
//{
//    return UIStatusBarStyleLightContent;
//}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)sendSMS:(id)sender {
    
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
    controller.delegate = self;
    
    if([MFMessageComposeViewController canSendText])
    {
          NSString *toNumber = nil;
        if ([Utility isViettel]) {
            toNumber = DS_VIETTEL_XS;
            if ([maTinhChon isEqualToString:@"XSTD"]) {
                 controller.body = [NSString stringWithFormat:@"%@",@"XSMB"];
            }
            else
            controller.body = [NSString stringWithFormat:@"%@",maTinhChon];
            controller.recipients = [NSArray arrayWithObjects:toNumber, nil];

        }
        else
        {
            toNumber = DS_NOT_VIETTEL;
            NSString *strOr = [Utility dsMaTinhMienTrung];
            if ([strOr rangeOfString:maTinhChon].location!=NSNotFound) {
                controller.body = [NSString stringWithFormat:@"DK %@",@"XSMT"];
                
            }
            else
            {
                controller.body = [NSString stringWithFormat:@"DK %@",@"XSMN"];
            }
            if ([maTinhChon isEqualToString:@"XSTD"]) {
                controller.body = [NSString stringWithFormat:@"DK %@",@"XSMB"];
            }
            if ([maTinhChon isEqualToString:@"XSBDH"]) {
                controller.body = [NSString stringWithFormat:@"DK %@",@"XSBDH"];
            }
            controller.recipients = [NSArray arrayWithObjects:toNumber, nil];
            

            
           
            
              
        }
               controller.messageComposeDelegate = self;
        [self presentModalViewController:controller animated:YES];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
	switch (result) {
		case MessageComposeResultCancelled:
			NSLog(@"Cancelled");
			break;
		case MessageComposeResultFailed:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Không gửi được tin nhắn" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
        }
			break;
		case MessageComposeResultSent:
        {UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thành công" message:@"Gửi tin nhắn thành công" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
        }
			break;
		default:
			break;
	}
    
	[self dismissModalViewControllerAnimated:YES];
}

#pragma ---
#pragma mark

- (IBAction)thongKeFrom3:(id)sender {
  
    ThongKeVC *mainView = [[ThongKeVC alloc] initWithNibName:@"ThongKeVC" bundle:nil];            CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = mainView;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];

}

@end
