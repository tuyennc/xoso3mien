//
//  LichThuongVC.h
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LichThuongVC : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *btnMienNam;
@property (strong, nonatomic) IBOutlet UILabel *tittleVC;
@property (strong, nonatomic) IBOutlet UIButton *btnMienTrung;
@property (strong, nonatomic) IBOutlet UIImageView *imgSelectBut;
@property (strong, nonatomic) IBOutlet UITableView *tblMenu;
@property (strong, nonatomic) IBOutlet UIImageView *imgAddV;
@property (strong, nonatomic) IBOutlet UINavigationBar *navTu;
@end
