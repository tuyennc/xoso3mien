//
//  LichThuongVC.m
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import "LichThuongVC.h"
#import "DetailLich.h"

@interface LichThuongVC ()
{
    ECSlidingViewController  *slidingViewController;
    LogInOutVC   *logInOut;
    BOOL isMienNam;
    NSArray *arrMienNam;
    NSArray *arrMienTrung;
    NSMutableDictionary *dictTinh;
    
    NSString *codeMatinh;
     DangKiVC     *dangKi;
      AccountInfor   *accInfor;
    int randomY;
}
@end

@implementation LichThuongVC
//236/71
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.slidingViewController resetTopView];
    self.navTu.topItem.title = @"";
    _tittleVC.text =@"Lịch Mở Thưởng";
    _tittleVC.font = [UIFont fontWithName:XSFontCondensed size:20];
    if (IS_OS_7_OR_LATER) {
        
        self.navigationController.view.backgroundColor = [UIColor whiteColor];
        
        UIView *viewSta = [[UIView alloc] initWithFrame:CGRectMake(0, -20, 320, 20)];
        viewSta.backgroundColor = RGB(106, 0, 0);
        
        [self.view addSubview:viewSta];
//        CGRect screenRect = [[UIScreen mainScreen] bounds];
//        self.view.frame =  CGRectMake(0,20,self.view.frame.size.width,screenRect.size.height-20);
        
    }
}
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    [super viewDidLoad];
    
    int minY = 1;
    int maxY = 3;
    int rangeY = maxY - minY;
    randomY = (arc4random() % rangeY) + minY;
    if (randomY==1) {
        NSURL *url   = [[NSBundle mainBundle] URLForResource:@"anhdong" withExtension:@"gif"];
        _imgAddV.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    }
    else
        _imgAddV.image = [UIImage imageNamed:@"banner-ole-mobile.jpg"];

    
      _btnMienTrung.backgroundColor = RGB(181, 28, 28);
    _btnMienNam.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:17];
    _btnMienTrung.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:17];
    
    NSString* pathSound = [[NSBundle mainBundle] pathForResource:@"DSMTinh" ofType:@"plist"];
    dictTinh = [[NSMutableDictionary alloc] initWithContentsOfFile:pathSound];

    
    arrMienTrung = [[Utility dsTinhMienTrung] componentsSeparatedByString:@"#"];
    arrMienTrung = [[Utility dsTinhMienNam] componentsSeparatedByString:@"#"];
    // Do any additional setup after loading the view from its nib.
    UIImage *imgItem =[ Utility resizeImg:[UIImage imageNamed:@"iconmenu.png"] withSize:CGSizeMake(30, 30*30/50)] ;    
    self.navTu.topItem.leftBarButtonItem = [UIBarButtonItem barItemWithImage:imgItem target:self action:@selector(leftBtnClicked:)];
    if (IS_OS_7_OR_LATER) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        [self setNeedsStatusBarAppearanceUpdate];
        self.navTu.barTintColor = RGB(106, 0, 0);
    }
    else
    {
        self.navTu.TintColor = RGB(106, 0, 0);
    }
    
    //    ecsliding
    slidingViewController = [[ECSlidingViewController alloc] init];
    MenuVC *menuVC   = [[MenuVC alloc] initWithNibName:@"MenuVC" bundle:nil];
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    self.slidingViewController.underLeftViewController  = menuVC;
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
    
    self.tblMenu.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 1.0f)] ;
    self.tblMenu.tableHeaderView.backgroundColor = [UIColor lightGrayColor];
    self.tblMenu.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 1.0f)] ;
    self.tblMenu.tableFooterView.backgroundColor = [UIColor lightGrayColor];
    //    self.tblMenu.tableFooterView.hidden = YES;
    
    if (IS_OS_7_OR_LATER) {
        UIView *viewSta = [[UIView alloc] initWithFrame:CGRectMake(0, -20, 320, 20)];
        viewSta.backgroundColor = RGB(106, 0, 0);
        
        [self.view addSubview:viewSta];
        [self.tblMenu setSeparatorInset:UIEdgeInsetsZero];
    }
    self.tblMenu.separatorColor =  [UIColor lightGrayColor];
    //    login
    UIButton *btnSelectDate=[UIButton buttonWithType:UIButtonTypeCustom];
    btnSelectDate.frame = CGRectMake(0, 0,30,30);
    
    if ([[AppDelegate sharedDelegate] isLogin]) {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"Logged.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    else
    {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"LogInOut.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    [btnSelectDate addTarget:self action:@selector(pushViewLogIO) forControlEvents:UIControlEventTouchUpInside];
    self.navTu.topItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSelectDate];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateImageLogin)
                                                 name:@"UPDATE_IMAGE_LOGIN"
                                               object:nil];
    
    
   
}
- (IBAction)leftBtnClicked:(id)sender
{
    [[[UIApplication sharedApplication]keyWindow]endEditing:YES];
    [self.slidingViewController anchorTopViewTo:ECRight];
}
#pragma mark
#pragma mark loginout delete

-(void)dangKi
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    dangKi  = [[DangKiVC alloc] initWithNibName:@"DangKiVC" bundle:nil]  ;
      dangKi.delegate = self;
    [self presentPopupViewController:dangKi animationType:MJPopupViewAnimationFade];
    
}
-(void)closePopUp
{
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
    
}
-(void)updateImageLogin
{
    UIButton *btnSelectDate=[UIButton buttonWithType:UIButtonTypeCustom];
    btnSelectDate.frame = CGRectMake(0, 0,30,30);
    
    if ([[AppDelegate sharedDelegate] isLogin]) {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"Logged.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    else
    {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"LogInOut.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    [btnSelectDate addTarget:self action:@selector(pushViewLogIO) forControlEvents:UIControlEventTouchUpInside];
    self.navTu.topItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSelectDate];
}

-(void)pushViewLogIO
{
    if ([[AppDelegate sharedDelegate ] isLogin]==NO) {
        logInOut  = [[LogInOutVC alloc] initWithNibName:@"LogInOutVC" bundle:nil]  ;
        logInOut.delegate = self;
        [self presentPopupViewController:logInOut animationType:MJPopupViewAnimationFade];
        
    }
    else
    {
        accInfor  = [[AccountInfor alloc] initWithNibName:@"AccountInfor" bundle:nil]  ;
        accInfor.delegate = self;
        [self presentPopupViewController:accInfor animationType:MJPopupViewAnimationFade];
        
    }
}

#pragma mark
#pragma mark IBOUlet

- (IBAction)miennam:(id)sender {
    
    isMienNam = YES;
      _btnMienTrung.backgroundColor = RGB(106, 0, 0);
    _btnMienNam.backgroundColor = RGB(181, 28, 28);
    _imgSelectBut.frame = CGRectMake(236, 0, 9, 9);
    
    [_tblMenu reloadData];
}
- (IBAction)mientrung:(id)sender {
  
    isMienNam = NO;
    _btnMienTrung.backgroundColor = RGB(181, 28, 28);
    _btnMienNam.backgroundColor = RGB(106, 0, 0);
    _imgSelectBut.frame = CGRectMake(71, 0, 9, 9);
      [_tblMenu reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark
#pragma mark - tableview delegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
            return 7;
    
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"";
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
   
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
        /* Create custom view to display section header... */
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
        [label setFont:[UIFont fontWithName:XSFontCondensed size:17]];
      
    
            if (section==0) {
                [label setText:@"Chủ nhật"];
            }
            else if (section ==1)
            {
                [label setText:@"Thứ hai"];
            }
            else if (section ==2)
            {
                 [label setText:@"Thứ ba"];
            }
            else if (section ==3)
            {
                 [label setText:@"Thứ tư"];
            }
            else if (section ==4)
            {
                 [label setText:@"Thứ năm"];
            }
            else if (section ==5)
            {
                  [label setText:@"Thứ sáu"];
            
            }
            else if (section ==6)
            {
                [label setText:@"Thứ bảy"];
                
            }
    
 
        
        label.textAlignment = NSTextAlignmentCenter;
        
        [view addSubview:label];
        view.backgroundColor= RGB(216, 216, 216);
        return view;

  
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
        {
            
            if (isMienNam) {
                return 3;
                
            }
            else
            {
              return 2;
            }
            
            
        }
            break;
        case 1:
        {
            
            if (isMienNam) {
                return 3;

            }
            else
            {
                return 2;
            }
        }
            break;
        case 2:
        {
            
            if (isMienNam) {
                return 3;

                
            }
            else
            {
               return 2;
                
            }
        }
            break;
        case 3:
        {
            
            if (isMienNam) {
                return 3;

                
                
            }
            else
            {
               return 2;
                
            }
        }
            break;
        case 4:
        {
            
            if (isMienNam) {
                return 3;

                
            }
            else
            {
                return 3;

                
            }
        }
            break;
        case 5:
        {
            
            if (isMienNam) {
                return 3;

                
            }
            else
            {
                
                return 2;
            }
        }
            break;
        case 6:
        {
            
            if (isMienNam) {
              return 4;
                
            }
            else
            {
                
                return 2;

            }
        }
            break;
        default:
            break;
    }

    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        return 30;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor whiteColor];
    }
    [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
   
    UILabel *lblNgay = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 50)];
     UILabel *lblTinh = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    
    lblNgay.textColor = RGB(51, 51, 51);
     lblTinh.textColor = RGB(51, 51, 51);
    lblNgay.font = [UIFont fontWithName:XSFontCondensed size:17];
    lblTinh.font = [UIFont fontWithName:XSFontCondensed size:17];
    lblTinh.numberOfLines = 3;
    if (isMienNam && indexPath.row==6) {
        lblTinh.frame =CGRectMake(100, 0, 220, 70);
        lblNgay.frame =CGRectMake(0, 0, 100, 70);
    }
    
//    CALayer *bottomBorder = [CALayer layer];
//    
//    bottomBorder.frame = CGRectMake(0.0f, 0,1 , lblNgay.frame.size.height);
//    
//    bottomBorder.backgroundColor = [UIColor lightGrayColor].CGColor;
//    
//    [lblTinh.layer addSublayer:bottomBorder];
    
    UIImage *imgItem2 =[ Utility resizeImg:[UIImage imageNamed:@"menu-arrow.png"] withSize:CGSizeMake(5, 16)] ;
    UIImageView *imageView = [[UIImageView alloc] initWithImage:imgItem2];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    cell.accessoryView = imageView;
    
    switch (indexPath.section) {
        case 0:
        {
           
            if (isMienNam) {
                if (indexPath.row==0) {
                    lblTinh.text = @"   Kiên Giang";
                }
                else if (indexPath.row==1)
                {
                    lblTinh.text = @"   Đà Lạt";
                }
                else if (indexPath.row==2)
                {
                      lblTinh.text = @"   Tiền Giang";
                }
               
            }
            else
            {
                if (indexPath.row==0) {
                    lblTinh.text = @"   Khánh Hòa";
                }
                else if (indexPath.row==1)
                {
                    lblTinh.text = @"   Kon Tum";
                }
               

            }
            
            
        }
            break;
        case 1:
        {
          
            if (isMienNam) {
            if (indexPath.row==0)
                {
                    lblTinh.text = @"  Tp.Hồ Chí Minh ";
                }
                else if (indexPath.row==1)
                {
                    lblTinh.text = @" Cà Mau";
                }
                else if (indexPath.row==2)
                {
                    lblTinh.text = @" Đồng Tháp";
                }
            }
            else
            {
                if (indexPath.row==0)
                {
                    lblTinh.text = @"   Phú Yên";
                }
                else if (indexPath.row==1)
                {
                    lblTinh.text = @" Thừa Thiên Huế";
                }
            }
                        }
            break;
        case 2:
        {
      
            if (isMienNam) {
                if (indexPath.row==0)
                {
                    lblTinh.text = @"   Bạc Liêu";
                }
                else if (indexPath.row==1)
                {
                    lblTinh.text = @"  Bến Tre";
                }
                else if (indexPath.row==2)
                {
                    lblTinh.text = @" Vũng Tàu";
                }
                
                         }
            else
            {
                if (indexPath.row==0)
                {
                    lblTinh.text = @"   Quảng Nam";
                }
                else if (indexPath.row==1)
                {
                    lblTinh.text = @" Đắc Lắc";
                }
                
            }
        }
            break;
        case 3:
        {
           
            if (isMienNam) {
                if (indexPath.row==0)
                {
                    lblTinh.text = @"   Đồng Nai";
                }
                else if (indexPath.row==1)
                {
                    lblTinh.text = @" Sóc Trăng";
                }
                else if (indexPath.row==2)
                {
                    lblTinh.text = @" Cần Thơ";
                }
                
               
            }
            else
            {
                if (indexPath.row==0)
                {
                    lblTinh.text = @"  Đà Nẵng ";
                }
                else if (indexPath.row==1)
                {
                    lblTinh.text = @" Khánh Hòa";
                }
                
            }
        }
            break;
        case 4:
        {
          
            if (isMienNam) {
                if (indexPath.row==0)
                {
                    lblTinh.text = @"  Bình Thuận ";
                }
                else if (indexPath.row==1)
                {
                    lblTinh.text = @" An Giang";
                }
                else if (indexPath.row==2)
                {
                    lblTinh.text = @" Tây Ninh";
                }
               
            }
            else
            {
                if (indexPath.row==0)
                {
                    lblTinh.text = @"   Bình Định";
                }
                else if (indexPath.row==1)
                {
                    lblTinh.text = @" Quảng Bình";
                }
                else if (indexPath.row==2)
                {
                    lblTinh.text = @" Quảng Trị";
                }
                
            }
        }
            break;
        case 5:
        {
          
            if (isMienNam) {
                if (indexPath.row==0)
                {
                    lblTinh.text = @"  Bình Dương ";
                }
                else if (indexPath.row==1)
                {
                    lblTinh.text = @" Trà Vinh";
                }
                else if (indexPath.row==2)
                {
                    lblTinh.text = @" Vĩnh Long";
                }
                
            }
            else
            {
               
                if (indexPath.row==0)
                {
                    lblTinh.text = @"  Gia Lai ";
                }
                else if (indexPath.row==1)
                {
                    lblTinh.text = @" Ninh Thuận";
                }
               
            }
        }
            break;
        case 6:
        {
          
            if (isMienNam) {
                if (indexPath.row==0)
                {
                    lblTinh.text = @"   Tp.Hồ Chí Minh";
                }
                else if (indexPath.row==1)
                {
                    lblTinh.text = @" Bình Phước";
                }
                else if (indexPath.row==2)
                {
                    lblTinh.text = @" Hậu Giang";
                }
                else if (indexPath.row==3)
                {
                    lblTinh.text = @" Long An";
                }
               
            }
            else
            {
               
                if (indexPath.row==0)
                {
                    lblTinh.text = @"   Đà Nẵng";
                }
                else if (indexPath.row==1)
                {
                    lblTinh.text = @" Đắc Nông";
                }
                else if (indexPath.row==2)
                {
                    lblTinh.text = @" Quảng Ngãi";
                }
            }
        }
            break;
              default:
            break;
    }
    
    [cell.contentView addSubview:lblTinh];
//     [cell.contentView addSubview:lblNgay];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
        {
            
            if (isMienNam) {
                if (indexPath.row==0) {
                    codeMatinh = @"XSKG";
                }
                else if (indexPath.row==1)
                {
                    codeMatinh = @"XSDL";
                }
                else if (indexPath.row==2)
                {
                    codeMatinh = @"XSTG";
                }
                
            }
            else
            {
                if (indexPath.row==0) {
                    codeMatinh = @"XSKH";
                }
                else if (indexPath.row==1)
                {
                    codeMatinh = @"XSKT";
                }
                
                
            }
            
            
        }
            break;
        case 1:
        {
            
            if (isMienNam) {
                if (indexPath.row==0)
                {
                    codeMatinh = @"XSHCM";
                }
                else if (indexPath.row==1)
                {
                    codeMatinh = @"XSCM";
                }
                else if (indexPath.row==2)
                {
                    codeMatinh = @"XSDT";
                }
            }
            else
            {
                if (indexPath.row==0)
                {
                    codeMatinh = @"XSPY";
                }
                else if (indexPath.row==1)
                {
                    codeMatinh = @"XSTTH";
                }
            }
        }
            break;
        case 2:
        {
            
            if (isMienNam) {
                if (indexPath.row==0)
                {
                    codeMatinh = @"XSBL";
                }
                else if (indexPath.row==1)
                {
                    codeMatinh = @"XSBT";
                }
                else if (indexPath.row==2)
                {
                    codeMatinh = @"XSVT";
                }
                
            }
            else
            {
                if (indexPath.row==0)
                {
                    codeMatinh = @"XSQNM";
                }
                else if (indexPath.row==1)
                {
                    codeMatinh = @"XSDLK";
                }
                
            }
        }
            break;
        case 3:
        {
            
            if (isMienNam) {
                if (indexPath.row==0)
                {
                    codeMatinh = @"XSDN";
                }
                else if (indexPath.row==1)
                {
                    codeMatinh = @"XSST";
                }
                else if (indexPath.row==2)
                {
                    codeMatinh = @"XSCT";
                }
                
                
            }
            else
            {
                if (indexPath.row==0)
                {
                    codeMatinh = @"XSDNG";
                }
                else if (indexPath.row==1)
                {
                    codeMatinh = @"XSKH";
                }
                
            }
        }
            break;
        case 4:
        {
            
            if (isMienNam) {
                if (indexPath.row==0)
                {
                    codeMatinh = @"XSBTH";
                }
                else if (indexPath.row==1)
                {
                    codeMatinh = @"XSAG";
                }
                else if (indexPath.row==2)
                {
                    codeMatinh = @"XSTN";
                }
                
            }
            else
            {
                if (indexPath.row==0)
                {
                    codeMatinh = @"XSBDH";
                }
                else if (indexPath.row==1)
                {
                    codeMatinh = @"XSQB";
                }
                else if (indexPath.row==2)
                {
                    codeMatinh = @"XSQT";
                }
                
            }
        }
            break;
        case 5:
        {
            
            if (isMienNam) {
                if (indexPath.row==0)
                {
                    codeMatinh = @"XSBD";
                }
                else if (indexPath.row==1)
                {
                    codeMatinh = @"XSTV";
                }
                else if (indexPath.row==2)
                {
                    codeMatinh = @"XSVL";
                }
                
            }
            else
            {
                
                if (indexPath.row==0)
                {
                    codeMatinh = @"XSGL";
                }
                else if (indexPath.row==1)
                {
                    codeMatinh = @"XSNT";
                }
                
            }
        }
            break;
        case 6:
        {
            
            if (isMienNam) {
                if (indexPath.row==0)
                {
                    codeMatinh = @"XSHCM";
                }
                else if (indexPath.row==1)
                {
                    codeMatinh = @"XSBP";
                }
                else if (indexPath.row==2)
                {
                    codeMatinh = @"XSHG";
                }
                else if (indexPath.row==3)
                {
                    codeMatinh = @"XSLA";
                }
                
            }
            else
            {
                
                if (indexPath.row==0)
                {
                    codeMatinh = @"XSDNG";
                }
                else if (indexPath.row==1)
                {
                    codeMatinh = @"XSDNO";
                }
                else if (indexPath.row==2)
                {
                    codeMatinh = @"XSQNI";
                }
            }
        }
            break;
        default:
            break;
    }

    NSLog(@"%@",codeMatinh);
     DetailLich *menuVC   = [[DetailLich alloc] initWithNibName:@"DetailLich" bundle:nil];
    menuVC.maTinhChon = codeMatinh;
//    [self pres]
    [self.navigationController pushViewController:menuVC animated:YES];
}
- (IBAction)quangcao:(id)sender {
    if (randomY==1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:LINK_OLIMPIA]];
    }
    else
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:LINK_OLE]];

}

@end
