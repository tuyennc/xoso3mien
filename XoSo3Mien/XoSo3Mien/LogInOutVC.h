//
//  LogInOutVC.h
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol LogInOutDelegate<NSObject>
@optional

- (void) dangKi;
- (void) closePopUp;
@end
@interface LogInOutVC : UIViewController

//{
//     id<LogInOutDelegate> delegate;
//}
@property (strong, nonatomic) IBOutlet UILabel *lblNotify;
@property (strong, nonatomic) IBOutlet UIButton *btnDangNhap;
@property (strong, nonatomic) IBOutlet UITextField *lblTruyCao;
@property (strong, nonatomic) IBOutlet UITextField *tfMK;
@property (strong, nonatomic) IBOutlet UIButton *btnDangky;
@property (nonatomic, assign) id<LogInOutDelegate> delegate;
@property (strong, nonatomic) IBOutlet UILabel *lblDangNhap;







@end
