//
//  LogInOutVC.m
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import "LogInOutVC.h"
#import "AES128.h"


@interface LogInOutVC ()
{
       ASIHTTPRequest *request;
}
@end

@implementation LogInOutVC


@synthesize delegate = _delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


#pragma mark
#pragma mark Request ket qua
- (void)requestLogin:(NSString*)userName withPass:(NSString*)pass
{
    [[AppDelegate sharedDelegate].hudRequest show:YES];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://id.inet.vn/checkLoginInfoApp?user=%@&pass=%@",userName,pass]];
    NSLog(@"requset :%@",url);
    request = [ASIHTTPRequest requestWithURL:url];
    
    [request setCompletionBlock:^{
        
        
        
        [[AppDelegate sharedDelegate].hudRequest hide:YES];
        // Use when fetching text data
        NSString *responseString = [request responseString];
        NSLog(@"%@",responseString);
       
        if ([responseString isEqualToString:@"0\r\n"]) {
             [[AppDelegate sharedDelegate] setIsLogin:NO];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Tên đăng nhập hoặc mật khẩu của bạn không đúng !" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            NSUserDefaults *df =[NSUserDefaults standardUserDefaults];
            [df setObject:@"0" forKey:@"USER_INFOR"];
            [df synchronize];
        }
        else
        {
            
            [[AppDelegate sharedDelegate] setIsLogin:YES];
            NSUserDefaults *df =[NSUserDefaults standardUserDefaults];
            [df setObject:responseString forKey:@"USER_INFOR"];
            [df synchronize];
            if (_delegate && [_delegate respondsToSelector:@selector(closePopUp)]) {
                [_delegate closePopUp];
            }
             [[NSNotificationCenter defaultCenter] postNotificationName:@"UPDATE_IMAGE_LOGIN" object:self];
        }
    }];
    [request setFailedBlock:^{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Không thể kết nối" delegate:self cancelButtonTitle:@"Kết nối lại" otherButtonTitles:@"Huỷ", nil] ;
        [alert show];
        //        [UIHelper showMessage:@"Không thể kết nối" withTitle:@""];
    }];
    [request startAsynchronous];
    
}



- (IBAction)dangnhap:(id)sender {
    
    [AES128 initialize];
    [self requestLogin:_lblTruyCao.text withPass:[AES128 AES128Encrypt:_tfMK.text]];
    

}
- (IBAction)dangKi:(id)sender {
    
    if (_delegate && [_delegate respondsToSelector:@selector(dangKi)]) {
        [_delegate dangKi];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _btnDangky.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:15];
    _btnDangNhap.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:15];
    _lblDangNhap.font = [UIFont fontWithName:XSFontCondensed size:15];
    _lblNotify.font =  [UIFont fontWithName:XSFontCondensed size:15];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
