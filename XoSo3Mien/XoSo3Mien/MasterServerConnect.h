//
//  MasterServerConnect.h
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol MasterServerDelegate<NSObject>
@optional
- (void) getDataSucsess;
@end


@interface MasterServerConnect : NSObject<NSStreamDelegate,UIAlertViewDelegate>

@property (nonatomic, assign) id<MasterServerDelegate> delegate;
-(void) connectToServerUsingStream:(NSString *)urlStr portNo: (uint) portNo;
-(void) writeToServer:(const uint8_t *) buf;
-(void)mainconnect:(NSString*)textField;
-(void) disconnect;
@end
