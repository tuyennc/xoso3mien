//
//  MasterServerConnect.m
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import "MasterServerConnect.h"
#import "StartUpScreen.h"

@implementation MasterServerConnect

@synthesize delegate = _delegate;

NSMutableData *data;

NSInputStream *iStream;
NSOutputStream *oStream;
NSString *sumStr1=@"";
NSString *sumStr2=@"";
NSString *sumStr3=@"";
CFReadStreamRef readStream = NULL;
CFWriteStreamRef writeStream = NULL;
bool isConnect1=FALSE;


-(void) connectToServerUsingStream:(NSString *)urlStr
                            portNo: (uint) portNo {
    isConnect1=TRUE;
    CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault,
                                       (__bridge CFStringRef) urlStr,
                                       portNo,
                                       &readStream,
                                       &writeStream);
    
    if (readStream && writeStream) {
        CFReadStreamSetProperty(readStream,
                                kCFStreamPropertyShouldCloseNativeSocket,
                                kCFBooleanTrue);
        CFWriteStreamSetProperty(writeStream,
                                 kCFStreamPropertyShouldCloseNativeSocket,
                                 kCFBooleanTrue);
        
        iStream = (__bridge NSInputStream *)readStream;

        [iStream setDelegate:self];
        [iStream scheduleInRunLoop:[NSRunLoop currentRunLoop]
                           forMode:NSDefaultRunLoopMode];
        [iStream open];
        
        oStream = (__bridge NSOutputStream *)writeStream;

        [oStream setDelegate:self];
        [oStream scheduleInRunLoop:[NSRunLoop currentRunLoop]
                           forMode:NSDefaultRunLoopMode];
        [oStream open];
    }
}

-(void) writeToServer:(const uint8_t *) buf {
    [oStream write:buf maxLength:strlen((char*)buf)];
}

- (void)stream:(NSStream *)stream handleEvent:(NSStreamEvent)eventCode {
    switch(eventCode) {
        case NSStreamEventOpenCompleted:
        {
            NSLog(@"Stream opened in ");

            
            
		}
              break;
        case NSStreamEventHasBytesAvailable:
        {
            if (data == nil) {
                data = [[NSMutableData alloc] init];
            }
            uint8_t buf[1024];
            unsigned int len = 0;
            NSMutableData* responseData = [[NSMutableData alloc] initWithCapacity:0];

            while ([iStream hasBytesAvailable]) {
                len = [iStream read:buf maxLength:1024];
                if (len > 0 && len<=1024) {
                    
                    [responseData appendBytes:(const void *)buf length:len];
                    //   [NSThread sleepForTimeInterval:.1];
                }
            }
            [data appendData:responseData];
            NSString *str = [[NSString alloc] initWithData:data
                                                  encoding:NSUTF8StringEncoding];
            NSLog(@"str %@",str);
            if (![str isEqualToString:@""]) {
//                [[AppDelegate sharedDelegate].hudRequest hide:YES];
                NSArray *arrResulf = [str componentsSeparatedByString:@"|"];
                [AppDelegate sharedDelegate].appID= [arrResulf objectAtIndex:4];
                [AppDelegate sharedDelegate].serverZ = [arrResulf objectAtIndex:2];
                [AppDelegate sharedDelegate].portZ = [[arrResulf objectAtIndex:3] intValue];
             
//                if ([[[AppDelegate sharedDelegate]currentApp] isKindOfClass:[StartUpScreen class]]) {
                    if (_delegate && [_delegate respondsToSelector:@selector(getDataSucsess)]) {
                        [_delegate getDataSucsess];
                    }
//                }
               
            }
            
            
//            sumStr1=[[NSString alloc] initWithString:str];
//            
//            sumStr3=[[NSString alloc] initWithString:sumStr2];
//            sumStr3=[sumStr3 stringByAppendingString:str];
//            sumStr2=[[NSString alloc] initWithString:sumStr3];
         
           
          
            data = nil;
        }break;
        case NSStreamEventErrorOccurred:
        {
            	if (stream == iStream) {
                    UIAlertView *alertCon = [[UIAlertView alloc] initWithTitle:@"Không thể kết nối" message:@"Vui lòng kiểm tra kết nối" delegate:self cancelButtonTitle:@"Kết nối lại" otherButtonTitles:nil, nil];
                    [alertCon show];
                }
            
		}
            break;
            
        case NSStreamEventEndEncountered:
        {
       
            NSLog(@"NSStreamEventEndEncountered!");
   
            
        }
        case NSStreamEventHasSpaceAvailable:
            NSLog(@"NSStreamEventHasSpaceAvailable!");
            
            
            
            break;
        default:{
        }break;
    }
}

-(void) disconnect {
    isConnect1 =FALSE;
    [iStream close];
    [oStream close];
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)mainconnect:(NSString*)textField{
   

    if (!isConnect1) {
        [self connectToServerUsingStream:@"210.211.127.172" portNo:1890];
    }
    NSString* text1 = textField;
    NSString* message = [NSString stringWithFormat:@"%@||\r\n", text1];
      NSLog(@"send:%@", message);
    [self writeToServer:(uint8_t *)[message cStringUsingEncoding:NSASCIIStringEncoding]];
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    isConnect1 = FALSE;
    [self mainconnect:@"SLAVE_REQ|0|0"];
}

@end
