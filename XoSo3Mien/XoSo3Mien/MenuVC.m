//
//  MenuVC.m
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import "MenuVC.h"
#import "MoiBanBeVC.h"
#import "QuaThuVC.h"
#import "ChonVeSoVC.h"
#import "DoSoVC.h"
#import "LichThuongVC.h"
#import "ThongKeVC.h"
#import "SoMoVC.h"
#import "VipVC.h"
#import "LogInOutVC.h"
#import "KetQuaVC.h"
#import "HuongDanVC.h"
#import "LogInOutVC.h"
#import "AccountInfor.h"
#import "DangKiVC.h"


@interface MenuVC ()<AccountInforDelegate,LogInOutDelegate,DangKyDelegate>
{
    NSArray *arrText;
    LogInOutVC *loginOUt;
    AccountInfor *accinfo;
    DangKiVC *dangKi;
    NSString *userName;
}
@end

@implementation MenuVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    if (IS_OS_7_OR_LATER) {
        UIView *viewSta = [[UIView alloc] initWithFrame:CGRectMake(0, -20, 320, 20)];
        viewSta.backgroundColor = RGB(236, 236, 236);
        
        
        [self.view addSubview:viewSta];
        
        [self.tblMenu setSeparatorInset:UIEdgeInsetsZero];
//        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
//        self.view.clipsToBounds =YES;
        
        self.view.frame =  CGRectMake(0,20,self.view.frame.size.width,self.view.frame.size.height-20);
    }

    [super viewWillAppear:animated];
    
        self.tblMenu.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 1.0f)] ;
    self.tblMenu.tableHeaderView.backgroundColor = [UIColor lightGrayColor];
    self.tblMenu.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 1.0f)] ;
    self.tblMenu.tableFooterView.backgroundColor = [UIColor lightGrayColor];
    //    self.tblMenu.tableFooterView.hidden = YES;
    
    userName = @"";
    NSUserDefaults *df = [NSUserDefaults standardUserDefaults];
    NSString *strKQ =[df objectForKey:@"USER_INFOR"];
    
    if (strKQ.length> 5) {
        NSArray *arrInfor = [strKQ componentsSeparatedByString:@"<br/>"];
        
        //    strKQ = [strKQ stringByReplacingOccurrencesOfString:@"<br/>"
        //                                         withString:@""];
        //
        userName =    [[[arrInfor objectAtIndex:3] componentsSeparatedByString:@":"] objectAtIndex:1];
    }
    
    
      NSString *menuText=@"";
    if (![AppDelegate sharedDelegate].isLogin) {
        menuText=  @"Kết Quả#VIP#Số Mơ#Thống Kê#Lịch Mở Thưởng#Dò Số#Chọn Vế Số#Quay Thử#Mời Bạn Bè#Đăng Nhập,Đăng kí";
    }
    else
    {
        menuText= [NSString stringWithFormat:@"Kết Quả#VIP#Số Mơ#Thống Kê#Lịch Mở Thưởng#Dò Số#Chọn Vế Số#Quay Thử#Mời Bạn Bè#Hi,%@",userName];
    }
    
    
    arrText=[menuText componentsSeparatedByString:@"#"];
    if (IS_OS_7_OR_LATER) {
        [self.tblMenu setSeparatorInset:UIEdgeInsetsZero];
    }
    self.tblMenu.separatorColor =  [UIColor lightGrayColor];
}
- (void)viewDidLoad
{
//    if (IS_OS_7_OR_LATER) {
//        UIView *viewSta = [[UIView alloc] initWithFrame:CGRectMake(0, -20, 320, 40)];
//        viewSta.backgroundColor = RGB(106, 0, 0);
//        
//        
//        [self.view addSubview:viewSta];
//        
//        [self.tblMenu setSeparatorInset:UIEdgeInsetsZero];
//        //        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
//        self.view.clipsToBounds =YES;
//        
//        self.view.frame =  CGRectMake(0,20,self.view.frame.size.width,self.view.frame.size.height-20);
//    }

    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = RGB(236, 236, 236);
    self.tblMenu.backgroundColor = [UIColor clearColor];
    //    self.view.backgroundColor = [Data getColor:@"#191919"];
    
  
    
    //    arrImg=[[Data getMenuIcon] componentsSeparatedByString:@"#"];
    [self.slidingViewController setAnchorRightRevealAmount:280.0f];
    self.slidingViewController.underLeftWidthLayout = ECFullWidth;
}


#pragma mark -UItableView

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return arrText.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cellkaka";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
    }
   
    cell.textLabel.text =[NSString stringWithFormat:@"%@",[arrText objectAtIndex:indexPath.row]];
    cell.textLabel.textColor = RGB(77, 77, 77);
    cell.textLabel.font =[UIFont fontWithName:XSFontCondensed size:17];
//    cell.backgroundColor = RGB(22, 0, 2);
    cell.backgroundColor = RGB(236, 236, 236);
    
    
    
    switch (indexPath.row) {
        case 0:
        {
             UIImage *imgItem =[ Utility resizeImg:[UIImage imageNamed:@"mn_ketqua.png"] withSize:CGSizeMake(25, 25)] ;
             cell.imageView.image = imgItem;
        }
            
            break;
        case 1:
        {
            UIImage *imgItem =[ Utility resizeImg:[UIImage imageNamed:@"mn_tuvan.png"] withSize:CGSizeMake(25, 25)] ;
            cell.imageView.image = imgItem;
        }
            break;
        case 2:
        {
            UIImage *imgItem =[ Utility resizeImg:[UIImage imageNamed:@"mn_somo.png"] withSize:CGSizeMake(25, 25)] ;
            cell.imageView.image = imgItem;
        }            break;
        case 3:
        {
            UIImage *imgItem =[ Utility resizeImg:[UIImage imageNamed:@"mn_thongke.png"] withSize:CGSizeMake(25, 25)] ;
            cell.imageView.image = imgItem;
        }            break;
        case 4:
        {
            UIImage *imgItem =[ Utility resizeImg:[UIImage imageNamed:@"mn_lichmothuong.png"] withSize:CGSizeMake(25, 25)] ;
            cell.imageView.image = imgItem;
        }
            break;
        case 5:
        {
            UIImage *imgItem =[ Utility resizeImg:[UIImage imageNamed:@"mn_doso.png"] withSize:CGSizeMake(25, 25)] ;
            cell.imageView.image = imgItem;
        }
            break;
        case 6:
        {
            UIImage *imgItem =[ Utility resizeImg:[UIImage imageNamed:@"mn_chonve.png"] withSize:CGSizeMake(25, 25)] ;
            cell.imageView.image = imgItem;
        }
            break;
        case 7:
        {
            UIImage *imgItem =[ Utility resizeImg:[UIImage imageNamed:@"mn_quaythu.png"] withSize:CGSizeMake(25, 25)] ;
            cell.imageView.image = imgItem;
        }
            break;
        case 8:
        {
            UIImage *imgItem =[ Utility resizeImg:[UIImage imageNamed:@"mn_moibanbe.png"] withSize:CGSizeMake(25, 25)] ;
            cell.imageView.image = imgItem;
        }
            break;
//        case 9:
//        {
//            UIImage *imgItem =[ Utility resizeImg:[UIImage imageNamed:@"mn_huongdan.png"] withSize:CGSizeMake(25, 25)] ;
//            cell.imageView.image = imgItem;
//        }
//            break;
        case 9:
        {
            UIImage *imgItem =[ Utility resizeImg:[UIImage imageNamed:@"mn_dangnhap.png"] withSize:CGSizeMake(25, 25)] ;
            cell.imageView.image = imgItem;
        }
            break;
        case 10:
        {
            UIImage *imgItem =[ Utility resizeImg:[UIImage imageNamed:@"mn_thoat.png"] withSize:CGSizeMake(25, 25)] ;
            cell.imageView.image = imgItem;
        }            break;
//        case 12:
//        {
//            UIImage *imgItem =[ Utility resizeImg:[UIImage imageNamed:@""] withSize:CGSizeMake(25, 25)] ;
//            cell.imageView.image = imgItem;
//        }
            break;
//          UIImage *imgItem =[ Utility resizeImg:[UIImage imageNamed:@"iconmenu.png"] withSize:CGSizeMake(25, 25)] ;
//            cell.imageView.frame = CGRectMake(0, 0, 0, 20);
//            cell.imageView.contentMode = UIViewContentModeCenter;
            
        default:
            break;
    }
   
    
     UIImage *imgItem2 =[ Utility resizeImg:[UIImage imageNamed:@"menu-arrow.png"] withSize:CGSizeMake(7, 18)] ;
    UIImageView *imageView = [[UIImageView alloc] initWithImage:imgItem2];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    cell.accessoryView = imageView;
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor =  RGB(106, 0, 0);
//    bgColorView.layer.cornerRadius = 7;
    bgColorView.layer.masksToBounds = YES;
    [cell setSelectedBackgroundView:bgColorView];
//    cell.selectedBackgroundView.backgroundColor= RGB(106, 0, 0);
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (indexPath.row) {
        case 0:
        {
            
            KetQuaVC *mainView = [[KetQuaVC alloc] initWithNibName:@"KetQuaVC" bundle:nil];
            CGRect frame = self.slidingViewController.topViewController.view.frame;
            self.slidingViewController.topViewController = mainView;
            self.slidingViewController.topViewController.view.frame = frame;
            break;
        }
            
        case 1:
        {
           
            VipVC *mainView = [[VipVC alloc] initWithNibName:@"VipVC" bundle:nil];
            CGRect frame = self.slidingViewController.topViewController.view.frame;
            self.slidingViewController.topViewController = mainView;
            self.slidingViewController.topViewController.view.frame = frame;
            break;
        }
        case 2:
        {
            SoMoVC *mainView = [[SoMoVC alloc] initWithNibName:@"SoMoVC" bundle:nil];
            CGRect frame = self.slidingViewController.topViewController.view.frame;
            self.slidingViewController.topViewController = mainView;
            self.slidingViewController.topViewController.view.frame = frame;
            break;
        }
        case 3:
        {
            
            ThongKeVC *mainView = [[ThongKeVC alloc] initWithNibName:@"ThongKeVC" bundle:nil];
            CGRect frame = self.slidingViewController.topViewController.view.frame;
            self.slidingViewController.topViewController = mainView;
            self.slidingViewController.topViewController.view.frame = frame;
            break;
        }
            
        case 4:
        {
            
            LichThuongVC *mainView = [[LichThuongVC alloc] initWithNibName:@"LichThuongVC" bundle:nil];
            CGRect frame = self.slidingViewController.topViewController.view.frame;
            self.slidingViewController.topViewController = mainView;
            self.slidingViewController.topViewController.view.frame = frame;
            break;
        }
            
            
        case 5:
        {
            
            
            DoSoVC *mainView = [[DoSoVC alloc] initWithNibName:@"DoSoVC" bundle:nil];
            CGRect frame = self.slidingViewController.topViewController.view.frame;
            self.slidingViewController.topViewController = mainView;
            self.slidingViewController.topViewController.view.frame = frame;
            
            break;
        }
        case 6:
        {
            
            ChonVeSoVC *mainView = [[ChonVeSoVC alloc] initWithNibName:@"ChonVeSoVC" bundle:nil];
            CGRect frame = self.slidingViewController.topViewController.view.frame;
            self.slidingViewController.topViewController = mainView;
            self.slidingViewController.topViewController.view.frame = frame;
            
            
            break;
        }
            
        case 7:
        {
            
            
            QuaThuVC *mainView = [[QuaThuVC alloc] initWithNibName:@"QuaThuVC" bundle:nil];
            CGRect frame = self.slidingViewController.topViewController.view.frame;
            self.slidingViewController.topViewController = mainView;
            self.slidingViewController.topViewController.view.frame = frame;
            
        }
        case 8:
        {
            MoiBanBeVC *mainView = [[MoiBanBeVC alloc] initWithNibName:@"MoiBanBeVC" bundle:nil];
            CGRect frame = self.slidingViewController.topViewController.view.frame;
            self.slidingViewController.topViewController = mainView;
            self.slidingViewController.topViewController.view.frame = frame;
            break;
        }
//        case 9:
//        {
//            
//            //push huong dan
//            break;
//        }
        case 9:
        {
            //push huong dan
            if ([AppDelegate sharedDelegate].isLogin) {
                           accinfo = [[AccountInfor alloc] initWithNibName:@"AccountInfor" bundle:nil];
                accinfo.delegate =self;
                [self presentPopupViewController:accinfo animationType:MJPopupViewAnimationFade];

            }
            else
            {
                loginOUt = [[LogInOutVC alloc] initWithNibName:@"LogInOutVC" bundle:nil];
 loginOUt.delegate =self;
                loginOUt.delegate = self;
                [self presentPopupViewController:loginOUt animationType:MJPopupViewAnimationFade];
            }
            
            break;
        }
//        case 10:
//        {
//            //push huong dan
//        }
        case 10:
        {
            if ([AppDelegate sharedDelegate].isLogin) {

            NSUserDefaults *df =[NSUserDefaults standardUserDefaults];
            [df setObject:@"0" forKey:@"USER_INFOR"];
            [df synchronize];
            [[AppDelegate sharedDelegate] setIsLogin:NO];
                NSString *menuText=@"";
                if (![AppDelegate sharedDelegate].isLogin) {
                    menuText=  @"Kết Quả#VIP#Số Mơ#Thống Kê#Lịch Mở Thưởng#Dò Số#Chọn Vế Số#Quay Thử#Mời Bạn Bè#Đăng Nhập,Đăng kí";
                }
                else
                {
                    
                    menuText= [NSString stringWithFormat:@"Kết Quả#VIP#Số Mơ#Thống Kê#Lịch Mở Thưởng#Dò Số#Chọn Vế Số#Quay Thử#Mời Bạn Bè#Hi,%@",userName];
                }
                
                arrText=[menuText componentsSeparatedByString:@"#"];
                [_tblMenu reloadData];
            }
            else
            {
                exit(0);
            }
            break;
        }
            
        default:
        {
            break;
        }
    }
   
    [self.slidingViewController resetTopViewWithAnimations:nil onComplete:^{
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }];
}

-(void)closePopUp
{
     [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    NSString *menuText=@"";
    if (![AppDelegate sharedDelegate].isLogin) {
        menuText=  @"Kết Quả#VIP#Số Mơ#Thống Kê#Lịch Mở Thưởng#Dò Số#Chọn Vế Số#Quay Thử#Mời Bạn Bè#Đăng Nhập,Đăng kí";
    }
    else
    {
      
        menuText= [NSString stringWithFormat:@"Kết Quả#VIP#Số Mơ#Thống Kê#Lịch Mở Thưởng#Dò Số#Chọn Vế Số#Quay Thử#Mời Bạn Bè#Hi,%@",userName];
    }
    
    arrText=[menuText componentsSeparatedByString:@"#"];
    [_tblMenu reloadData];
    
}
-(void)dangKi
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    dangKi  = [[DangKiVC alloc] initWithNibName:@"DangKiVC" bundle:nil]  ;
   dangKi.delegate = self;
    [self presentPopupViewController:dangKi animationType:MJPopupViewAnimationFade];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
