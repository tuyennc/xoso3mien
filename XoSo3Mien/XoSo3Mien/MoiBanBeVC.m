//
//  MoiBanBeVC.m
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import "MoiBanBeVC.h"
#import <AddressBook/AddressBook.h>
#import "Contast.h"

@interface MoiBanBeVC ()
{
    ECSlidingViewController  *slidingViewController;
    LogInOutVC   *logInOut;
    NSArray *arrDanhBa;
     DangKiVC     *dangKi;
      AccountInfor   *accInfor;
    int randomY;
}
@end

@implementation MoiBanBeVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    int minY = 1;
    int maxY = 3;
    int rangeY = maxY - minY;
    randomY = (arc4random() % rangeY) + minY;
    if (randomY==1) {
        NSURL *url   = [[NSBundle mainBundle] URLForResource:@"anhdong" withExtension:@"gif"];
        _imgAddV.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    }
    else
        _imgAddV.image = [UIImage imageNamed:@"banner-ole-mobile.jpg"];
    if (IS_OS_7_OR_LATER) {
        [_tblDanhBa setSeparatorInset:UIEdgeInsetsZero];
    }

    [super viewDidLoad];
    arrDanhBa = [[NSArray alloc] init];
    // Do any additional setup after loading the view from its nib.
    UIImage *imgItem =[ Utility resizeImg:[UIImage imageNamed:@"iconmenu.png"] withSize:CGSizeMake(30, 30*30/50)] ;
    
    self.navTu.topItem.leftBarButtonItem = [UIBarButtonItem barItemWithImage:imgItem target:self action:@selector(leftBtnClicked:)];
    if (IS_OS_7_OR_LATER) {
        UIView *viewSta = [[UIView alloc] initWithFrame:CGRectMake(0, -20, 320, 20)];
        viewSta.backgroundColor = RGB(106, 0, 0);
  
//        _imgAddV.fram
        [self.view addSubview:viewSta];
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        self.view.frame =  CGRectMake(0,20,self.view.frame.size.width,screenRect.size.height-20);

        self.navTu.barTintColor = RGB(106, 0, 0);
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        [self setNeedsStatusBarAppearanceUpdate];
    }
    else
    {
        self.navTu.TintColor = RGB(106, 0, 0);
    }
    
    //    ecsliding
    slidingViewController = [[ECSlidingViewController alloc] init];
    MenuVC *menuVC   = [[MenuVC alloc] initWithNibName:@"MenuVC" bundle:nil];
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    self.slidingViewController.underLeftViewController  = menuVC;
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
//    CFErrorRef *error = NULL;
//    ABAddressBookRef addressBook =ABAddressBookCreateWithOptions(NULL, error);
//    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople( addressBook );
//    CFIndex nPeople = ABAddressBookGetPersonCount( addressBook );
//    
//    for ( int i = 0; i < nPeople; i++ )
//    {
//        ABRecordRef person = CFArrayGetValueAtIndex( allPeople, i );
//        NSString *firstName = (__bridge NSString *)(ABRecordCopyValue(person, kABPersonFirstNameProperty));
//        NSString *lastName = (__bridge NSString *)(ABRecordCopyValue(person, kABPersonLastNameProperty));
//     NSLog(@"Name:%@ %@", firstName, lastName);
//        
//        ABMultiValueRef phoneNumbers = ABRecordCopyValue(person, kABPersonPhoneProperty);
//        NSString *phoneNumber;
//        for (CFIndex i = 0; i < ABMultiValueGetCount(phoneNumbers); i++) {
//            phoneNumber = (__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(phoneNumbers, i);
//            NSLog(@"phone:%@", phoneNumber);
//        }
//    }
    [self getAllContacts];
    //    login
    UIButton *btnSelectDate=[UIButton buttonWithType:UIButtonTypeCustom];
    btnSelectDate.frame =CGRectMake(0, 0,30,30);
    
    if ([[AppDelegate sharedDelegate] isLogin]) {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"Logged.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    else
    {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"LogInOut.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    [btnSelectDate addTarget:self action:@selector(pushViewLogIO) forControlEvents:UIControlEventTouchUpInside];
    self.navTu.topItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSelectDate];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateImageLogin)
                                                 name:@"UPDATE_IMAGE_LOGIN"
                                               object:nil];
    
    
  
}

-(void)getAllContacts
{
    
    CFErrorRef *error = nil;
    NSMutableArray *arrTemp = [[NSMutableArray alloc] init];
    
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
    
    __block BOOL accessGranted = NO;
    if (ABAddressBookRequestAccessWithCompletion != NULL) { // we're on iOS 6
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
        });
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        
    }
    else { // we're on iOS 5 or older
        accessGranted = YES;
    }
    
    if (accessGranted) {

        
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
        ABRecordRef source = ABAddressBookCopyDefaultSource(addressBook);
         CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople( addressBook );
//        CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeopleInSourceWithSortOrdering(addressBook, source, kABPersonSortByFirstName);
        CFIndex nPeople = ABAddressBookGetPersonCount(addressBook);
       
        
        
        for (int i = 0; i < nPeople; i++)
        {
          
            Contast *aContast = [[Contast alloc] init];
            ABRecordRef person = CFArrayGetValueAtIndex(allPeople, i);
            
            //get First Name and Last Name
            
           NSString *firstNames = (__bridge NSString*)ABRecordCopyValue(person, kABPersonFirstNameProperty);
            
            NSString *lastNames =  (__bridge NSString*)ABRecordCopyValue(person, kABPersonLastNameProperty);
            
            if (firstNames == NULL) {
                firstNames = @"";
            }
            if (lastNames == NULL) {
                lastNames = @"";
            }
            aContast.firstName = firstNames;
            aContast.lastName = lastNames;
           
            // get contacts picture, if pic doesn't exists, show standart one
            
            NSData  *imgData = (__bridge NSData *)ABPersonCopyImageData(person);
            aContast.imgAvartar = [UIImage imageWithData:imgData];
//            contacts.image = [UIImage imageWithData:imgData];
            if (!aContast.imgAvartar) {
                aContast.imgAvartar = [UIImage imageNamed:@"NOIMG.png"];
            }
            //get Phone Numbers
            
            NSMutableArray *phoneNumbers = [[NSMutableArray alloc] init];
            
            ABMultiValueRef multiPhones = ABRecordCopyValue(person, kABPersonPhoneProperty);
            for(CFIndex i=0;i<ABMultiValueGetCount(multiPhones);i++) {
                
                CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(multiPhones, i);
                NSString *phoneNumber = (__bridge NSString *) phoneNumberRef;
                [phoneNumbers addObject:phoneNumber];
                
                aContast.number = phoneNumber;
            }
            
            
         
            
            //get Contact email
            
//            NSMutableArray *contactEmails = [NSMutableArray new];
//            ABMultiValueRef multiEmails = ABRecordCopyValue(person, kABPersonEmailProperty);
//            
//            for (CFIndex i=0; i<ABMultiValueGetCount(multiEmails); i++) {
//                CFStringRef contactEmailRef = ABMultiValueCopyValueAtIndex(multiEmails, i);
//                NSString *contactEmail = (__bridge NSString *)contactEmailRef;
//                
//                [contactEmails addObject:contactEmail];
//                // NSLog(@"All emails are:%@", contactEmails);
//                
//            }
     
            
            [arrTemp addObject:aContast];
        }
  
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"firstName"
                                                     ascending:YES] ;
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor] ;
        NSArray *arr = arrTemp;
        arr = [arr sortedArrayUsingDescriptors:sortDescriptors] ;
        arrDanhBa = [[NSMutableArray alloc] initWithArray:arr];
       
//        arrDanhBa = [arrTemp sortedArrayUsingSelector:@selector(compare:)];
      
       
        
    }
}
//- (NSComparisonResult)compare:(Contast *)otherObject {
//    return [self.firstName compare:otherObject.firstName];
//}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.slidingViewController resetTopView];
    self.navTu.topItem.title = @"";
    _tittleVC.text =@"Mời Bạn Bè";
     _tittleVC.font = [UIFont fontWithName:XSFontCondensed size:17];
}
- (IBAction)leftBtnClicked:(id)sender
{
    [[[UIApplication sharedApplication]keyWindow]endEditing:YES];
    [self.slidingViewController anchorTopViewTo:ECRight];
}
#pragma mark
#pragma mark loginout delete

-(void)dangKi
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    dangKi  = [[DangKiVC alloc] initWithNibName:@"DangKiVC" bundle:nil]  ;
     dangKi.delegate = self;
    [self presentPopupViewController:dangKi animationType:MJPopupViewAnimationFade];
    
}
-(void)closePopUp
{
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
    
}
-(void)updateImageLogin
{
    UIButton *btnSelectDate=[UIButton buttonWithType:UIButtonTypeCustom];
    btnSelectDate.frame = CGRectMake(0, 0,30,30);
    
    if ([[AppDelegate sharedDelegate] isLogin]) {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"Logged.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    else
    {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"LogInOut.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    [btnSelectDate addTarget:self action:@selector(pushViewLogIO) forControlEvents:UIControlEventTouchUpInside];
    self.navTu.topItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSelectDate];
}

-(void)pushViewLogIO
{
    if ([[AppDelegate sharedDelegate ] isLogin]==NO) {
        logInOut  = [[LogInOutVC alloc] initWithNibName:@"LogInOutVC" bundle:nil]  ;
        logInOut.delegate = self;
        [self presentPopupViewController:logInOut animationType:MJPopupViewAnimationFade];
        
    }
    else
    {
        accInfor  = [[AccountInfor alloc] initWithNibName:@"AccountInfor" bundle:nil]  ;
        accInfor.delegate = self;
        [self presentPopupViewController:accInfor animationType:MJPopupViewAnimationFade];
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark----
#pragma mark Tableview delegate vs dataoucre

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
  return   arrDanhBa.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    return 40;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor whiteColor];
    }
    [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
  
    Contast *aCon =[arrDanhBa objectAtIndex:indexPath.row];
 cell.imageView.image = aCon.imgAvartar;
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@",aCon.firstName,aCon
                           .lastName];
    cell.textLabel.font = [UIFont fontWithName:XSFontCondensed size:17];
    UIImage *imgItem2 =[ Utility resizeImg:[UIImage imageNamed:@"menu-arrow.png"] withSize:CGSizeMake(7, 18)] ;
    UIImageView *imageView = [[UIImageView alloc] initWithImage:imgItem2];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    cell.accessoryView = imageView;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
    controller.delegate = self;
    
    if([MFMessageComposeViewController canSendText])
    {
        
        Contast *acont = [arrDanhBa objectAtIndex:indexPath.row];
        NSString *toNumber = acont.number;
      
        controller.body = @"Tôi đang sử dụng ứng dụng xổ số 3 miền ,rất may mắn và hữu dụng, hãy sử dụng cùng tôi!\n Tải cho androi : https://play.google.com/store/apps/details?id=inet.app \n Tải cho ios : https://itunes.apple.com/us/app/xo-so-ba-mien/id831100530?ls=1&mt=8";
        
            controller.recipients = [NSArray arrayWithObjects:toNumber, nil];
            
            
            
            
            
            
       
        controller.messageComposeDelegate = self;
        [self presentModalViewController:controller animated:YES];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
	switch (result) {
		case MessageComposeResultCancelled:
			NSLog(@"Cancelled");
			break;
		case MessageComposeResultFailed:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Không gửi được tin nhắn" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
        }
			break;
		case MessageComposeResultSent:
        {UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thành công" message:@"Gửi tin nhắn thành công" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
        }
			break;
		default:
			break;
	}
    
	[self dismissModalViewControllerAnimated:YES];
}

- (IBAction)quangcao:(id)sender {
    if (randomY==1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:LINK_OLIMPIA]];
    }
    else
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:LINK_OLE]];

}

@end
