//
//  PUKetQua.h
//  XoSo3Mien
//
//  Created by Tuyen on 1/29/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import <UIKit/UIKit.h>



@protocol PUKetQuaDelegate<NSObject>
@optional
- (void) xemChiTiet;
- (void) closePopUp;

@end

@interface PUKetQua : UIViewController
@property (nonatomic, retain) id<PUKetQuaDelegate>delegate;

@property (strong, nonatomic) IBOutlet UIButton *btnChiTiet;
@property (strong, nonatomic) IBOutlet UIButton *btnDong;
@property (strong, nonatomic) IBOutlet UITextView *tvKetQua;
@property (strong, nonatomic) IBOutlet NSString *strKetQua;
@property (strong, nonatomic) IBOutlet UIImageView *imgKetQua;



@end
