//
//  PUKetQua.m
//  XoSo3Mien
//
//  Created by Tuyen on 1/29/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import "PUKetQua.h"

@interface PUKetQua ()

@end

@implementation PUKetQua
@synthesize delegate = _delegate;
@synthesize strKetQua = _strKetQua;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
     _btnChiTiet.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:17];
     _btnChiTiet.layer.cornerRadius = 10;
    
    _btnDong.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:17];
    _btnDong.layer.cornerRadius = 10;
    _tvKetQua.font = [UIFont fontWithName:XSFontCondensed size:17];
    _tvKetQua.text = _strKetQua;
}

#pragma mark 
#pragma mark IBOutlet

- (IBAction)chiTiet:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(xemChiTiet)]) {
        [_delegate xemChiTiet];
    }
}
- (IBAction)dong:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(closePopUp)]) {
        [_delegate closePopUp];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
