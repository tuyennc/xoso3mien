//
//  PopUpSoiCau.h
//  XoSo3Mien
//
//  Created by Tuyen on 2/10/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol  PopUpSoiCauDelegate <NSObject>

- (void) dongPop;
-(void)sendSMSXIEN;

@end
@interface PopUpSoiCau : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *btnSMS;

@property (nonatomic, assign) int typeSoiCau;
@property (nonatomic, retain) NSDictionary *dictSoiCau;
@property (nonatomic, retain) NSString *tenTinh;
@property (nonatomic, retain) NSString *maTinh;
//view soi cau
@property (strong, nonatomic) IBOutlet UILabel *lbl1;
@property (strong, nonatomic) IBOutlet UILabel *lbl2;
@property (strong, nonatomic) IBOutlet UILabel *lbl3;
@property (strong, nonatomic) IBOutlet UILabel *lbl4;
@property (strong, nonatomic) IBOutlet UILabel *lbl5;
@property (strong, nonatomic) IBOutlet UILabel *lblNgay;
@property (nonatomic, assign) id<PopUpSoiCauDelegate> delegate;
@property (strong, nonatomic) IBOutlet UILabel *ngayAm;
@property (strong, nonatomic) IBOutlet UILabel *timeHoangDao;


//ViewS

@property (strong, nonatomic) IBOutlet UIView *view0;
@property (strong, nonatomic) IBOutlet UIView *view1;
@property (strong, nonatomic) IBOutlet UIView *view3;
@property (strong, nonatomic) IBOutlet UIView *view2;
@property (strong, nonatomic) IBOutlet UIView *view4;
@property (strong, nonatomic) IBOutlet UIView *view5;
@property (strong, nonatomic) IBOutlet UIView *view6;
@property (strong, nonatomic) IBOutlet UIView *view7;
@property (strong, nonatomic) IBOutlet UIView *view8;


//View cápo dep
@property (strong, nonatomic) IBOutlet UILabel *dep1;

@property (strong, nonatomic) IBOutlet UILabel *dep2;
@property (strong, nonatomic) IBOutlet UILabel *dep3;
@property (strong, nonatomic) IBOutlet UILabel *dep4;
@property (strong, nonatomic) IBOutlet UILabel *dep5;


//view Bt
@property (strong, nonatomic) IBOutlet UILabel *bachThu1;

//lo xien
@property (strong, nonatomic) IBOutlet UILabel *lx1;
@property (strong, nonatomic) IBOutlet UILabel *lx2;
@property (strong, nonatomic) IBOutlet UILabel *lx3;

//lo xien quay
@property (strong, nonatomic) IBOutlet UILabel *lxq;
//lo amduong
@property (strong, nonatomic) IBOutlet UILabel *loAD;
@property (strong, nonatomic) IBOutlet UILabel *longuHanh;
@property (strong, nonatomic) IBOutlet UILabel *danngay;
@property (strong, nonatomic) IBOutlet UILabel *dantuan;

- (IBAction)sendSMS:(id)sender ;

@end
