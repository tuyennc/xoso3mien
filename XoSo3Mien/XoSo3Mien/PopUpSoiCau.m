//
//  PopUpSoiCau.m
//  XoSo3Mien
//
//  Created by Tuyen on 2/10/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import "PopUpSoiCau.h"
#import "Calendar.h"

@interface PopUpSoiCau ()

@end

@implementation PopUpSoiCau
@synthesize typeSoiCau = _typeSoiCau;
@synthesize dictSoiCau = _dictSoiCau;
@synthesize tenTinh = _tenTinh;
@synthesize delegate = _delegate;
@synthesize maTinh = _maTinh;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    Calendar *objCa = [[Calendar alloc] init];;
    NSArray *dateNgay = [[_dictSoiCau objectForKey:@"date"] componentsSeparatedByString:@"/"];
    
    NSArray *arr =   [objCa convertSolar2Lunar:[[dateNgay objectAtIndex:0] intValue] mm:[[dateNgay objectAtIndex:1] intValue] yy:[[dateNgay objectAtIndex:2] intValue] timeZone:7];
    NSArray *arrCanChi = [objCa showCanChiWithDayLunar:[[arr objectAtIndex:0] intValue] monthLunar:[[arr objectAtIndex:1] intValue] yearLunar:[[arr objectAtIndex:2] intValue] nhuan:[[arr objectAtIndex:3] intValue]];
    NSString *timeHoangDao = [objCa timeHoangDaoWithDaySolar:[[dateNgay objectAtIndex:0] intValue] monthSolar:[[dateNgay objectAtIndex:1] intValue] yearSolar:[[dateNgay objectAtIndex:2] intValue]];
    
    UILabel *lblNgayAm = (UILabel*)[self.view viewWithTag:10];
    lblNgayAm.text = [NSString stringWithFormat:@"Ngày %@ %@, tháng %@ %@, năm %@ %@",[arrCanChi objectAtIndex:1],[arrCanChi objectAtIndex:2],[arrCanChi objectAtIndex:3],[arrCanChi objectAtIndex:4],[arrCanChi objectAtIndex:5],[arrCanChi objectAtIndex:6]];
    UILabel *timeHD = (UILabel*)[self.view viewWithTag:11];
    timeHD.text = timeHoangDao;
    lblNgayAm.font = [UIFont fontWithName:XSFontCondensed size:15];
    timeHD.font = [UIFont fontWithName:XSFontCondensed size:15];

    
//    UIButton *btnDong = (UIButton*)[self.view viewWithTag:12];
//    btnDong.userInteractionEnabled = YES;
//    [btnDong addTarget:self action:@selector(dongPopu) forControlEvents:UIControlEventTouchUpInside];
//    btnDong.layer.cornerRadius = 7;
    
}
- (IBAction)dong:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(dongPop)]) {
        [_delegate dongPop];
    }
}

-(void)dongPopu
{
    if (_delegate && [_delegate respondsToSelector:@selector(dongPop)]) {
        [_delegate dongPop];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
  
    _btnSMS.layer.cornerRadius = 7;
    _btnSMS.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:15];
    
    UILabel *lblNgay = (UILabel*)[self.view viewWithTag:13];
    lblNgay.font = [UIFont fontWithName:XSFontCondensed size:15];
  
    for (UIView *subView in self.view.subviews) {
        if ([subView isKindOfClass:[UIButton class]]) {
            UIButton *lblTem = (UIButton*)subView;
            lblTem.layer.cornerRadius = 7;
            
            lblTem.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:15];
            
        }

        for (UIView *lbl in subView.subviews) {
            if ([lbl isKindOfClass:[UILabel class]]) {
                UILabel *lblTem = (UILabel*)lbl;
                lblTem.font = [UIFont fontWithName:XSFontCondensed size:15];
                
            }
        }
    }
    
    if (_typeSoiCau == 0) {
        _lbl1.text = [[_dictSoiCau objectForKey:@"decham"] stringValue];
         _lbl2.text = [_dictSoiCau objectForKey:@"dudoande"];
         _lbl3.text =[_dictSoiCau objectForKey:@"loto3so"] ;
         _lbl4.text = [_dictSoiCau objectForKey:@"dudoanlo"];
         _lbl5.text = [_dictSoiCau objectForKey:@"luachon"];
        lblNgay.text = [NSString stringWithFormat:@"Soi cầu %@ ,ngày %@",_tenTinh,[_dictSoiCau objectForKey:@"date"]];
        _view0.hidden = NO;
    }
    else if (_typeSoiCau==1)
    {
        _dep1.text = [_dictSoiCau objectForKey:@"cau10"];
         _dep2.text = [_dictSoiCau objectForKey:@"cau9"];
         _dep3.text = [_dictSoiCau objectForKey:@"cau8"];
         _dep4.text = [_dictSoiCau objectForKey:@"cau7"];
         _dep5.text = [_dictSoiCau objectForKey:@"curr"];
         _dep1.text = [_dictSoiCau objectForKey:@"cau10"];
           lblNgay.text = [NSString stringWithFormat:@"Cầu đề %@ ,ngày %@",_tenTinh,[_dictSoiCau objectForKey:@"date"]];
        _view1.hidden = NO;
    }
    else if (_typeSoiCau==2)
    {
        _bachThu1.text = [_dictSoiCau objectForKey:@"curr"];
        lblNgay.text = [NSString stringWithFormat:@"So đề %@ ,ngày %@",_tenTinh,[_dictSoiCau objectForKey:@"date"]];
        _view2.hidden = NO;
    }
    else if (_typeSoiCau==3)
    {
           _lx1.text = [_dictSoiCau objectForKey:@"xien2"];
            _lx2.text = [_dictSoiCau objectForKey:@"xien3"];
            _lx3.text = [_dictSoiCau objectForKey:@"xien4"];
        lblNgay.text = [NSString stringWithFormat:@"So đề %@ ,ngày %@",_tenTinh,[_dictSoiCau objectForKey:@"date"]];
        _view3.hidden = NO;
    }
    else if (_typeSoiCau==4 )
    {
        _lxq.text = [_dictSoiCau objectForKey:@"info"];
        lblNgay.text = [NSString stringWithFormat:@"So đề %@ ,ngày %@",_tenTinh,[_dictSoiCau objectForKey:@"date"]];
        _view4.hidden = NO;
    }
    else if (_typeSoiCau==5)
    {
        _loAD.text = [_dictSoiCau objectForKey:@"info"];
        lblNgay.text = [NSString stringWithFormat:@"So đề %@ ,ngày %@",_tenTinh,[_dictSoiCau objectForKey:@"date"]];
        _view5.hidden = NO;
    }
    else if (_typeSoiCau==6)
    {
        _longuHanh.text = [_dictSoiCau objectForKey:@"info"];
        lblNgay.text = [NSString stringWithFormat:@"So đề %@ ,ngày %@",_tenTinh,[_dictSoiCau objectForKey:@"date"]];
        _view6.hidden = NO;
    }
    else if (_typeSoiCau==7)
    {
        _danngay.text = [_dictSoiCau objectForKey:@"info"];
        lblNgay.text = [NSString stringWithFormat:@"So đề %@ ,ngày %@",_tenTinh,[_dictSoiCau objectForKey:@"date"]];
        _view7.hidden = NO;
    }
    else if (_typeSoiCau==8)
    {
        _dantuan.text = [_dictSoiCau objectForKey:@"info"];
        lblNgay.text = [NSString stringWithFormat:@"So đề %@ ,ngày %@",_tenTinh,[_dictSoiCau objectForKey:@"date"]];
        _view8.hidden = NO;
    }
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)sendSMS:(id)sender {
    
    if (_delegate && [_delegate respondsToSelector:@selector(sendSMSXIEN)]) {
        [_delegate sendSMSXIEN];
    }

}


@end
