//
//  QuaThuVC.h
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuaThuVC : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *btnQuayThu;

@property (strong, nonatomic) IBOutlet UITableView *_tblKetQua;
@property (strong, nonatomic) IBOutlet UITableView *tblTinh;
@property (strong, nonatomic) IBOutlet UILabel *tittleVC;
@property (strong, nonatomic) IBOutlet UIButton *btnChonTinh;
@property (strong, nonatomic) IBOutlet UINavigationBar *navTu;


@property (strong, nonatomic) IBOutlet UIButton *btnSendSMS;
@property (strong, nonatomic) IBOutlet UIButton *btnSendSMSTK;
@property (strong, nonatomic) IBOutlet UIImageView *imgAddV;

- (IBAction)sendSMS:(id)sender ;
- (IBAction)thongKeFrom3:(id)sender ;
@property (strong, nonatomic) IBOutlet UILabel *lblGiaCuoc1;

@end
