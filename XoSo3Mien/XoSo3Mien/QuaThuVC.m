//
//  QuaThuVC.m
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import "QuaThuVC.h"



@interface QuaThuVC ()
{
    ECSlidingViewController  *slidingViewController;
    LogInOutVC   *logInOut;
    NSString *maVungChon;
       NSMutableDictionary *dictKetQuaxS;
    NSArray *arrTinh ;
    NSArray *arrMaTinh ;
 DangKiVC     *dangKi;
      AccountInfor   *accInfor;
    int randomY;
}
@end

@implementation QuaThuVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.slidingViewController resetTopView];
    self.navTu.topItem.title = @"";
    _tittleVC.text =@"Quay Thử";
     _tittleVC.font = [UIFont fontWithName:XSFontCondensed size:20];
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    _tblTinh.hidden = YES;
}
- (IBAction)btnChontinh:(id)sender {
    _tblTinh.hidden = NO;
}

- (IBAction)quay:(id)sender {
    [dictKetQuaxS removeAllObjects];
    NSString *str = @"";
    if ([maVungChon isEqualToString:@"XSTD"]) {
        str= [NSString stringWithFormat:@"1:%d|2:%d-%d|3:%d-%d-%d-%d-%d-%d|4:%d-%d-%d-%d|5:%d-%d-%d-%d-%d-%d|6:%d-%d-%d|7:%d-%d-%d-%d|8:0|0:%d", 10000 + rand() % 89999,10000 + rand() % 89999,
              10000 + rand() % 89999,10000 + rand() % 89999,10000 + rand() % 89999,10000 + rand() % 89999,10000 + rand() % 89999,10000 + rand() % 89999,1000 + rand() % 8999,1000 + rand() % 8999,1000 + rand() % 8999,1000 + rand() % 8999,1000 + rand() % 8999,1000 + rand() % 8999,1000 + rand() % 8999,1000 + rand() % 8999,1000 + rand() % 8999,1000 + rand() % 8999,1000 + rand() % 8999,100 + rand() % 899,100 + rand() % 899,100 + rand() % 899,10 + rand() % 89,10 + rand() % 89,10 + rand() % 89,10 + rand() % 89,10000 + rand() % 89999
              ];
    }
    else
   str= [NSString stringWithFormat:@"1:%d|2:%d-%d|3:%d-%d|4:%d-%d-%d-%d-%d-%d-%d|5:%d|6:%d-%d-%d|7:%d|8:%d|0:%d",10000 + rand() % 89999,10000 + rand() % 89999,10000 + rand() % 89999,10000 + rand() % 89999,10000 + rand() % 89999,10000 + rand() % 89999,10000 + rand() % 89999,10000 + rand() % 89999,10000 + rand() % 89999,10000 + rand() % 89999,10000 + rand() % 89999,10000 + rand() % 89999,1000 + rand() % 8999,1000 + rand() % 8999,1000 + rand() % 8999,1000 + rand() % 8999,100+ rand() % 899,10+ rand() % 89,100000 + rand() % 899999];
    
    dictKetQuaxS =  [self convertSocket2Http:str ];
    
    
    
//    [UIView animateWithDuration:2. animations:^{
           [__tblKetQua reloadData];
 
//    } ];
 
    
    
}

- (NSMutableDictionary*)convertSocket2Http:(NSString*)resuft
{
    NSMutableDictionary *dictConvert=[[NSMutableDictionary alloc] init];
    NSArray *arr = [resuft componentsSeparatedByString:@"|"];
    NSString *giai1 = [[arr objectAtIndex:0] substringFromIndex:2];
    NSString *giai2 = [[arr objectAtIndex:1] substringFromIndex:2];
    NSString *giai3 = [[arr objectAtIndex:2] substringFromIndex:2];
    NSString *giai4 = [[arr objectAtIndex:3] substringFromIndex:2];
    NSString *giai5 = [[arr objectAtIndex:4] substringFromIndex:2];
    NSString *giai6 = [[arr objectAtIndex:5] substringFromIndex:2];
    NSString *giai7 = [[arr objectAtIndex:6] substringFromIndex:2];
    NSString *giai8 = [[arr objectAtIndex:7] substringFromIndex:2]
    ;NSString *giai0 = [[arr objectAtIndex:8] substringFromIndex:2];
    [dictConvert setObject:giai1 forKey:@"G1"];
    [dictConvert setObject:giai2 forKey:@"G2"];
    [dictConvert setObject:giai3 forKey:@"G3"];
    [dictConvert setObject:giai4 forKey:@"G4"];
    [dictConvert setObject:giai5 forKey:@"G5"];
    [dictConvert setObject:giai6 forKey:@"G6"];
    [dictConvert setObject:giai7 forKey:@"G7"];
    [dictConvert setObject:giai8 forKey:@"G8"];
    [dictConvert setObject:giai0 forKey:@"DB"];
   
//    NSDictionary *dic = dictConvert;
    return dictConvert;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    int minY = 1;
    int maxY = 3;
    int rangeY = maxY - minY;
    randomY = (arc4random() % rangeY) + minY;
    if (randomY==1) {
        NSURL *url   = [[NSBundle mainBundle] URLForResource:@"anhdong" withExtension:@"gif"];
        _imgAddV.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    }
    else
        _imgAddV.image = [UIImage imageNamed:@"banner-ole-mobile.jpg"];

    if (IS_OS_7_OR_LATER) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        [self setNeedsStatusBarAppearanceUpdate];
        UIView *viewSta = [[UIView alloc] initWithFrame:CGRectMake(0, -20, 320, 20)];
        viewSta.backgroundColor = RGB(106, 0, 0);
        
        [self.view addSubview:viewSta];
        [_tblTinh setSeparatorInset:UIEdgeInsetsZero];
        [__tblKetQua setSeparatorInset:UIEdgeInsetsZero];
    }
    [__tblKetQua setSeparatorColor:[UIColor lightGrayColor]];
    [_tblTinh setSeparatorColor:[UIColor lightGrayColor]];
    maVungChon = @"XSTD";
    _btnQuayThu.titleLabel.font  = [UIFont fontWithName:XSFontCondensed size:14];
    [_btnChonTinh setTitle:[NSString stringWithFormat:@"+     %@",@"Miền Bắc"] forState:UIControlStateNormal];
     _btnChonTinh.titleLabel.font  = [UIFont fontWithName:XSFontCondensed size:14];
    _btnChonTinh.layer.borderColor = RGB(106, 0, 0).CGColor;
    _btnChonTinh.layer.borderWidth = 1;
    _btnChonTinh.layer.cornerRadius = 10;
    _btnQuayThu.layer.cornerRadius = 10;
    
    // Do any additional setup after loading the view from its nib.
    UIImage *imgItem =[ Utility resizeImg:[UIImage imageNamed:@"iconmenu.png"] withSize:CGSizeMake(30, 30*30/50)] ;
    
    self.navTu.topItem.leftBarButtonItem = [UIBarButtonItem barItemWithImage:imgItem target:self action:@selector(leftBtnClicked:)];
    if (IS_OS_7_OR_LATER) {
        self.navTu.barTintColor = RGB(106, 0, 0);
    }
    else
    {
        self.navTu.TintColor = RGB(106, 0, 0);
    }
    arrMaTinh = [[Utility dsMaTinhCaNuoc] componentsSeparatedByString:@"#"];
    arrTinh = [[Utility dsTinhCaNuoc] componentsSeparatedByString:@"#"];

    //    ecsliding
    slidingViewController = [[ECSlidingViewController alloc] init];
    MenuVC *menuVC   = [[MenuVC alloc] initWithNibName:@"MenuVC" bundle:nil];
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    self.slidingViewController.underLeftViewController  = menuVC;
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
    
//    sms
    [_btnSendSMS setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [_btnSendSMS setContentVerticalAlignment:UIControlContentVerticalAlignmentTop];
    [_btnSendSMS setTitleEdgeInsets:UIEdgeInsetsMake(6.0f, 30.0f, 0.0f, 0.0f)];
    
    
    _btnSendSMS.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:13];
    
    [_btnSendSMSTK setTitle:@"Thống kê" forState:UIControlStateNormal];
    [_btnSendSMSTK setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [_btnSendSMSTK setContentVerticalAlignment:UIControlContentVerticalAlignmentTop];
    [_btnSendSMSTK setTitleEdgeInsets:UIEdgeInsetsMake(10.0f, 30.0f, 0.0f, 0.0f)];
    
    [_btnSendSMS setTitle: [NSString stringWithFormat:@"Nhận KQXS %@",@"Miền Bắc"] forState:UIControlStateNormal];
    _btnSendSMSTK.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:17];
    _lblGiaCuoc1.text = [NSString stringWithFormat:@"%@ (1.000đ/ngày)",[Utility getToday]];
    _lblGiaCuoc1.font = [UIFont fontWithName:XSFontCondensed size:13];
    //    login
    UIButton *btnSelectDate=[UIButton buttonWithType:UIButtonTypeCustom];
    btnSelectDate.frame = CGRectMake(0, 0,30,30);
    
    if ([[AppDelegate sharedDelegate] isLogin]) {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"Logged.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    else
    {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"LogInOut.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    [btnSelectDate addTarget:self action:@selector(pushViewLogIO) forControlEvents:UIControlEventTouchUpInside];
    self.navTu.topItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSelectDate];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateImageLogin)
                                                 name:@"UPDATE_IMAGE_LOGIN"
                                               object:nil];
    
    
   
}
- (IBAction)leftBtnClicked:(id)sender
{
    [[[UIApplication sharedApplication]keyWindow]endEditing:YES];
    [self.slidingViewController anchorTopViewTo:ECRight];
}
#pragma mark
#pragma mark loginout delete

-(void)dangKi
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    dangKi  = [[DangKiVC alloc] initWithNibName:@"DangKiVC" bundle:nil]  ;
     dangKi.delegate = self;
    [self presentPopupViewController:dangKi animationType:MJPopupViewAnimationFade];
    
}
-(void)closePopUp
{
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
    
}
-(void)updateImageLogin
{
    UIButton *btnSelectDate=[UIButton buttonWithType:UIButtonTypeCustom];
    btnSelectDate.frame =CGRectMake(0, 0,30,30);
    
    if ([[AppDelegate sharedDelegate] isLogin]) {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"Logged.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    else
    {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"LogInOut.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    [btnSelectDate addTarget:self action:@selector(pushViewLogIO) forControlEvents:UIControlEventTouchUpInside];
    self.navTu.topItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSelectDate];
}

-(void)pushViewLogIO
{
    if ([[AppDelegate sharedDelegate ] isLogin]==NO) {
        logInOut  = [[LogInOutVC alloc] initWithNibName:@"LogInOutVC" bundle:nil]  ;
        logInOut.delegate = self;
        [self presentPopupViewController:logInOut animationType:MJPopupViewAnimationFade];
        
    }
    else
    {
        accInfor  = [[AccountInfor alloc] initWithNibName:@"AccountInfor" bundle:nil]  ;
        accInfor.delegate = self;
        [self presentPopupViewController:accInfor animationType:MJPopupViewAnimationFade];
        
    }}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==__tblKetQua) {
        if ([maVungChon isEqualToString:@"XSTD"]) {
            return 8;
        }
        else
            return 9;

    }
    else return arrMaTinh.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
  if (tableView==__tblKetQua) {
        if ([maVungChon isEqualToString:@"XSTD"]) {
            if (indexPath.row == 3||indexPath.row == 5) {
                return  40;
            }
            else
                return 30;
        }
        else
        {
            if (indexPath.row == 4) {
                return  40;
            }
            else
                return 30;
        }
        
  }
    else
        return 40;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor whiteColor];
    }
    [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
 if (tableView==__tblKetQua) {
     float heithCell = 30;
        
        if ([maVungChon isEqualToString:@"XSTD"]) {
            if (indexPath.row == 3||indexPath.row == 5) {
                heithCell = 40;
            }
            
        }
        else
        {
            if (indexPath.row == 4) {
                heithCell = 40;
            }
        }
        
        UILabel *lblName = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, heithCell)];
        lblName.textAlignment = NSTextAlignmentLeft;
        lblName.textColor = RGB(51, 51, 51);
        lblName.font = [UIFont fontWithName:XSFontCondensed size:17];
        
        
        UILabel *lblKetQua = [[UILabel alloc] initWithFrame:CGRectMake(80, 0, 320-110, heithCell)];
        lblKetQua.textAlignment = NSTextAlignmentCenter;
        lblKetQua.textColor = RGB(51, 51, 51);
        lblKetQua.numberOfLines = 2;
        lblKetQua.lineBreakMode = NSLineBreakByWordWrapping;
        lblKetQua.font =[UIFont fontWithName:XSFontCondensed size:17];
     
        
        switch (indexPath.row) {
            case 0:
            {
                lblName.text = @"   Giải đặc biệt";
                lblKetQua.text = [dictKetQuaxS objectForKey:@"DB"];
                lblName.textColor = RGB(255, 66, 0);
                lblKetQua.textColor = RGB(255, 66, 0);
            }
                break;
            case 1:
            {
                lblName.text = @"   Giải nhất";
                lblKetQua.text = [dictKetQuaxS objectForKey:@"G1"];
            }
                break;
            case 2:
            {
                lblName.text = @"   Giải nhì";
                lblKetQua.text = [dictKetQuaxS objectForKey:@"G2"];
            }
                break;
            case 3:
            {
                lblName.text = @"   Giải ba";
                lblKetQua.text = [dictKetQuaxS objectForKey:@"G3"];
            }
                break;
            case 4:
            {
                lblName.text = @"   Giải tư";
                lblKetQua.text = [dictKetQuaxS objectForKey:@"G4"];
            }
                break;
            case 5:
            {
                lblName.text = @"   Giải năm";
                lblKetQua.text = [dictKetQuaxS objectForKey:@"G5"];
            }
                break;
            case 6:
            {
                lblName.text = @"   Giải sáu";
                lblKetQua.text = [dictKetQuaxS objectForKey:@"G6"];
            }
                break;
            case 7:
            {
                lblName.text = @"   Giải bảy";
                lblKetQua.text = [dictKetQuaxS objectForKey:@"G7"];
            }
                break;
            case 8:
            {
                lblName.text = @"   Giải tám";
                lblKetQua.text = [dictKetQuaxS objectForKey:@"G8"];
            }
                break;
                
            default:
                break;
        }
   
     
        [cell.contentView addSubview:lblName];
        [cell.contentView addSubview:lblKetQua];
        
 }
    else
    {
        UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, 92, 40)];
        lbl.text = [arrTinh objectAtIndex:indexPath.row];
        lbl.font = [UIFont fontWithName:XSFontCondensed size:17];
        lbl.textColor = [UIColor whiteColor];
        cell.backgroundColor = RGB(106, 0, 0);
        UIImage *img = [Utility resizeImg:[UIImage imageNamed:@"arr_chontinh.png"] withSize:CGSizeMake(8, 12)];
        cell.imageView.image = img;
        [cell.contentView addSubview:lbl];
    }

    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == _tblTinh)
      
    
//    if([maVungChon isEqualToString:@"XSTD"])
//    {
//        maVungChon = @"Miền Bắc";
//    }
          maVungChon = [arrMaTinh objectAtIndex:indexPath.row];
        _tblTinh.hidden = YES;
         [_btnChonTinh setTitle:[NSString stringWithFormat:@"+      %@",[arrTinh objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
    [_btnSendSMS setTitle: [NSString stringWithFormat:@"Nhận KQXS %@",[arrTinh objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
    if ([maVungChon isEqualToString:@"XSBDH"] || [maVungChon isEqualToString:@"XSTD"]) {
        _lblGiaCuoc1.text = [NSString stringWithFormat:@"%@ (1.000đ/ngày)",[Utility getToday]];
        
    }
    else
    {
        _lblGiaCuoc1.text = [NSString stringWithFormat:@"%@ (2.000đ/ngày)",[Utility getToday]];
    }
}
- (IBAction)sendSMS:(id)sender {
    
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
    controller.delegate = self;
    
    if([MFMessageComposeViewController canSendText])
    {
        NSString *toNumber = nil;
        if ([Utility isViettel]) {
            toNumber = DS_VIETTEL_XS;
            if ([maVungChon isEqualToString:@"XSTD"]) {
                controller.body = [NSString stringWithFormat:@"%@",@"XSMB"];
            }
            else
                controller.body = [NSString stringWithFormat:@"%@",maVungChon];
            controller.recipients = [NSArray arrayWithObjects:toNumber, nil];
            
        }
        else
        {
            toNumber = DS_NOT_VIETTEL;
            NSString *strOr = [Utility dsMaTinhMienTrung];
            if ([strOr rangeOfString:maVungChon].location!=NSNotFound) {
                controller.body = [NSString stringWithFormat:@"DK %@",@"XSMT"];
                
            }
            else
            {
                controller.body = [NSString stringWithFormat:@"DK %@",@"XSMN"];
            }
            if ([maVungChon isEqualToString:@"XSTD"]) {
                controller.body = [NSString stringWithFormat:@"DK %@",@"XSMB"];
            }
            if ([maVungChon isEqualToString:@"XSBDH"]) {
                controller.body = [NSString stringWithFormat:@"DK %@",@"XSBDH"];
            }
            controller.recipients = [NSArray arrayWithObjects:toNumber, nil];
            
        }
        controller.messageComposeDelegate = self;
        [self presentModalViewController:controller animated:YES];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
	switch (result) {
		case MessageComposeResultCancelled:
			NSLog(@"Cancelled");
			break;
		case MessageComposeResultFailed:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Không gửi được tin nhắn" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
        }
			break;
		case MessageComposeResultSent:
        {UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thành công" message:@"Gửi tin nhắn thành công" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
        }
			break;
		default:
			break;
	}
    
	[self dismissModalViewControllerAnimated:YES];
}

#pragma ---
#pragma mark

- (IBAction)thongKeFrom3:(id)sender {
    
    ThongKeVC *mainView = [[ThongKeVC alloc] initWithNibName:@"ThongKeVC" bundle:nil];            CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = mainView;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
- (IBAction)quangcao:(id)sender {
    if (randomY==1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:LINK_OLIMPIA]];
    }
    else
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:LINK_OLE]];
    

}

@end
