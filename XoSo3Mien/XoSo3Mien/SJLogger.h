//
//  SJLogger.h
//  SJLog
//
//  Created by sid on 13/01/11.
//  Copyright 2011 whackylabs. All rights reserved.
/*
*	version 1.0:
*	- logs enable/disable
*/


#import <Foundation/Foundation.h>

/*
 1: ON
 0: OFF
 */
#define LOG 1

@interface SJLogger : NSObject {

}

+(void)logFile:(char*)sourceFile lineNumber:(int)lineNumber format:(NSString*)format, ...;
@end
