//
//  SJLogger.m
//  SJLog
//
//  Created by sid on 13/01/11.
//  Copyright 2011 whackylabs. All rights reserved.
//

#import "SJLogger.h"

void SJLog(NSString *format,...)
{
	if(LOG)
	{	
		va_list args;
		va_start(args,format);
		NSLogv(format, args);
		va_end(args);
	}
}

@implementation SJLogger

+ (void)logFile:(char*)sourceFile lineNumber:(int)lineNumber format:(NSString*)format, ...
{
    va_list ap;
    NSString *print,*file;
    if(!LOG)
        return;
    va_start(ap,format);
    file=[[NSString alloc] initWithBytes:sourceFile length:strlen(sourceFile) encoding:NSUTF8StringEncoding];
    print=[[NSString alloc] initWithFormat:format arguments:ap];
    va_end(ap);
    //NSLog handles synchronization issues
    NSLog(@"%s:%d %@",[[file lastPathComponent] UTF8String],lineNumber,print);
    [print release];
    [file release];
}

@end
