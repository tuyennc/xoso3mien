//
//  SlaveServerConnect.h
//  XoSo3Mien
//
//  Created by Tuyen on 1/20/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SlaveDelegate;



@interface SlaveServerConnect : NSObject<NSStreamDelegate>
{
}

@property (nonatomic, assign) id<SlaveDelegate> delegate;
-(void) connectToServerUsingStream:(NSString *)urlStr portNo: (uint) portNo;
-(void) writeToServer:(const uint8_t *) buf;
-(void)mainconnect:(NSString*)textField;
-(void)connect:(NSString*)textField;
@property(nonatomic,retain)  NSString *sumStr30;
@property(nonatomic,retain) NSMutableData *data;

@end
@protocol SlaveDelegate<NSObject>
@optional
- (void) data_XSSYNC_RESP_Recieved:(NSString*)resuft;
- (void) data_CLIENT_INFO_Recieved;
- (void) data_CLIENT_STATUS_Recieved:(NSString*)sumStr30;
- (void) data_MATCH_FAVORITE_Recieved:(NSString*)sumStr30;
- (void) data_CHAMPION_UPDATE_Recieved:(NSString*)sumStr30;
- (void) data_TKCAU_RESP_Recieved:(NSString*)resuft;
- (void) data_XS_CAU_RESP_Recieved:(NSString*)resuft;
@end