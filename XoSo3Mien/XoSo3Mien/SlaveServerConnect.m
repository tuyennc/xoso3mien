//
//  SlaveServerConnect.m
//  XoSo3Mien
//
//  Created by Tuyen on 1/20/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import "SlaveServerConnect.h"

@implementation SlaveServerConnect



@synthesize delegate = _delegate;

NSInputStream *iStream0;
NSOutputStream *oStream0;

CFReadStreamRef readStream0 = NULL;
CFWriteStreamRef writeStream0 = NULL;
bool isConnect=FALSE;
NSString *sumString=@"";

-(void) connectToServerUsingStream:(NSString *)urlStr
                            portNo: (uint) portNo {
    //    NSLog(@"Connect to server:%@",urlStr);
    //    NSLog(@"Port:%d",portNo);
    isConnect=TRUE;
    CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault,
                                       (__bridge CFStringRef) urlStr,
                                       portNo,
                                       &readStream0,
                                       &writeStream0);
    
    if (readStream0 && writeStream0) {
        CFReadStreamSetProperty(readStream0,
                                kCFStreamPropertyShouldCloseNativeSocket,
                                kCFBooleanTrue);
        CFWriteStreamSetProperty(writeStream0,
                                 kCFStreamPropertyShouldCloseNativeSocket,
                                 kCFBooleanTrue);
        
        iStream0 = (__bridge NSInputStream *)readStream0;
      
        [iStream0 setDelegate:self];
        [iStream0 scheduleInRunLoop:[NSRunLoop currentRunLoop]
                            forMode:NSDefaultRunLoopMode];
        [iStream0 open];
        
        oStream0 = (__bridge NSOutputStream *)writeStream0;
        [oStream0 setDelegate:self];
        [oStream0 scheduleInRunLoop:[NSRunLoop currentRunLoop]
                            forMode:NSDefaultRunLoopMode];
        [oStream0 open];
    }
}

-(void) writeToServer:(const uint8_t *) buf {
    [oStream0 write:buf maxLength:strlen((char*)buf)];
}

- (void)stream:(NSStream *)stream handleEvent:(NSStreamEvent)eventCode {

    if (stream == iStream0) {
        
            NSLog(@"NSStreamEventHasBytesAvailable!");
    if (self.data == nil) {
        self.data = [[NSMutableData alloc] init];
    }
        
        [[[AppDelegate sharedDelegate] hudRequest] setHidden:YES];
        
    uint8_t buf[1024];
    unsigned int len = 0;
    NSMutableData* responseData = [[NSMutableData alloc] initWithCapacity:0];
    
    while ([iStream0 hasBytesAvailable]) {
        len = [iStream0 read:buf maxLength:1024];
        if (len > 0 && len<=1024) {
            
            [responseData appendBytes:(const void *)buf length:len];
            //   [NSThread sleepForTimeInterval:.1];
        }
    }
    [self.data appendData:responseData];
    NSString *str = [[NSString alloc] initWithData:self.data
                                          encoding:NSUTF8StringEncoding];
    NSLog(@"push %@",str);
        
          NSUserDefaults *ndf = [NSUserDefaults standardUserDefaults];
       
        NSArray *arr = [str componentsSeparatedByString:@"||\r\n"];
        
//        NSLog(@"mang ===== %@",arr);
        for (NSString *strSub in arr) {
            
       
//        NSString *firt = [arr objectAtIndex:0];
        if (![strSub isEqualToString:@""]) {
             NSArray *aaa = [strSub componentsSeparatedByString:@"|"];
            NSString *requsetName = [aaa objectAtIndex:0];
//            NSLog(@"fuck ======== %@",requsetName);
            if ([requsetName isEqualToString:@"XSSYNC_RESP"]) {
//                for (NSString *resulft in arr) {
//                    if (![resulft isEqualToString:@""]) {
//                        NSArray *arrSub = [resulft componentsSeparatedByString:@"|"];
//                        NSString *date = [ndf objectForKey:[arrSub objectAtIndex:3]];
//                        if (![date isEqualToString:[arrSub objectAtIndex:2]]) {
////                            NSLog(@"ma tinh %@ with time %@ ,with date %@",[arrSub objectAtIndex:3],[arrSub objectAtIndex:2],date);
//                            [ndf setObject:[arrSub objectAtIndex:2] forKey:[arrSub objectAtIndex:3]];
//                            [ndf synchronize];
//                            if (_delegate && [_delegate respondsToSelector:@selector(data_XSSYNC_RESP_Recieved:)]) {
//                                 [_delegate data_XSSYNC_RESP_Recieved:resulft];
//                            }
//                           
//                        }
//                    }
//                }
                if (_delegate && [_delegate respondsToSelector:@selector(data_XSSYNC_RESP_Recieved:)]) {
                    [_delegate data_XSSYNC_RESP_Recieved:strSub];
                }
            }
            else if ([requsetName isEqualToString:@"TKCAU2_RESP"])
            {
                
                if (_delegate && [_delegate respondsToSelector:@selector(data_TKCAU_RESP_Recieved:)]) {
                  [_delegate data_TKCAU_RESP_Recieved:strSub];
                }
            }
            else if ([requsetName isEqualToString:@"XS_CAU_RESP"])
            {
                
                if (_delegate && [_delegate respondsToSelector:@selector(data_XS_CAU_RESP_Recieved:)]) {
                    [_delegate data_XS_CAU_RESP_Recieved:strSub];
                }
            }
        }
        }
            
       
    }

}

-(void) disconnect {
    NSLog(@"Relear hereqqaaaaaaaaaa");
    isConnect =FALSE;
    [iStream0 close];
    [oStream0 close];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)mainconnect:(NSString*)textField{
       //    NSLog(@"Slave serrver111:%@ - %d",[Data getSlaveIP],[Data getSlavePort]);
    if (!isConnect) {
        //        NSLog(@"Slave serrver:%@ - %d",[Data getSlaveIP],[Data getSlavePort]);
        [self connectToServerUsingStream:[AppDelegate sharedDelegate].serverZ portNo:[AppDelegate sharedDelegate].portZ];
    }
    sumString=@"";
    self.sumStr30=@"";
    //    NSLog(@"send:%@", textField);
    NSString* message1 = [NSString stringWithFormat:@"%@||\r\n", textField];
    NSLog(@"send:%@", message1);
    [self writeToServer:(uint8_t *)[message1 cStringUsingEncoding:NSASCIIStringEncoding]];
    
}

-(void)connect:(NSString*)textField{
    //    NSLog(@"Slave serrver111:%@ - %d",[Data getSlaveIP],[Data getSlavePort]);
//    if (!isConnect) {
        //        NSLog(@"Slave serrver:%@ - %d",[Data getSlaveIP],[Data getSlavePort]);
        [self connectToServerUsingStream:[AppDelegate sharedDelegate].serverZ portNo:[AppDelegate sharedDelegate].portZ];
//    }
    //    sumString=@"";
    //    self.sumStr30=@"";
    //    //    NSLog(@"send:%@", textField);
    //    NSString* message1 = [NSString stringWithFormat:@"%@||\r\n", textField];
    //    NSLog(@"send:%@", message1);
    //    [self writeToServer:(uint8_t *)[message1 cStringUsingEncoding:NSASCIIStringEncoding]];
    
}
@end
