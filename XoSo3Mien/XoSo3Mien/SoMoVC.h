//
//  SoMoVC.h
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SoMoVC : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *tfSearch;

@property (strong, nonatomic) IBOutlet UILabel *lblboso;

@property (strong, nonatomic) IBOutlet UIImageView *imgAddV;

@property (strong, nonatomic) IBOutlet UILabel *lblten;
@property (strong, nonatomic) IBOutlet UITableView *tblKetQua;
@property (strong, nonatomic) IBOutlet UILabel *tittleVC;
@property (strong, nonatomic) IBOutlet UINavigationBar *navTu;
@end
