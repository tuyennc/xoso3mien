//
//  SoMoVC.m
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import "SoMoVC.h"

@interface SoMoVC ()<UITextFieldDelegate>
{
    ECSlidingViewController  *slidingViewController;
    LogInOutVC   *logInOut;
     ASIHTTPRequest *request;
    NSMutableArray *arrSoMo;
      BOOL isFiltered;
    NSMutableArray *filteredTableData;
     DangKiVC     *dangKi;
      AccountInfor   *accInfor;
    int randomY;
}
@end

@implementation SoMoVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.slidingViewController resetTopView];
    self.navTu.topItem.title = @"";
    _tittleVC.text =@"Giải mã giấc mơ";
     _tittleVC.font = [UIFont fontWithName:XSFontCondensed size:20];
    _tfSearch.textColor =RGB(54, 54, 54);
    
    _tfSearch.font = [UIFont fontWithName:XSFontCondensed size:17];
    [self requestSoMo];
}
#pragma mark
#pragma mark Request ket qua
- (void)requestSoMo
{
    [[AppDelegate sharedDelegate].hudRequest show:YES];
//    http://appsrv.sms.vn/app/xs?action=somo
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://appsrv.sms.vn/app/xs?action=somo"]];
    NSLog(@"requset :%@",url);
    request = [ASIHTTPRequest requestWithURL:url];
    
    [request setCompletionBlock:^{
        
        
        
        [[AppDelegate sharedDelegate].hudRequest hide:YES];
        // Use when fetching text data
        NSString *responseString = [request responseString];
        NSLog(@"%@",responseString);
        //          NSData *responseData = [request responseData];
        arrSoMo = [[responseString JSONValue] objectForKey:@"items"];
        [_tblKetQua reloadData];
        
    }];
    [request setFailedBlock:^{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Không thể kết nối" delegate:self cancelButtonTitle:@"Kết nối lại" otherButtonTitles:@"Huỷ", nil] ;
        [alert show];
        //        [UIHelper showMessage:@"Không thể kết nối" withTitle:@""];
    }];
    [request startAsynchronous];
    
}

#pragma mark
#pragma mark - tableview delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
    if(isFiltered)
        return [filteredTableData count];
    else
        return  arrSoMo.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   
        return 30;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor whiteColor];
    }
    [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    UILabel *lblName = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,160, 30)];
     UILabel *lblBoso = [[UILabel alloc] initWithFrame:CGRectMake(160, 0,160, 30)];
  lblName.font =  [UIFont fontWithName:XSFontCondensed size:17];
   lblBoso.font =  [UIFont fontWithName:XSFontCondensed size:17];
    lblName.textColor =  RGB(54, 54, 54);
    lblBoso.textColor =  RGB(54, 54, 54);

    CALayer *bottomBorder = [CALayer layer];
    
    bottomBorder.frame = CGRectMake(0.0f, 0,1 , 30);
    
    bottomBorder.backgroundColor = [UIColor lightGrayColor].CGColor;
    
    [lblBoso.layer addSublayer:bottomBorder];
    
    if (!isFiltered)
    {
        lblName.text =[NSString stringWithFormat:@"   %@",[[arrSoMo objectAtIndex:indexPath.row] objectForKey:@"name"]] ;
    lblBoso.text = [NSString stringWithFormat:@"   %@",[[arrSoMo objectAtIndex:indexPath.row] objectForKey:@"number"]];
    }
    else
    {
        lblName.text = [[filteredTableData objectAtIndex:indexPath.row] objectForKey:@"name"];
        lblBoso.text = [[filteredTableData objectAtIndex:indexPath.row] objectForKey:@"number"];
    }
    
    [cell.contentView addSubview:lblName];
     [cell.contentView addSubview:lblBoso];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
   NSString * searchStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSLog(@"asdasd %@",searchStr);
    if(searchStr.length == 0)
    {
        isFiltered = FALSE;
    }
    else
    {
        isFiltered = true;
        filteredTableData = [[NSMutableArray alloc] init];
        
        for (NSDictionary*dict in arrSoMo)
        {
            NSRange nameRange = [[dict objectForKey:@"name"] rangeOfString:searchStr options:NSCaseInsensitiveSearch];
            
            if(nameRange.location != NSNotFound )
            {
                [filteredTableData addObject:dict];
            }
        }
    }
    
    [_tblKetQua reloadData];
    return YES;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    int minY = 1;
    int maxY = 3;
    int rangeY = maxY - minY;
  randomY = (arc4random() % rangeY) + minY;
    if (randomY==1) {
        NSURL *url   = [[NSBundle mainBundle] URLForResource:@"anhdong" withExtension:@"gif"];
        _imgAddV.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    }
    else
        _imgAddV.image = [UIImage imageNamed:@"banner-ole-mobile.jpg"];
    
    arrSoMo = [[NSMutableArray alloc] init];
    // Do any additional setup after loading the view from its nib.
    UIImage *imgItem =[ Utility resizeImg:[UIImage imageNamed:@"iconmenu.png"] withSize:CGSizeMake(30, 30*30/50)] ;
    
    self.navTu.topItem.leftBarButtonItem = [UIBarButtonItem barItemWithImage:imgItem target:self action:@selector(leftBtnClicked:)];
    if (IS_OS_7_OR_LATER) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        [self setNeedsStatusBarAppearanceUpdate];
        UIView *viewSta = [[UIView alloc] initWithFrame:CGRectMake(0, -20, 320, 20)];
        viewSta.backgroundColor = RGB(106, 0, 0);
        
        [self.view addSubview:viewSta];
        self.navTu.barTintColor = RGB(106, 0, 0);
    }
    else
    {
        self.navTu.TintColor = RGB(106, 0, 0);
    }
    
    //    ecsliding
    slidingViewController = [[ECSlidingViewController alloc] init];
    MenuVC *menuVC   = [[MenuVC alloc] initWithNibName:@"MenuVC" bundle:nil];
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    self.slidingViewController.underLeftViewController  = menuVC;
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    _lblboso.font = [UIFont fontWithName:XSFontCondensed size:17];
      _lblten.font = [UIFont fontWithName:XSFontCondensed size:17];
//    self.tblKetQua.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 30.0f)] ;
//    
//    
//    self.tblKetQua.tableHeaderView.backgroundColor = RGB(211, 211, 211);
    CALayer *bottomBorder = [CALayer layer];
    
    bottomBorder.frame = CGRectMake(0.0f, 0,1 , 30);
    
    bottomBorder.backgroundColor = [UIColor lightGrayColor].CGColor;
    
    [_lblboso.layer addSublayer:bottomBorder];
    
    self.tblKetQua.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 1.0f)] ;
    self.tblKetQua.tableFooterView.backgroundColor = [UIColor lightGrayColor];
    //    self.tblMenu.tableFooterView.hidden = YES;
    
    if (IS_OS_7_OR_LATER) {
        [self.tblKetQua setSeparatorInset:UIEdgeInsetsZero];
    }
    self.tblKetQua.separatorColor =  [UIColor lightGrayColor];
    //    login
    UIButton *btnSelectDate=[UIButton buttonWithType:UIButtonTypeCustom];
    btnSelectDate.frame = CGRectMake(0, 0,30,30);
    
    if ([[AppDelegate sharedDelegate] isLogin]) {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"Logged.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    else
    {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"LogInOut.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    [btnSelectDate addTarget:self action:@selector(pushViewLogIO) forControlEvents:UIControlEventTouchUpInside];
    self.navTu.topItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSelectDate];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateImageLogin)
                                                 name:@"UPDATE_IMAGE_LOGIN"
                                               object:nil];
    
    
    
}
- (IBAction)leftBtnClicked:(id)sender
{
    [[[UIApplication sharedApplication]keyWindow]endEditing:YES];
    [self.slidingViewController anchorTopViewTo:ECRight];
}
#pragma mark
#pragma mark loginout delete

-(void)dangKi
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    dangKi  = [[DangKiVC alloc] initWithNibName:@"DangKiVC" bundle:nil]  ;
     dangKi.delegate = self;
    [self presentPopupViewController:dangKi animationType:MJPopupViewAnimationFade];
    
}
-(void)closePopUp
{
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
    
}
-(void)updateImageLogin
{
    UIButton *btnSelectDate=[UIButton buttonWithType:UIButtonTypeCustom];
    btnSelectDate.frame = CGRectMake(0, 0,30,30);
    
    if ([[AppDelegate sharedDelegate] isLogin]) {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"Logged.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    else
    {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"LogInOut.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    [btnSelectDate addTarget:self action:@selector(pushViewLogIO) forControlEvents:UIControlEventTouchUpInside];
    self.navTu.topItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSelectDate];
}

-(void)pushViewLogIO
{
    if ([[AppDelegate sharedDelegate ] isLogin]==NO) {
        logInOut  = [[LogInOutVC alloc] initWithNibName:@"LogInOutVC" bundle:nil]  ;
        logInOut.delegate = self;
        [self presentPopupViewController:logInOut animationType:MJPopupViewAnimationFade];
        
    }
    else
    {
        accInfor  = [[AccountInfor alloc] initWithNibName:@"AccountInfor" bundle:nil]  ;
        accInfor.delegate = self;
        [self presentPopupViewController:accInfor animationType:MJPopupViewAnimationFade];
        
    }}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)quangCao:(id)sender {
    
    if (randomY==1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:LINK_OLIMPIA]];
    }
 else
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:LINK_OLE]];
    
}

@end
