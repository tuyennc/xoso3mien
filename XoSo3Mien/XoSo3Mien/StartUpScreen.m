//
//  StartUpScreen.m
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import "StartUpScreen.h"
#import "MasterServerConnect.h"
#import "InitialSlidingViewController.h"


@interface StartUpScreen ()<MasterServerDelegate>
{
    MasterServerConnect *network;
    UIActivityIndicatorView *spinner;
}
@end

@implementation StartUpScreen

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [[AppDelegate sharedDelegate] setCurrentApp:self];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        self.navigationController.navigationBar.translucent = NO;
        //        self.navigationController.navigationBar.barTintColor = [Data getNaviBarColor];
        self.navigationController.navigationBar.barTintColor = [UIColor redColor];
        self.navigationController.navigationBar.translucent = NO;
    }
    // Do any additional setup after loading the view from its nib.
}
-(void) viewDidAppear:(BOOL)animated{

    [super viewDidAppear:animated];
    [self reloadPushData];
}
- (void)reloadPushData
{
//    //        NSLog(@"Begin request");
    
    
 
  spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    spinner.frame =(IS_IPHONE5)?CGRectMake(145, 317, 24, 24):CGRectMake(145, 230, 24, 24);
    
    [self.view addSubview:spinner];
    [spinner startAnimating];
      network=[[MasterServerConnect alloc] init];
    network.delegate = self;
    [network mainconnect:@"SLAVE_REQ|0|0"];
////    NSThread *processor = [[NSThread alloc] initWithTarget:self selector:@selector(sendRequest) object:nil];
////    [processor start];
    
    
    
}

-(void)getDataSucsess
{
    [network disconnect];
    [spinner stopAnimating];
    InitialSlidingViewController *initialView = [[InitialSlidingViewController alloc] initWithNibName:@"InitialSlidingViewController" bundle:nil];
    
    UINavigationController * navigationController = [[UINavigationController alloc]initWithRootViewController:initialView];
    [AppDelegate sharedDelegate].hudRequest = [MBProgressHUD showHUDAddedTo:navigationController.view animated:YES];
    [AppDelegate sharedDelegate].hudRequest.mode=MBProgressHUDModeCustomView ;
    [AppDelegate sharedDelegate].hudRequest.labelText = @"Loadding...";
    
    //    navigationController.navigationBar.translucent = NO;
    [self presentModalViewController:navigationController animated:NO];
    

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
