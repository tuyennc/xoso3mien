//
//  ThemVeSoVC.h
//  XoSo3Mien
//
//  Created by Tuyen on 1/29/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol  ThemVeSoDelegate <NSObject>

- (void) themVeSo:(NSString*)veSo withDate:(NSString*)ngay withMatinh:(NSString*)maTinh withTentinh:(NSString *)tenTinh withTrung:(NSMutableArray*)isTrung withKetQua:(NSDictionary*)dictKetQua;


@end

@interface ThemVeSoVC : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *tfSoVe;
@property (strong, nonatomic) IBOutlet UIButton *btnNgay;
@property (strong, nonatomic) IBOutlet UIButton *btnXemKetQua;
@property (strong, nonatomic) IBOutlet UIButton *btnTinh;
@property (nonatomic, assign) id<ThemVeSoDelegate> delegate;

@end
