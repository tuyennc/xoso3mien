//
//  ThemVeSoVC.m
//  XoSo3Mien
//
//  Created by Tuyen on 1/29/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import "ThemVeSoVC.h"
#import "ChonLichSubView.h"
#import "ChonTinhSubView.h"

@interface ThemVeSoVC ()<ChonLichDelegate, ChonTinhDelegate>
{
    ChonLichSubView *chonLichView;
    ChonTinhSubView *chonTinhView;
    NSString  *ngayChon;
     NSString  *nameTinhChon;
    NSString  *maTinhChon;
    ASIHTTPRequest *request;
     NSDictionary *dictKetQuaxS;

}

@end

@implementation ThemVeSoVC
@synthesize delegate= _delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    dictKetQuaxS = [[NSDictionary alloc] init];
    [_btnNgay setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [_btnNgay setContentVerticalAlignment:UIControlContentVerticalAlignmentTop];
    [_btnNgay setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 5.0f, 0.0f, 0.0f)];
    
    [_btnNgay setTitle:[NSString stringWithFormat:@"+   %@", [Utility getToday]] forState:UIControlStateNormal];
    [_btnTinh setTitle:[NSString stringWithFormat:@"+   %@", @"Miền Bắc"] forState:UIControlStateNormal];
    
    [_btnTinh setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [_btnTinh setContentVerticalAlignment:UIControlContentVerticalAlignmentTop];
    [_btnTinh setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 6.0f, 0.0f, 0.0f)];
    
    
    _btnNgay.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:17];
     _btnTinh.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:17];
    
      _btnXemKetQua.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:17];
    
    _btnNgay.layer.borderColor = RGB(106, 0, 0).CGColor;
    _btnNgay.layer.borderWidth = 1;
    _btnNgay.layer.cornerRadius = 10;
    _btnTinh.layer.borderColor = RGB(106, 0, 0).CGColor;
    _btnTinh.layer.borderWidth = 1;
    _btnTinh.layer.cornerRadius = 10;
    _btnXemKetQua.layer.cornerRadius = 10;
    
    
    _tfSoVe.layer.borderColor = RGB(106, 0, 0).CGColor;
    _tfSoVe.layer.borderWidth = 1;
    _tfSoVe.layer.cornerRadius = 10;
    
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ChonLichSubView" owner:self options:nil];
    chonLichView = (ChonLichSubView *)[nib objectAtIndex:0];
    chonLichView.frame = CGRectMake(33, 137, 210, 239);
    chonLichView.userInteractionEnabled = YES;
    [self.view addSubview:chonLichView];
    chonLichView.delegate = self;
    chonLichView.hidden = YES;
    
    NSArray *nibTinh = [[NSBundle mainBundle] loadNibNamed:@"ChonTinhSubView" owner:self options:nil];
    
    chonTinhView = (ChonTinhSubView *)[nibTinh objectAtIndex:0];
    chonTinhView.frame = CGRectMake(43, 180, 92, 194);
    chonTinhView.userInteractionEnabled = YES;
    [self.view addSubview:chonTinhView];
    chonTinhView.delegate = self;
    chonTinhView.arrNameTinh = (NSMutableArray*)[[Utility dsTinhCaNuoc] componentsSeparatedByString:@"#"];
    chonTinhView.arrCodeTinh = (NSMutableArray*)[[Utility dsMaTinhCaNuoc] componentsSeparatedByString:@"#"];
    
    chonTinhView.hidden = YES;
    maTinhChon = @"XSTD";
    nameTinhChon = @"Miền Bắc";

}

#pragma mark
#pragma mark  Reque
- (void)requestXSdate:(NSString*)date withTinh:(NSString*)tinh
{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://appsrv.sms.vn/app/xs?action=getkqxs&sdate=%@&edate=%@&code=%@",date,date,tinh]];
    NSLog(@"requset :%@",url);
    request = [ASIHTTPRequest requestWithURL:url];
    
    [request setCompletionBlock:^{
        // Use when fetching text data
        NSString *responseString = [request responseString];
        NSLog(@"%@",responseString);
        //          NSData *responseData = [request responseData];
        NSDictionary *responseDict = [[responseString JSONValue] objectForKey:@"items"];
        if (![responseDict isKindOfClass:[NSString class]] ) {
          
            NSMutableArray *arrResulft = [responseDict objectForKey:@"item"];
            dictKetQuaxS = [arrResulft objectAtIndex:0];
            if (![[dictKetQuaxS objectForKey:@"date"] isEqualToString:ngayChon]) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Chưa có dữ liệu cho ngày này !" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
                [alert show];
            }
            else
            {
//            _lblTenTinh.text = [NSString stringWithFormat:@"%@",[responseDict objectForKey:@"name"]];
//            _tittleVC.text = [NSString stringWithFormat:@"%@",[responseDict objectForKey:@"name"]];
//            _lblngay.text = [NSString stringWithFormat:@"Ngày %@",[dictKetQuaxS objectForKey:@"date"]];
                
                [self checkVeso];
            }
            
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Chưa có dữ liệu cho ngày này !" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
            [alert show];
        }
        
        
    }];
    [request setFailedBlock:^{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Không thể kết nối" delegate:self cancelButtonTitle:@"Kết nối lại" otherButtonTitles:@"Huỷ", nil] ;
        [alert show];
        //        [UIHelper showMessage:@"Không thể kết nối" withTitle:@""];
    }];
    [request startAsynchronous];
    
}

-(void)checkVeso
{
    
    NSString *veso = _tfSoVe.text;
    NSLog(@"check ve so %@",dictKetQuaxS);
    NSString *strGiai = @"Bạn đã trúng";

    
   NSString *strDb =[dictKetQuaxS objectForKey:@"DB"];
    NSString *g1 =  [dictKetQuaxS objectForKey:@"G1"];
   NSString *tg2 =  [dictKetQuaxS objectForKey:@"G2"];
   NSString *tg3 = [dictKetQuaxS objectForKey:@"G3"];
   NSString *tg4 = [dictKetQuaxS objectForKey:@"G4"];
   NSString *tg5 = [dictKetQuaxS objectForKey:@"G5"];
   NSString *tg6 = [dictKetQuaxS objectForKey:@"G6"];
   NSString *tg7 =  [dictKetQuaxS objectForKey:@"G7"];
   NSString *tg8 = [dictKetQuaxS objectForKey:@"G8"];
    
    
    NSMutableArray *arrTrung = [[NSMutableArray alloc] init];
    
    if ([veso isEqualToString:strDb]) {
        [arrTrung addObject:@"giải đặc biệt"];
    }
    
    if ([[veso substringFromIndex:veso.length - 2] isEqualToString:[strDb substringFromIndex:strDb.length-2]]) {
       [arrTrung addObject:@"giải khuyến khích"];
    }

    
    if ([[veso substringFromIndex:veso.length - g1.length] isEqualToString:g1]) {
        [arrTrung addObject:@"giải nhất"];
    }
    
    for (NSString*g2 in [tg2 componentsSeparatedByString:@"-"]) {
        if ([[veso substringFromIndex:veso.length - g2.length] isEqualToString:g2]) {
            [arrTrung addObject:@"giải hai"];
        }
    }
    for (NSString*g3 in [tg3 componentsSeparatedByString:@"-"]) {
    if ([[veso substringFromIndex:veso.length - g3.length] isEqualToString:g3]) {
        [arrTrung addObject:@"giải ba"];

    }}
        
         for (NSString*g4 in [tg4 componentsSeparatedByString:@"-"]) {
    if ([[veso substringFromIndex:veso.length - g4.length] isEqualToString:g4]) {
        [arrTrung addObject:@"giải bốn"];

    }
         }
    
    for (NSString*g5 in [tg5 componentsSeparatedByString:@"-"]) {
    if ([[veso substringFromIndex:veso.length - g5.length] isEqualToString:g5]) {
        [arrTrung addObject:@"giải năm"];
    }
    }
    
    for (NSString*g6 in [tg6 componentsSeparatedByString:@"-"]) {
    if ([[veso substringFromIndex:veso.length - g6.length] isEqualToString:g6]) {
        [arrTrung addObject:@"giải sáu"];

    }
    }
    
    for (NSString*g7 in [tg7 componentsSeparatedByString:@"-"]) {
    if ([[veso substringFromIndex:veso.length - g7.length] isEqualToString:g7]) {
        [arrTrung addObject:@"giải bảy"];

    }
    }
    if (tg8.length>0)
    {
        for (NSString*g8 in [tg8 componentsSeparatedByString:@"-"]) {
            if ([[veso substringFromIndex:veso.length - g8.length] isEqualToString:g8]) {
                [arrTrung addObject:@"giải tám"];
                
            }
    }
   
    }
    
    if (arrTrung.count>0) {
            if (_delegate && [_delegate respondsToSelector:@selector(themVeSo:withDate:withMatinh:withTentinh:withTrung:withKetQua:)]) {
                
                
                [_delegate themVeSo:_tfSoVe.text withDate:ngayChon withMatinh:maTinhChon withTentinh:nameTinhChon withTrung:[arrTrung
                 copy] withKetQua:dictKetQuaxS];
            }

        }
    else
    {
        if (_delegate && [_delegate respondsToSelector:@selector(themVeSo:withDate:withMatinh:withTentinh:withTrung:withKetQua:)]) {
            
            [_delegate themVeSo:_tfSoVe.text withDate:ngayChon withMatinh:maTinhChon withTentinh:nameTinhChon withTrung:nil  withKetQua:dictKetQuaxS];
        }

    }    
    

}

#pragma mark
#pragma mark chonLich && chonTinh delegate

- (void) chonNgay:(NSString *)date
{
    [_btnNgay setTitle:[NSString stringWithFormat:@"+   %@",date] forState:UIControlStateNormal];
  
    ngayChon = date;
    
   
   }

- (void) maTinhChon:(NSString *)maTinh withName:(NSString *)nameTinh
{
    [_btnTinh setTitle:[NSString stringWithFormat:@"+   %@",nameTinh] forState:UIControlStateNormal];
    chonTinhView.hidden = YES;
    maTinhChon = maTinh;
    nameTinhChon = nameTinh;
 
}

- (IBAction)xemketQUa:(id)sender {
    NSLog(@"ngay %@, ma tinh %@, ve so %@",ngayChon,maTinhChon,_tfSoVe.text);
    if (_tfSoVe.text.length<5) {
        UIAlertView *aler = [[UIAlertView alloc] initWithTitle:nil message:@"Nhập đúng số cần dò" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [aler show];
    }
    else
    {
        
        [self requestXSdate:ngayChon withTinh:maTinhChon];
    
    }
}
- (IBAction)chonNg:(id)sender {
     [self.view endEditing:YES];
    chonLichView.hidden = NO;
    chonTinhView.hidden = YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    chonLichView.hidden = YES;
    chonTinhView.hidden = YES;

}
- (IBAction)chontinh:(id)sender {
     [self.view endEditing:YES];
    chonTinhView.hidden = NO;
    chonLichView.hidden = YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 6 ) ? NO : YES;
}


@end
