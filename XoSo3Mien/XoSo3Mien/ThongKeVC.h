//
//  ThongKeVC.h
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThongKeVC : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *btnNhanh;
@property (strong, nonatomic) IBOutlet UIButton *btnDacBiet;
@property (strong, nonatomic) IBOutlet UILabel *tittleVC;
@property (strong, nonatomic) IBOutlet UIImageView *imgSelectBut;
@property (strong, nonatomic) IBOutlet UITextField *tfCoupe;


@property (strong, nonatomic) IBOutlet UITableView *tblKetQua;
@property (strong, nonatomic) IBOutlet UITableView *tblTypeTK;
@property (strong, nonatomic) IBOutlet UITableView *tblTKNhanh;
@property (strong, nonatomic) IBOutlet UITableView *tblTinh;
@property (strong, nonatomic) IBOutlet UITableView *tblNgay;


@property (strong, nonatomic) IBOutlet UIButton *btnSMS;
@property (strong, nonatomic) IBOutlet UINavigationBar *navTu;

@property (strong, nonatomic) IBOutlet UIButton *btnTypeTK;

@property (strong, nonatomic) IBOutlet UIView *viewDB;
@property (strong, nonatomic) IBOutlet UIView *viewNhanh;


@property (strong, nonatomic) IBOutlet UIButton *btnTK;
@property (strong, nonatomic) IBOutlet UIButton *btnNgay;
@property (strong, nonatomic) IBOutlet UIButton *btnVung;


@end
