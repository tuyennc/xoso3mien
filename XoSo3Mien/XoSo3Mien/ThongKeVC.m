//
//  ThongKeVC.m
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import "ThongKeVC.h"

@interface ThongKeVC ()<UITableViewDataSource,UITableViewDelegate>
{
    ECSlidingViewController  *slidingViewController;
    LogInOutVC   *logInOut;
    ASIHTTPRequest *request;
    NSMutableArray *arrDacBiet;
    NSMutableArray *arrNhanh;
    NSMutableArray *arrNhanhMost;
     NSMutableArray *arrNhanhLeast;
    NSString *typeDacbiet;
    NSString *typeDacbietTV;
    BOOL isTKNhanh;
    
    NSString *maVungChon;
    NSMutableDictionary *dictKetQuaxS;
    NSArray *arrTinh ;
    NSArray *arrMaTinh ;
    NSString *strSoNgay;
     DangKiVC     *dangKi;
      AccountInfor   *accInfor;
}
@end

@implementation ThongKeVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.slidingViewController resetTopView];
    self.navTu.topItem.title = @"";
    _tittleVC.text =@"Thông Kê Nhanh";
    _tittleVC.font = [UIFont fontWithName:XSFontCondensed size:20];
    _btnNhanh.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:17];
    _btnDacBiet.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:17];
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _tblTinh.layer.cornerRadius = 7;
    _tblNgay.layer.cornerRadius = 7;
    _btnSMS.layer.borderColor = RGB(106, 0, 0).CGColor;
    _btnSMS.layer.borderWidth = 1;
    _btnSMS.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:15];
    typeDacbiet = @"dau";
    typeDacbietTV = @"Đầu";
//    _tblTKNhanh.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 30.0f)] ;
    
    arrMaTinh = [[Utility dsMaTinhCaNuoc] componentsSeparatedByString:@"#"];
    arrTinh = [[Utility dsTinhCaNuoc] componentsSeparatedByString:@"#"];
    _btnDacBiet.backgroundColor =RGB(106, 0, 0);
    _btnNhanh.backgroundColor = RGB(181, 28, 28);
    [_btnTypeTK setTitle:[NSString stringWithFormat:@"+   %@",typeDacbietTV] forState:UIControlStateNormal];
    
    
    
    [_btnNgay setTitle:@"+  10 ngày" forState:UIControlStateNormal];
    strSoNgay = @"10";
    
    maVungChon = @"XSTD";
    [_btnVung setTitle:@"+  Miền Bắc" forState:UIControlStateNormal];
     [_btnSMS setTitle:[NSString stringWithFormat:@"CLICK  ĐỂ NHẬN THỐNG KÊ CẶP SÔ %@ (2000đ/ngày)",@"Miền Bắc"] forState:UIControlStateNormal] ;

    [_btnSMS setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [_btnSMS setContentVerticalAlignment:UIControlContentVerticalAlignmentTop];
    [_btnSMS setTitleEdgeInsets:UIEdgeInsetsMake(7.0f, 32.0f, 0.0f, 0.0f)];
    
  
    
    [Utility buttonXoSo:_btnTypeTK];
    [Utility buttonXoSo:_btnNgay];
    [Utility buttonXoSo:_btnVung];
//    [Utility buttonXoSo:_btnTK];
    _btnTK.layer.cornerRadius = 10;
    _btnTK.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:17];
    
    _tfCoupe.layer.borderColor = RGB(106, 0, 0).CGColor;
    _tfCoupe.layer.borderWidth = 1;
    _tfCoupe.font = [UIFont fontWithName:XSFontCondensed size:17];
    _tfCoupe.layer.cornerRadius = 10;
    arrDacBiet = [[NSMutableArray alloc] init];
    arrNhanh   = [[NSMutableArray alloc] init];
    arrNhanhMost   = [[NSMutableArray alloc] init];
    arrNhanhLeast   = [[NSMutableArray alloc] init];
    if (IS_OS_7_OR_LATER) {
        UIView *viewSta = [[UIView alloc] initWithFrame:CGRectMake(0, -20, 320, 20)];
        viewSta.backgroundColor = RGB(106, 0, 0);
        
        [self.view addSubview:viewSta];
        [_tblKetQua setSeparatorInset:UIEdgeInsetsZero];
        [_tblTypeTK setSeparatorInset:UIEdgeInsetsZero];
        [_tblTKNhanh setSeparatorInset:UIEdgeInsetsZero];
          [_tblNgay setSeparatorInset:UIEdgeInsetsZero];
          [_tblTinh setSeparatorInset:UIEdgeInsetsZero];
    }
    [_tblKetQua setSeparatorColor:[UIColor grayColor]];
    // Do any additional setup after loading the view from its nib.
    UIImage *imgItem =[ Utility resizeImg:[UIImage imageNamed:@"iconmenu.png"] withSize:CGSizeMake(30, 30*30/50)] ;
    
    self.navTu.topItem.leftBarButtonItem = [UIBarButtonItem barItemWithImage:imgItem target:self action:@selector(leftBtnClicked:)];
    if (IS_OS_7_OR_LATER) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        [self setNeedsStatusBarAppearanceUpdate];
        self.navTu.barTintColor = RGB(106, 0, 0);
    }
    else
    {
        self.navTu.TintColor = RGB(106, 0, 0);
    }
    //    ecsliding
    slidingViewController = [[ECSlidingViewController alloc] init];
    MenuVC *menuVC   = [[MenuVC alloc] initWithNibName:@"MenuVC" bundle:nil];
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    self.slidingViewController.underLeftViewController  = menuVC;
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    //    login
    UIButton *btnSelectDate=[UIButton buttonWithType:UIButtonTypeCustom];
    btnSelectDate.frame = CGRectMake(0, 0,30,30);
    
    if ([[AppDelegate sharedDelegate] isLogin]) {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"Logged.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    else
    {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"LogInOut.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    [btnSelectDate addTarget:self action:@selector(pushViewLogIO) forControlEvents:UIControlEventTouchUpInside];
    self.navTu.topItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSelectDate];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateImageLogin)
                                                 name:@"UPDATE_IMAGE_LOGIN"
                                               object:nil];
    
    
   
}
- (IBAction)leftBtnClicked:(id)sender
{
    [[[UIApplication sharedApplication]keyWindow]endEditing:YES];
    [self.slidingViewController anchorTopViewTo:ECRight];
}
-(void)pushViewLogIO
{
    if ([[AppDelegate sharedDelegate ] isLogin]==NO) {
        logInOut  = [[LogInOutVC alloc] initWithNibName:@"LogInOutVC" bundle:nil]  ;
        logInOut.delegate = self;
        [self presentPopupViewController:logInOut animationType:MJPopupViewAnimationFade];
        
    }
    else
    {
        accInfor  = [[AccountInfor alloc] initWithNibName:@"AccountInfor" bundle:nil]  ;
        accInfor.delegate = self;
        [self presentPopupViewController:accInfor animationType:MJPopupViewAnimationFade];
        
    }
}
#pragma mark
#pragma mark loginout delete

-(void)dangKi
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    dangKi  = [[DangKiVC alloc] initWithNibName:@"DangKiVC" bundle:nil]  ;
     dangKi.delegate = self;
    [self presentPopupViewController:dangKi animationType:MJPopupViewAnimationFade];
    
}
-(void)closePopUp
{
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
    
}
-(void)updateImageLogin
{
    UIButton *btnSelectDate=[UIButton buttonWithType:UIButtonTypeCustom];
    btnSelectDate.frame = CGRectMake(0, 0,30,30);
    
    if ([[AppDelegate sharedDelegate] isLogin]) {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"Logged.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    else
    {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"LogInOut.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    [btnSelectDate addTarget:self action:@selector(pushViewLogIO) forControlEvents:UIControlEventTouchUpInside];
    self.navTu.topItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSelectDate];
}

#pragma mark IBOutlet

- (IBAction)tkDacBiet:(id)sender {
    _tittleVC.text =@"Thống Kê Đặc Biệt";
    _viewDB.hidden = NO;
    _viewNhanh.hidden = YES;
    isTKNhanh = NO;
    _btnDacBiet.backgroundColor = RGB(181, 28, 28);
    _btnNhanh.backgroundColor =RGB(106, 0, 0);
   _imgSelectBut.frame = CGRectMake(236, 0, 9, 9);
}


- (IBAction)tkNhanh:(id)sender {
    _tittleVC.text =@"Thông Kê Nhanh";
    _viewDB.hidden = YES;
    _viewNhanh.hidden = NO;
    isTKNhanh = YES;
    _btnDacBiet.backgroundColor = RGB(106, 0, 0);
    _btnNhanh.backgroundColor =RGB(181, 28, 28);
    
     _imgSelectBut.frame = CGRectMake(71, 0, 9, 9);
}

- (IBAction)requestTKNHANH:(id)sender {
    
    [self requestTKNhanh:maVungChon withSoNgay:strSoNgay withNumber:[_tfCoupe.text integerValue]];
}


- (IBAction)typeThongke:(id)sender {
    _tblTypeTK.hidden = NO;
}

- (IBAction)showLich:(id)sender {
    [self.view endEditing:YES];
    _tblTinh.hidden = NO;
    _tblNgay.hidden = YES;
}

- (IBAction)showNgay:(id)sender {
    [self.view endEditing:YES];
    _tblTinh.hidden = YES;
    _tblNgay.hidden = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark Request Thong ke
- (void)requestTKDB:(NSString*)type
{
    [[AppDelegate sharedDelegate].hudRequest show:YES];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://appsrv.sms.vn/app/xs?action=tkdb&select=%@",type]];
    NSLog(@"requset :%@",url);
    request = [ASIHTTPRequest requestWithURL:url];
    
    [request setCompletionBlock:^{
        
        _tblTypeTK.hidden = YES;
        [[AppDelegate sharedDelegate].hudRequest hide:YES];
        // Use when fetching text data
        NSString *responseString = [request responseString];
        NSLog(@"%@",responseString);
        //          NSData *responseData = [request responseData];
        arrDacBiet = [[responseString JSONValue] objectForKey:@"items"];
        if (![arrDacBiet isKindOfClass:[NSString class]]) {
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"number"
                                                         ascending:YES] ;
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor] ;
            NSArray *arr = arrDacBiet;
            arr = [arr sortedArrayUsingDescriptors:sortDescriptors] ;
            arrDacBiet = [[NSMutableArray alloc] initWithArray:arr];
            [self.tblKetQua reloadData];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Chưa có dữ liệu cho ngày này" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
            [alert show];
        }
        
    }];
    [request setFailedBlock:^{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Không thể kết nối" delegate:self cancelButtonTitle:@"Kết nối lại" otherButtonTitles:@"Huỷ", nil] ;
        [alert show];
        //        [UIHelper showMessage:@"Không thể kết nối" withTitle:@""];
    }];
    [request startAsynchronous];
    
}
- (void)requestTKNhanh:(NSString*)maXS withSoNgay:(NSString*)soNgay withNumber:(int)couple
{
    
    if (_tfCoupe.text.length!=2) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Nhập đầy đủ cặp số" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        [self.view endEditing:YES];
    [[AppDelegate sharedDelegate].hudRequest show:YES];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://appsrv.sms.vn/app/xs?action=tk&code=%@&numberdate=%@&couple=%d",maXS,soNgay,couple]];
    NSLog(@"requset :%@",url);
    request = [ASIHTTPRequest requestWithURL:url];
    
    [request setCompletionBlock:^{
        
        [[AppDelegate sharedDelegate].hudRequest hide:YES];
        // Use when fetching text data
        NSString *responseString = [request responseString];
        NSLog(@"%@",responseString);
        //          NSData *responseData = [request responseData];
        NSDictionary *dict = [[responseString JSONValue] objectForKey:@"items"];
        if (![dict isKindOfClass:[NSString class]]) {
            NSString* tkcs = [dict objectForKey:@"tkcs"];
            [self convertToArr:tkcs];
            [self convertLeast:[dict objectForKey:@"least"]];
             [self convertMost:[dict objectForKey:@"most"]];
            
            [_tblTKNhanh reloadData];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Chưa có dữ liệu cho ngày này" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
            [alert show];
        }
        
    }];
    [request setFailedBlock:^{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Không thể kết nối" delegate:self cancelButtonTitle:@"Kết nối lại" otherButtonTitles:@"Huỷ", nil] ;
        [alert show];
        //        [UIHelper showMessage:@"Không thể kết nối" withTitle:@""];
    }];
    [request startAsynchronous];
    }
}

-(void)convertToArr:(NSString*)strTK
{
    NSArray *arr = [strTK componentsSeparatedByString:@","];
    [arrNhanh removeAllObjects];
    
    NSString *giai1 = [[arr objectAtIndex:1] substringFromIndex:2];
    NSString *giai2 = [[arr objectAtIndex:2] substringFromIndex:2];
    NSString *giai3 = [[arr objectAtIndex:3] substringFromIndex:2];
    NSString *giai4 = [[arr objectAtIndex:4] substringFromIndex:2];
    NSString *giai5 = [[arr objectAtIndex:5] substringFromIndex:2];
    NSString *giai6 = [[arr objectAtIndex:6] substringFromIndex:2];
    NSString *giai7 = [[arr objectAtIndex:7] substringFromIndex:2];
    NSString *giai8 = [[arr objectAtIndex:8] substringFromIndex:2];
    NSString *giai0 = [[arr objectAtIndex:0] substringFromIndex:2];
    
    if (![giai0 isEqualToString:@""]) {
        [arrNhanh addObject:[NSString stringWithFormat:@"Giải đặc biệt:%@",giai0]];
    }
    if (![giai1 isEqualToString:@""]) {
        [arrNhanh addObject:[NSString stringWithFormat:@"Giải nhất:%@",giai1]];
    }
    if (![giai2 isEqualToString:@""]) {
        [arrNhanh addObject:[NSString stringWithFormat:@"Giải nhì:%@",giai2]];
    }
    if (![giai3 isEqualToString:@""]) {
        [arrNhanh addObject:[NSString stringWithFormat:@"Giải ba:%@",giai3]];
    }
    if (![giai4 isEqualToString:@""]) {
        [arrNhanh addObject:[NSString stringWithFormat:@"Giải bốn:%@",giai4]];
    }
    if (![giai5 isEqualToString:@""]) {
        [arrNhanh addObject:[NSString stringWithFormat:@"Giải năm:%@",giai5]];
    }
    if (![giai6 isEqualToString:@""]) {
        [arrNhanh addObject:[NSString stringWithFormat:@"Giải sáu:%@",giai6]];
    }
    if (![giai7 isEqualToString:@""]) {
        [arrNhanh addObject:[NSString stringWithFormat:@"Giải bảy:%@",giai7]];
    }
    if (![giai8 isEqualToString:@""]) {
        [arrNhanh addObject:[NSString stringWithFormat:@"Giải tám:%@",giai8]];
    }
    
    
}

-(void)convertMost:(NSString *)strTK
{
    [arrNhanhMost removeAllObjects];

    NSArray *arr = [strTK componentsSeparatedByString:@","];

    for (NSString *strTemp in arr) {
       
        [arrNhanhMost addObject:strTemp];
    }

}

-(void)convertLeast:(NSString *)strTK
{
    [arrNhanhLeast removeAllObjects];
    
    NSArray *arr = [strTK componentsSeparatedByString:@","];

    for (NSString *strTemp in arr) {
        
        [arrNhanhLeast addObject:strTemp];
    }

    
}

#pragma mark
#pragma mark Tableview Delegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == _tblTKNhanh) {
        return 3;
    }
    else
    {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == _tblKetQua) {
        return  arrDacBiet.count+1;
    }
    else if(tableView == _tblTypeTK)
    {
        return 4;
    }
    else if(tableView == _tblTKNhanh)
    {
        if (section==0) {
            return (arrNhanh.count>=1)?(arrNhanh.count):0;
        }
        else
            return (arrNhanhMost.count>0)?5:0;
    }
     else if(tableView == _tblNgay)
     {
         return 3;
     }
     else if(tableView == _tblTinh)
     {
         return arrMaTinh.count;
     }
    else
        return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tblTKNhanh) {
        return 30;
    }
    else
    return 40;
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Celldss";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = RGB(243, 243, 243);
//    }
    
    [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    //    60,13
    if (tableView == _tblKetQua) {
        
        UILabel *lbl1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 60, 40)];
        lbl1.font = [UIFont fontWithName:XSFontCondensed size:17];
        lbl1.textAlignment = NSTextAlignmentCenter;
        UILabel *lbl2 = [[UILabel alloc] initWithFrame:CGRectMake(60, 0, 130, 40)];
        lbl2.font = [UIFont fontWithName:XSFontCondensed size:17];
        lbl2.textAlignment = NSTextAlignmentCenter;
        UILabel *lbl3 = [[UILabel alloc] initWithFrame:CGRectMake(190, 0, 130, 40)];
        lbl3.font = [UIFont fontWithName:XSFontCondensed size:17];
        lbl3.textAlignment = NSTextAlignmentCenter;
        lbl1.backgroundColor = RGB(243, 243, 243);
        lbl2.backgroundColor = RGB(243, 243, 243);
        lbl3.backgroundColor = RGB(243, 243, 243);
        lbl1.textColor = RGB(39, 39, 39);
        lbl2.textColor = RGB(39, 39, 39);
        lbl3.textColor = RGB(39, 39, 39);
        
        CALayer *bottomBorder = [CALayer layer];
        
        bottomBorder.frame = CGRectMake(0.0f, 0,1 , 40);
        
        bottomBorder.backgroundColor = [UIColor lightGrayColor].CGColor;
        
        CALayer *bottomBorder2 = [CALayer layer];
        
        bottomBorder2.frame = CGRectMake(0.0f, 0,1 , 40);
        
        bottomBorder2.backgroundColor = [UIColor lightGrayColor].CGColor;
        [lbl2.layer addSublayer:bottomBorder];
        
        [lbl3.layer addSublayer:bottomBorder2];
        
        
        if (indexPath.row==0) {
            lbl1.text = typeDacbietTV;
            lbl2.text= @"Ngày về";
            lbl3.text= @"Chưa ra";
            lbl1.backgroundColor = RGB(216, 216, 216);
            lbl2.backgroundColor = RGB(216, 216, 216);
            lbl3.backgroundColor = RGB(216, 216, 216);
            lbl1.textColor = RGB(106  , 0, 0);
            lbl2.textColor = RGB(106  , 0, 0);
            lbl3.textColor = RGB(106  , 0, 0);
        }
        else
        {
            lbl1.text = [[[arrDacBiet objectAtIndex:indexPath.row-1] objectForKey:@"number"] stringValue
                         ];
            lbl2.text=  [[arrDacBiet objectAtIndex:indexPath.row-1] objectForKey:@"date"];
            
            lbl3.text= [[[arrDacBiet objectAtIndex:indexPath.row-1] objectForKey:@"chuara"] stringValue];
        }
        [cell.contentView addSubview:lbl1];
        [cell.contentView addSubview:lbl2];
        [cell.contentView addSubview:lbl3];
    }
    else if (tableView ==_tblTypeTK)
    {
        if (indexPath.row==0) {
            cell.textLabel.text  = @"Đầu";
            
        }else if (indexPath.row==1)
        {
            cell.textLabel.text  = @"Đuôi";
            
        }
        else if (indexPath.row==2)
        {
            cell.textLabel.text  = @"Tổng";
            
            
        }
        else if (indexPath.row==3)
        {
            cell.textLabel.text  = @"Bộ";
            
            
        }
        cell.textLabel.font = [UIFont fontWithName:XSFontCondensed size:17];
        //        lbl3.textAlignment = NSTextAlignmentCenter;
        cell.backgroundColor = RGB(106, 0, 0);
        cell.textLabel.textColor = [UIColor whiteColor];
    }
    else if (tableView ==_tblTKNhanh)
    {
        if (indexPath.section==0) {
            
            NSArray *arrayTemp = [[arrNhanh objectAtIndex:indexPath.row] componentsSeparatedByString:@":"];
             UILabel*lblGiai = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 160, 30)];
                lblGiai.textAlignment = NSTextAlignmentCenter;
            lblGiai.font = [UIFont fontWithName:XSFontCondensed size:17];
            lblGiai.backgroundColor = [UIColor clearColor];
            
            lblGiai.text = [arrayTemp objectAtIndex:0];
            
            
            
            UILabel*lblLan = [[UILabel alloc] initWithFrame:CGRectMake(160, 0, 160, 30)];
            lblLan.textAlignment = NSTextAlignmentCenter;
            lblLan.font = [UIFont fontWithName:XSFontCondensed size:17];
            lblLan.backgroundColor = [UIColor clearColor];
            
            lblLan.text = [arrayTemp objectAtIndex:1];
            CALayer *bottomBorder = [CALayer layer];
            
            bottomBorder.frame = CGRectMake(0.0f, 0,1 , 30);
            
            bottomBorder.backgroundColor = [UIColor lightGrayColor].CGColor;
            
            [lblLan.layer addSublayer:bottomBorder];

            [cell.contentView addSubview:lblGiai];
             [cell.contentView addSubview:lblLan];

//            cell.textLabel.text = [arrNhanh objectAtIndex:indexPath.row];
        }
        else if(indexPath.section==1)
        {
            NSString *strTemp1 = [arrNhanhMost objectAtIndex:indexPath.row*2];
              NSString *strTemp2 = [arrNhanhMost objectAtIndex:indexPath.row*2+1];
            
            UILabel*lblCap1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
            lblCap1.text = [strTemp1 substringToIndex:2];
            UILabel*lblSL1 = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 110, 30)];
            lblSL1.text = [NSString stringWithFormat:@"%@ lần", [strTemp1 substringFromIndex:3] ];
            
            UILabel*lblCap2 = [[UILabel alloc] initWithFrame:CGRectMake(160, 0, 50, 30)];
            lblCap2.text = [strTemp2 substringToIndex:2];
            UILabel*lblSL2 = [[UILabel alloc] initWithFrame:CGRectMake(210, 0, 110, 30)];
            lblSL2.text =[NSString stringWithFormat:@"%@ lần",  [strTemp2 substringFromIndex:3]];

            
            lblCap1.textAlignment = NSTextAlignmentCenter;
             lblSL1.textAlignment = NSTextAlignmentCenter;
             lblCap2.textAlignment = NSTextAlignmentCenter;
             lblSL2.textAlignment = NSTextAlignmentCenter;
            
            lblCap1.font = [UIFont fontWithName:XSFontCondensed size:17];
            lblSL1.font = [UIFont fontWithName:XSFontCondensed size:17];
            lblCap2.font = [UIFont fontWithName:XSFontCondensed size:17];
            lblSL2.font = [UIFont fontWithName:XSFontCondensed size:17];
            
             lblCap1.backgroundColor = [UIColor clearColor];
            lblSL1.backgroundColor = [UIColor clearColor];
             lblCap2.backgroundColor = [UIColor clearColor];
             lblSL2.backgroundColor = [UIColor clearColor];
            
            
            CALayer *bottomBorder = [CALayer layer];
            
            bottomBorder.frame = CGRectMake(0.0f, 0,1 , 30);
            
            bottomBorder.backgroundColor = [UIColor lightGrayColor].CGColor;
            
            [lblSL1.layer addSublayer:bottomBorder];
            
            
            CALayer *bottomBorder2 = [CALayer layer];
            
            bottomBorder2.frame = CGRectMake(0.0f, 0,1 , 30);
            
            bottomBorder2.backgroundColor = [UIColor lightGrayColor].CGColor;
            
            [lblCap2.layer addSublayer:bottomBorder2];
            
            
            CALayer *bottomBorder3 = [CALayer layer];
            
            bottomBorder3.frame = CGRectMake(0.0f, 0,1 , 30);
            
            bottomBorder3.backgroundColor = [UIColor lightGrayColor].CGColor;
            
            [lblSL2.layer addSublayer:bottomBorder3];
            
            [cell.contentView addSubview:lblCap1];
            [cell.contentView addSubview:lblSL1];
            [cell.contentView addSubview:lblCap2];
            [cell.contentView addSubview:lblSL2];

        }
        else if(indexPath.section==2)
        {
            NSString *strTemp1 = [arrNhanhLeast objectAtIndex:indexPath.row*2];
            NSString *strTemp2 = [arrNhanhLeast objectAtIndex:indexPath.row*2+1];
            
            UILabel*lblCap1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
            lblCap1.text = [strTemp1 substringToIndex:2];
            UILabel*lblSL1 = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 110, 30)];
            lblSL1.text = [NSString stringWithFormat:@"%@ lần", [strTemp1 substringFromIndex:3] ];
            
            UILabel*lblCap2 = [[UILabel alloc] initWithFrame:CGRectMake(160, 0, 50, 30)];
            lblCap2.text = [strTemp2 substringToIndex:2];
            UILabel*lblSL2 = [[UILabel alloc] initWithFrame:CGRectMake(210, 0, 110, 30)];
            lblSL2.text =[NSString stringWithFormat:@"%@ lần",  [strTemp2 substringFromIndex:3]];
            
            
            lblCap1.textAlignment = NSTextAlignmentCenter;
            lblSL1.textAlignment = NSTextAlignmentCenter;
            lblCap2.textAlignment = NSTextAlignmentCenter;
            lblSL2.textAlignment = NSTextAlignmentCenter;
            
            lblCap1.font = [UIFont fontWithName:XSFontCondensed size:17];
            lblSL1.font = [UIFont fontWithName:XSFontCondensed size:17];
            lblCap2.font = [UIFont fontWithName:XSFontCondensed size:17];
            lblSL2.font = [UIFont fontWithName:XSFontCondensed size:17];
            
            lblCap1.backgroundColor = [UIColor clearColor];
            lblSL1.backgroundColor = [UIColor clearColor];
            lblCap2.backgroundColor = [UIColor clearColor];
            lblSL2.backgroundColor = [UIColor clearColor];
            
            
            CALayer *bottomBorder = [CALayer layer];
            
            bottomBorder.frame = CGRectMake(0.0f, 0,1 , 30);
            
            bottomBorder.backgroundColor = [UIColor lightGrayColor].CGColor;
            
            [lblSL1.layer addSublayer:bottomBorder];
            
            
            CALayer *bottomBorder2 = [CALayer layer];
            
            bottomBorder2.frame = CGRectMake(0.0f, 0,1 , 30);
            
            bottomBorder2.backgroundColor = [UIColor lightGrayColor].CGColor;
            
            [lblCap2.layer addSublayer:bottomBorder2];
            
            
            CALayer *bottomBorder3 = [CALayer layer];
            
            bottomBorder3.frame = CGRectMake(0.0f, 0,1 , 30);
            
            bottomBorder3.backgroundColor = [UIColor lightGrayColor].CGColor;
            
            [lblSL2.layer addSublayer:bottomBorder3];
            
            [cell.contentView addSubview:lblCap1];
            [cell.contentView addSubview:lblSL1];
            [cell.contentView addSubview:lblCap2];
            [cell.contentView addSubview:lblSL2];
        }
        
    }
    else if(tableView == _tblTinh)
    {
        UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, 92, 40)];
        lbl.text = [arrTinh objectAtIndex:indexPath.row];
        lbl.font = [UIFont fontWithName:XSFontCondensed size:17];
        lbl.textColor = [UIColor whiteColor];
        cell.backgroundColor = RGB(106, 0, 0);
        UIImage *img = [Utility resizeImg:[UIImage imageNamed:@"arr_chontinh.png"] withSize:CGSizeMake(8, 12)];
        cell.imageView.image = img;
        
        [cell.contentView addSubview:lbl];
    }
    else if(tableView == _tblNgay)

    {
        UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, 92, 40)];
        lbl.text = [NSString stringWithFormat:@"%d ngày",(indexPath.row+1)*10];
        lbl.font = [UIFont fontWithName:XSFontCondensed size:17];
        lbl.textColor = [UIColor whiteColor];
        cell.backgroundColor = RGB(106, 0, 0);
        UIImage *img = [Utility resizeImg:[UIImage imageNamed:@"arr_chontinh.png"] withSize:CGSizeMake(8, 12)];
        cell.imageView.image = img;
        [cell.contentView addSubview:lbl];

        [cell.contentView addSubview:lbl];

    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView ==_tblTypeTK)
    {
        if (indexPath.row==0) {
            typeDacbietTV  = @"Đầu";
            typeDacbiet = @"dau";
        }else if (indexPath.row==1)
        {
            typeDacbietTV  = @"Đuôi";
            typeDacbiet = @"duoi";
        }
        else if (indexPath.row==2)
        {
            typeDacbietTV  = @"Tổng";
            typeDacbiet = @"tong";
            
        }
        else if (indexPath.row==3)
        {
            typeDacbietTV  = @"Bộ";
            typeDacbiet = @"bo";
            
        }
        [_btnTypeTK setTitle:[NSString stringWithFormat:@"+   %@",typeDacbietTV] forState:UIControlStateNormal];
        [self requestTKDB:typeDacbiet];
        
    }
    else if(tableView == _tblTinh)
    {
        maVungChon = [arrMaTinh objectAtIndex:indexPath.row];
        _tblTinh.hidden = YES;
        [_btnVung setTitle:[NSString stringWithFormat:@"+  %@",[arrTinh objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
             [_btnSMS setTitle:[NSString stringWithFormat:@"CLICK  ĐỂ NHẬN THỐNG KÊ CẶP SÔ %@ (2000đ/ngày)",[arrTinh objectAtIndex:indexPath.row]] forState:UIControlStateNormal] ;
    }
    else if(tableView == _tblNgay)
    {
        _tblNgay.hidden = YES;
               [_btnNgay setTitle:[NSString stringWithFormat:@"+  %d ngày",(indexPath.row+1)*10] forState:UIControlStateNormal];
        strSoNgay =[NSString stringWithFormat:@"%d",(indexPath.row+1)*10] ;
       
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 2) ? NO : YES;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"";
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView==_tblTKNhanh) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
        /* Create custom view to display section header... */
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
        [label setFont:[UIFont fontWithName:XSFontCondensed size:17]];
    
        if (section==0) {
            [label setText:@"Thông Kê Cặp Số "];
        }
        else if (section ==1)
        {
            [label setText:@"10 Cặp Số Về Nhiều Nhất"];
        }
        else
            [label setText:@"10 Cặp Số Về Ít Nhất"];
        
        label.textAlignment = NSTextAlignmentCenter;
        
        [view addSubview:label];
        view.backgroundColor= RGB(216, 216, 216);
        return view;
    }
    return nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView==_tblTKNhanh) {

    return 30;
    }
    else
        return 0;
}

#pragma ---
#pragma mark  sms
- (IBAction)guiSMS:(id)sender {
    
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
    controller.delegate = self;
    
    if([MFMessageComposeViewController canSendText])
    {
        NSString *toNumber = nil;
        if ([Utility isViettel]) {
            toNumber = DS_VIETTEL_CX;
            if ([maVungChon isEqualToString:@"XSTD"]) {
                controller.body = [NSString stringWithFormat:@"%@",@"CAU"];
            }
            else
            {
                NSString*matinh = maVungChon;
                
                controller.body = [matinh stringByReplacingOccurrencesOfString:@"XS" withString:@"CAU "];
            }
            controller.recipients = [NSArray arrayWithObjects:toNumber, nil];
            
        }
        else
        {
             toNumber = DS_NOT_VIETTEL;
            NSString *strOr = [Utility dsMaTinhMienTrung];
            if ([strOr rangeOfString:maVungChon].location!=NSNotFound) {
                controller.body = [NSString stringWithFormat:@"DK TK %@",@"MT"];

            }
            else
            {
                 controller.body = [NSString stringWithFormat:@"DK TK %@",@"MN"];
            }
            if ([maVungChon isEqualToString:@"XSTD"]) {
                controller.body = [NSString stringWithFormat:@"DK TK%@",@"MB"];
            }
                      controller.recipients = [NSArray arrayWithObjects:toNumber, nil];
            
            
        }
        controller.messageComposeDelegate = self;
        [self presentModalViewController:controller animated:YES];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
	switch (result) {
		case MessageComposeResultCancelled:
			NSLog(@"Cancelled");
			break;
		case MessageComposeResultFailed:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Không gửi được tin nhắn" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
        }
			break;
		case MessageComposeResultSent:
        {UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thành công" message:@"Gửi tin nhắn thành công" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
        }
			break;
		default:
			break;
	}
    
	[self dismissModalViewControllerAnimated:YES];
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    _tblNgay.hidden = YES;
    _tblTinh.hidden =YES;
}

@end
