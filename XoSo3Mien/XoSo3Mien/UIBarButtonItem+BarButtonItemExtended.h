//
//  UIBarButtonItem+BarButtonItemExtended.h
//  Paktor
//
//  Created by Hong Vu Xuan on 4/18/13.
//  Copyright (c) 2013 VMODEV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (BarButtonItemExtended) {
    
}

+ (UIBarButtonItem*)barItemWithImage:(UIImage*)image target:(id)target action:(SEL)action;
+ (UIBarButtonItem*)barItemWithText:(NSString*)text target:(id)target action:(SEL)action;
- (void)performBarButtonAction:(id)sender;

@end
