//
//  UIBarButtonItem+BarButtonItemExtended.m
//  Paktor
//
//  Created by Hong Vu Xuan on 4/18/13.
//  Copyright (c) 2013 VMODEV. All rights reserved.
//

#import "UIBarButtonItem+BarButtonItemExtended.h"

@implementation UIBarButtonItem (BarButtonItemExtended)

+ (UIBarButtonItem*)barItemWithImage:(UIImage*)image target:(id)target action:(SEL)action
{
    UIButton *imgButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [imgButton setImage:image forState:UIControlStateNormal];
    imgButton.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    
    UIBarButtonItem *b = [[UIBarButtonItem alloc]initWithCustomView:imgButton];
    
    [imgButton addTarget:b action:@selector(performBarButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [b setAction:action];
    [b setTarget:target];
    
    return b;
}
+ (UIBarButtonItem*)barItemWithText:(NSString*)text target:(id)target action:(SEL)action
{
    UIImage *image = [UIImage imageNamed:@"btn_top_right_blank"];
    UIButton *imgButton = [UIButton buttonWithType:UIButtonTypeCustom];    
    imgButton.titleLabel.font = [UIFont fontWithName:@"Calibri-Bold" size:15.0];
    imgButton.titleLabel.backgroundColor = [UIColor clearColor];
   
    [imgButton setTitle:text forState:UIControlStateNormal];
    [imgButton setBackgroundImage:image forState:UIControlStateNormal];   
    imgButton.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    
    UIBarButtonItem *b = [[UIBarButtonItem alloc]initWithCustomView:imgButton];
    
    [imgButton addTarget:b action:@selector(performBarButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [b setAction:action];
    [b setTarget:target];
    
    return b;
}
-(void)performBarButtonAction:(UIButton*)sender
{
    [[self target] performSelector:self.action withObject:self];
}

@end
