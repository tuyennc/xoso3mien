//
//  Utility.h
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SlaveServerConnect.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>

@interface Utility : NSObject


+ (UIImage* )resizeImg:(UIImage*)image withSize:(CGSize)newSize;
+ (NSString*)getToday;
+ (NSString*)dsTinhMienTrung;
+ (NSString*)dsTinhMienNam;
+(NSString*)dsMaTinhMienNam;
+(NSString*)dsMaTinhMienTrung;
+ (void) setSlaveConect:(SlaveServerConnect *) pin;
+ (SlaveServerConnect*)getSlaveConect;
+(NSString*)dsTinhCaNuoc;
+(NSString*)dsMaTinhCaNuoc;
+(NSString*)getTodayFullMin;
+(void)buttonXoSo:(UIButton*)btn;
+(BOOL)isViettel;

@end
