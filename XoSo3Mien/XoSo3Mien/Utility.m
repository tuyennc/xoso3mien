//
//  Utility.m
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import "Utility.h"

@implementation Utility


+ (UIImage* )resizeImg:(UIImage*)image withSize:(CGSize)newSize
{
//    NSLog(@"tuyen %f %f",image.size.height,image.size.width);
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
+(NSString*)getToday{
    NSDateFormatter * currentDateTime = [[NSDateFormatter alloc]init];
    [currentDateTime setDateFormat:@"dd/MM/yyyy"];
    //    NSDate *todayTime = [NSDate dateWithTimeInterval:(-11*60*60) sinceDate:[NSDate date]];
    NSDate *todayTime = [NSDate dateWithTimeInterval:(0) sinceDate:[NSDate date]];
    NSString * dateTimeString = [currentDateTime stringFromDate:todayTime];
    return dateTimeString;
}

+(NSString*)getTodayFullMin{
    NSDateFormatter * currentDateTime = [[NSDateFormatter alloc]init];
    NSDate *todayFormat = [NSDate dateWithTimeInterval:(0) sinceDate:[NSDate date]];
    [currentDateTime setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
    NSString * dateTimeString = [currentDateTime stringFromDate:todayFormat];
    return dateTimeString;
}

+(NSString*)dsTinhMienTrung
{
    NSString *str =@"Miền Trung#Daklak#Đà Nẵng#Đắc Nông#Gia Lai#Khánh Hòa#Kon Tum#Ninh Thuận#Phú Yên#Quảng Bình#Quảng Ngãi#Quảng Nam#Quảng Trị#Thừa Thiên Huế#Bình Định";
    return str;
}
+(NSString*)dsMaTinhMienTrung
{
    NSString *str =@"XSMT#XSDLK#XSDL#XSDNO#XSGL#XSKH#XSKT#XSNT#XSPY#XSQB#XSQNI#XSQNM#XSQT#XSTTH#XSBDH";
    return str;
}
+(NSString*)dsMaTinhMienNam
{
    NSString *str =@"XSMN#XSBDH#XSHCM#XSAG#XSBD#XSBL#XSBP#XSBT#XSCM#XSCT#XSDL#XSDN#XSDT#XSHG#XSKG#XSLA#XSST#XSTG#XSTN#XSTV#XSVL#XSVT";
    return str;
}
+(NSString*)dsTinhMienNam
{
    NSString *str =@"Miền Nam#Bình Thuận#Hồ Chí Minh#An Giang#Bình Dương#Bạc Liêu#Bình Phước#Bến Tre#Cà Mau#Cần Thơ#Đà Lạt#Đồng Nai#Đồng Tháp#Hậu Giang#Kiên Giang#Long An#Sóc Trăng#Tiền Giang#Tây Ninh#Trà Vinh#Vĩnh Long#Vũng Tàu";
    return str;
}
+(NSString*)dsTinhCaNuoc
{
    NSString *str =@"Miền Bắc#Daklak#Đà Nẵng#Đắc Nông#Gia Lai#Khánh Hòa#Kon Tum#Ninh Thuận#Phú Yên#Quảng Bình#Quảng Ngãi#Quảng Nam#Quảng Trị#Thừa Thiên Huế#Bình Định#Bình Thuận#Hồ Chí Minh#An Giang#Bình Dương#Bạc Liêu#Bình Phước#Bến Tre#Cà Mau#Cần Thơ#Đà Lạt#Đồng Nai#Đồng Tháp#Hậu Giang#Kiên Giang#Long An#Sóc Trăng#Tiền Giang#Tây Ninh#Trà Vinh#Vĩnh Long#Vũng Tàu";
    return str;
}
+(NSString*)dsMaTinhCaNuoc
{
    NSString *str =@"XSTD#XSDLK#XSDL#XSDNO#XSGL#XSKH#XSKT#XSNT#XSPY#XSQB#XSQNI#XSQNM#XSQT#XSTTH#XSBDH#XSBDH#XSHCM#XSAG#XSBD#XSBL#XSBP#XSBT#XSCM#XSCT#XSDL#XSDN#XSDT#XSHG#XSKG#XSLA#XSST#XSTG#XSTN#XSTV#XSVL#XSVT";
    return str;
}
SlaveServerConnect *slaveConnect;
+ (void) setSlaveConect:(SlaveServerConnect *) pin{
	slaveConnect=pin;
}
+ (SlaveServerConnect*)getSlaveConect
{
	return slaveConnect;
}




+(void)buttonXoSo:(UIButton*)btn
{
    [btn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [btn setContentVerticalAlignment:UIControlContentVerticalAlignmentTop];
    [btn setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 5.0f, 0.0f, 0.0f)];
    
    
    
    btn.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:15];
    
       btn.layer.borderColor = RGB(106, 0, 0).CGColor;
    btn.layer.borderWidth = 1;
    btn.layer.cornerRadius = 10;
   
}
+(BOOL)isViettel
{
    CTTelephonyNetworkInfo *netinfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [netinfo subscriberCellularProvider];
    NSLog(@"Carrier Name: %@", [carrier carrierName]);
     NSString*nhaMang =  [[carrier carrierName] lowercaseString];
    
    if ([nhaMang isEqual:@"viettel"]) {
      return  YES;
    }
    else
        return NO;
}

@end
