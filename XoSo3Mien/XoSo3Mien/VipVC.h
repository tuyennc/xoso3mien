//
//  VipVC.h
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VipVC : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *tittleVC;
@property (strong, nonatomic) IBOutlet UINavigationBar *navTu;


@property (strong, nonatomic) IBOutlet UIButton *btnNgay;


@property (strong, nonatomic) IBOutlet UIButton *btnSoiCau;
@property (strong, nonatomic) IBOutlet UIButton *btnCauNgay;
@property (strong, nonatomic) IBOutlet UIButton *hoiChuyenGia;
@property (strong, nonatomic) IBOutlet UIImageView *imgSelectBut;

@property (strong, nonatomic) IBOutlet UIView *viSoiCau;
@property (strong, nonatomic) IBOutlet UIView *viCauNgay;
@property (strong, nonatomic) IBOutlet UIView *viHoiChuyenGia;

@property (strong, nonatomic) IBOutlet UIButton *btnTinh2;

@property (strong, nonatomic) IBOutlet UIButton *btnTinh;
@property (strong, nonatomic) IBOutlet UITableView *tblTinh;
@property (strong, nonatomic) IBOutlet UITableView *tblTinh2;

@property (strong, nonatomic) IBOutlet UITableView *tblVip;
@property (strong, nonatomic) IBOutlet UITextField *tfBienDo;
@property (strong, nonatomic) IBOutlet UIButton *bntCau;
@property (strong, nonatomic) IBOutlet UIButton *btnSendSMS;
@property (strong, nonatomic) IBOutlet UIButton *btnSendSMSTK;

- (IBAction)sendSMS:(id)sender ;
- (IBAction)thongKeFrom3:(id)sender ;
@property (strong, nonatomic) IBOutlet UILabel *lblGiaCuoc1;
@property (strong, nonatomic) IBOutlet UILabel *lblNoData;


@end
