//
//  VipVC.m
//  XoSo3Mien
//
//  Created by Tuyen on 1/19/14.
//  Copyright (c) 2014 tuyennc. All rights reserved.
//

#import "VipVC.h"
#import "PopUpSoiCau.h"
#import "ChonLichSubView.h"
#import "CauNgayDetail.h"

@interface VipVC ()<PopUpSoiCauDelegate,SlaveDelegate,ChonLichDelegate,DangKyDelegate,LogInOutDelegate,AccountInforDelegate,UITextFieldDelegate>
{
    ECSlidingViewController  *slidingViewController;
    LogInOutVC   *logInOut;
     NSString *maVungChon;
    NSArray *arrTinh ;
    NSString *tenTinh;
    NSArray *arrMaTinh ;
     ASIHTTPRequest *request;
    int typeSoiCau ;
    BOOL isNotMB;
    NSMutableDictionary *dictSoicau;
    ChonLichSubView *chonLichView;
       NSString  *ngayChon;
    NSMutableDictionary *dictCauBD;
    NSMutableArray *arrButton;    
     DangKiVC     *dangKi;
    NSMutableArray *arrKetQuaBD;
    UIAlertView *alertNoData;
      AccountInfor   *accInfor;
}
@end

@implementation VipVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
      [self.slidingViewController resetTopView];
    self.navTu.topItem.title = @"";
    _tittleVC.text =@"VIP";
    _tittleVC.textAlignment = NSTextAlignmentCenter;
     _tittleVC.font = [UIFont fontWithName:XSFontCondensed size:20];
//    SlaveServerConnect *slaveConnect=[Utility getSlaveConect];
//    if (slaveConnect==NULL) {
//        slaveConnect=[SlaveServerConnect alloc];
//        [Utility setSlaveConect:slaveConnect];
//    }
//    slaveConnect.delegate = self;
//    NSString *requestContent=[NSString stringWithFormat:@"TKCAU_REQ|%@|02/13/2014|XSTD|10",[AppDelegate sharedDelegate].appID ];
// 
//     [slaveConnect mainconnect:requestContent];
  }

-(void)add100Button
{
    int x =0;
    int y =50;
    int z = 0;
    for (int i = 0; i<100; i++) {
       
     x= (i%10)*30+20;
      
        if (z%10==0) {
//            x=0;
            y=30+y;
        }
      z++;
        
        
        UILabel *view = [[UILabel alloc] initWithFrame:CGRectMake(x, y, 30, 30)];
        view.text = [NSString stringWithFormat:(i<10)?@"0%d":@"%d",i];
//        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:@"My Messages"];
//        [attString addAttribute:(NSString*)kCTUnderlineStyleAttributeName
//                          value:[NSNumber numberWithInt:kCTUnderlineStyleSingle]
//                          range:(NSRange){0,[attString length]}];
//        self.myMsgLBL.attributedText = attString;
        
        view.tag = i;
        view.textColor = [UIColor blueColor];
        view.userInteractionEnabled = YES;
        view.font = [UIFont fontWithName:XSFontCondensed size:17];
        view.textAlignment = NSTextAlignmentCenter;
        [_viCauNgay addSubview:view];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapNumber:)];
        view.hidden = YES;
        [view addGestureRecognizer:tap];
        [arrButton addObject:view];
        
    }
    for (int i=0 ;i<10;i++) {
        UILabel *view = [[UILabel alloc] initWithFrame:CGRectMake(0, 30*i+80, 30, 30)];
        view.text = [NSString stringWithFormat:@"Đ%d",i];
      view.textColor = RGB(31, 31, 31);
          view.textAlignment = NSTextAlignmentLeft;
        view.font = [UIFont fontWithName:XSFontCondensed size:17];
        view.textAlignment = NSTextAlignmentCenter;
        [_viCauNgay addSubview:view];
    }
    for (int i=0 ;i<10;i++) {
        UILabel *view = [[UILabel alloc] initWithFrame:CGRectMake(30*i+20,60 , 30, 30)];
        view.text = [NSString stringWithFormat:@"Đ%d",i];
        view.textColor = RGB(31, 31, 31);
        view.textAlignment = NSTextAlignmentCenter;
        view.font = [UIFont fontWithName:XSFontCondensed size:17];
        view.textAlignment = NSTextAlignmentCenter;
        [_viCauNgay addSubview:view];
    }
}
-(void)tapNumber:(UITapGestureRecognizer*)tap
{
//   XS_CAU_REQ |<appid>|<code>>|<biên_độ_cầu>||
    NSLog(@"%d",tap.view.tag);
    CauNgayDetail *scDetail = [[CauNgayDetail alloc] init];
    scDetail.capso =[NSString stringWithFormat:(tap.view.tag<10)?@"0%d":@"%d",tap.view.tag];
    scDetail.vitri = [dictCauBD objectForKey:scDetail.capso];
    scDetail.tenTinh =tenTinh;
    scDetail.songay = [_tfBienDo.text integerValue];
    
    scDetail.arrKetQua = arrKetQuaBD;
    [self.navigationController pushViewController:scDetail animated:YES];
}
- (void)viewDidLoad
{
    _tfBienDo.layer.borderColor = RGB(106, 0, 0).CGColor;
    _tfBienDo.layer.borderWidth = 1;
    _tfBienDo.layer.cornerRadius = 7 ;
    _tfBienDo.font = [UIFont fontWithName:XSFontCondensed size:17];
    _lblNoData.font = [UIFont fontWithName:XSFontCondensed size:15];
    
  
    [super viewDidLoad];
       _btnSoiCau.backgroundColor = RGB(181, 28, 28);
    
    arrKetQuaBD = [[NSMutableArray alloc] init];
    [Utility buttonXoSo:_btnTinh];
//    [Utility buttonXoSo:_bntCau];
    _bntCau.layer.cornerRadius =10;
    _bntCau.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:15];
    [_bntCau setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [_bntCau setContentVerticalAlignment:UIControlContentVerticalAlignmentTop];
    [_bntCau setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 17.0f, 0.0f, 0.0f)];
    
    [_btnTinh setTitle:@"+  Miền Bắc" forState:UIControlStateNormal];
    
    [Utility buttonXoSo:_btnTinh2];
    [_btnTinh2 setTitle:@"+  Miền Bắc" forState:UIControlStateNormal];
    
    
    dictCauBD = [[NSMutableDictionary alloc] init];
    arrMaTinh = [[Utility dsMaTinhCaNuoc] componentsSeparatedByString:@"#"];
    arrTinh = [[Utility dsTinhCaNuoc] componentsSeparatedByString:@"#"];
    maVungChon = @"XSTD";
   tenTinh = @"Miền Bắc";
    _btnSoiCau.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:17];
    _btnCauNgay.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:17];
    _hoiChuyenGia.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:17];
    // Do any additional setup after loading the view from its nib.
    UIImage *imgItem =[ Utility resizeImg:[UIImage imageNamed:@"iconmenu.png"] withSize:CGSizeMake(30, 30*30/50)] ;
    
    self.navTu.topItem.leftBarButtonItem = [UIBarButtonItem barItemWithImage:imgItem target:self action:@selector(leftBtnClicked:)];
    if (IS_OS_7_OR_LATER) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        [self setNeedsStatusBarAppearanceUpdate];
        self.navTu.barTintColor = RGB(106, 0, 0);
         [_tblTinh2 setSeparatorInset:UIEdgeInsetsZero];
         [_tblTinh setSeparatorInset:UIEdgeInsetsZero];
    }
    else
    {
        self.navTu.TintColor = RGB(106, 0, 0);
    }
    
    _tblTinh2.layer.cornerRadius = 7;
    _tblTinh.layer.cornerRadius = 7;
    
    [_btnNgay setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [_btnNgay setContentVerticalAlignment:UIControlContentVerticalAlignmentTop];
    [_btnNgay setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 5.0f, 0.0f, 0.0f)];
    
    [_btnNgay setTitle:[NSString stringWithFormat:@"+   %@", [Utility getToday]] forState:UIControlStateNormal];
    _btnNgay.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:17];
    _btnNgay.layer.borderColor = RGB(106, 0, 0).CGColor;
    _btnNgay.layer.borderWidth = 1;
    _btnNgay.layer.cornerRadius = 10;
    
    
    
    //    ecsliding
      [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    slidingViewController = [[ECSlidingViewController alloc] init];
    MenuVC *menuVC   = [[MenuVC alloc] initWithNibName:@"MenuVC" bundle:nil];
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    self.slidingViewController.underLeftViewController  = menuVC;
    
    
    self.tblVip.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 1.0f)] ;
    self.tblVip.tableHeaderView.backgroundColor = [UIColor lightGrayColor];
    self.tblVip.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 1.0f)] ;
    self.tblVip.tableFooterView.backgroundColor = [UIColor lightGrayColor];
    //    self.tblMenu.tableFooterView.hidden = YES;
    
    if (IS_OS_7_OR_LATER) {
        [self.tblVip setSeparatorInset:UIEdgeInsetsZero];
        UIView *viewSta = [[UIView alloc] initWithFrame:CGRectMake(0, -20, 320, 20)];
        viewSta.backgroundColor = RGB(106, 0, 0);
        
        [self.view addSubview:viewSta];
    }
    self.tblVip.separatorColor =  [UIColor lightGrayColor];
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ChonLichSubView" owner:self options:nil];
    chonLichView = (ChonLichSubView *)[nib objectAtIndex:0];
    chonLichView.frame = CGRectMake(0, 35, 210, 350);
    chonLichView.userInteractionEnabled = YES;

    chonLichView.delegate = self;
    chonLichView.hidden = YES;
    arrButton = [[NSMutableArray alloc] init];
    [self add100Button];
//    [_viCauNgay addSubview:chonLichView];
    [_btnSendSMS setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [_btnSendSMS setContentVerticalAlignment:UIControlContentVerticalAlignmentTop];
    [_btnSendSMS setTitleEdgeInsets:UIEdgeInsetsMake(6.0f, 30.0f, 0.0f, 0.0f)];
    
    
    _btnSendSMS.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:13];
    
    [_btnSendSMSTK setTitle:@"Thống kê" forState:UIControlStateNormal];
    [_btnSendSMSTK setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [_btnSendSMSTK setContentVerticalAlignment:UIControlContentVerticalAlignmentTop];
    [_btnSendSMSTK setTitleEdgeInsets:UIEdgeInsetsMake(10.0f, 30.0f, 0.0f, 0.0f)];
    
    [_btnSendSMS setTitle: [NSString stringWithFormat:@"Nhận KQXS %@",@"Miền Bắc"] forState:UIControlStateNormal];
    _btnSendSMSTK.titleLabel.font = [UIFont fontWithName:XSFontCondensed size:17];
     _lblGiaCuoc1.text = [NSString stringWithFormat:@"%@ (1.000đ/ngày)",[Utility getToday]];
     _lblGiaCuoc1.font = [UIFont fontWithName:XSFontCondensed size:13];

    
//    login
    UIButton *btnSelectDate=[UIButton buttonWithType:UIButtonTypeCustom];
    btnSelectDate.frame = CGRectMake(0, 0,30,30);
    
    if ([[AppDelegate sharedDelegate] isLogin]) {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"Logged.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    else
    {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"LogInOut.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    [btnSelectDate addTarget:self action:@selector(pushViewLogIO) forControlEvents:UIControlEventTouchUpInside];
    self.navTu.topItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSelectDate];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateImageLogin)
                                                 name:@"UPDATE_IMAGE_LOGIN"
                                               object:nil];
    
    
    


}
- (IBAction)leftBtnClicked:(id)sender
{
    [[[UIApplication sharedApplication]keyWindow]endEditing:YES];
    [self.slidingViewController anchorTopViewTo:ECRight];
}
-(void)pushViewLogIO
{
    if ([[AppDelegate sharedDelegate ] isLogin]==NO) {
        logInOut  = [[LogInOutVC alloc] initWithNibName:@"LogInOutVC" bundle:nil]  ;
        logInOut.delegate = self;
        [self presentPopupViewController:logInOut animationType:MJPopupViewAnimationFade];
        
    }
    else
    {
        accInfor  = [[AccountInfor alloc] initWithNibName:@"AccountInfor" bundle:nil]  ;
        accInfor.delegate = self;
        [self presentPopupViewController:accInfor animationType:MJPopupViewAnimationFade];
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark loginout delete

-(void)dangKi
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    dangKi  = [[DangKiVC alloc] initWithNibName:@"DangKiVC" bundle:nil]  ;
   dangKi.delegate = self;
    [self presentPopupViewController:dangKi animationType:MJPopupViewAnimationFade];
    
}
-(void)closePopUp
{
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
    
}
-(void)updateImageLogin
{
    UIButton *btnSelectDate=[UIButton buttonWithType:UIButtonTypeCustom];
    btnSelectDate.frame = CGRectMake(0, 0,30,30);
    
    if ([[AppDelegate sharedDelegate] isLogin]) {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"Logged.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    else
    {
        [btnSelectDate setImage:[Utility resizeImg:[UIImage imageNamed:@"LogInOut.png"] withSize:CGSizeMake(36, 36)] forState:UIControlStateNormal];
    }
    [btnSelectDate addTarget:self action:@selector(pushViewLogIO) forControlEvents:UIControlEventTouchUpInside];
    self.navTu.topItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSelectDate];
}

#pragma mark
#pragma mark chonLich && chonTinh delegate

- (void) chonNgay:(NSString *)date
{
    
    [_btnNgay setTitle:[NSString stringWithFormat:@"+   %@",date] forState:UIControlStateNormal];
    
    ngayChon = date;
    chonLichView.hidden = YES;
    
}

#pragma mark
#pragma mark - Slave delegate

-(void)data_XS_CAU_RESP_Recieved:(NSString *)resuft
{
    
    BOOL isAdd = YES;
    for (NSString *strCompare in arrKetQuaBD) {
       
        if ([resuft isEqualToString:strCompare]) {
            isAdd = NO;
            break;
        }
           }
    if (isAdd||arrKetQuaBD.count==0) {
        [arrKetQuaBD addObject:resuft];
        isAdd = NO;
    }
    NSLog(@"maiyeu %d",arrKetQuaBD.count);
//    NSArray *ketQua = [resuft componentsSeparatedByString:@"|"];
//    
//    
//    
//    NSDateFormatter* df = [[NSDateFormatter alloc] init];
//    [df setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
//    NSString *today = [ketQua objectAtIndex:<#(NSUInteger)#>];
//    
//    NSDate *date2 = [df  dateFromString:today];
//    NSDate *date1  = [df  dateFromString:showDate];
//    NSTimeInterval diff = [date2 timeIntervalSinceDate:date1];
}

-(void)data_TKCAU_RESP_Recieved:(NSString *)resuft
{
//    TKCAU_RESP|0|14/02/2014 19:58:44|XSTD|10|26:61:93|90:28:79
    
    [self.view endEditing:YES];
 
    [dictCauBD removeAllObjects];
    NSArray *arrTemp = [resuft componentsSeparatedByString:@"|"];
    
    NSLog(@"tuyennc recc %@, so luong %d",resuft,arrTemp.count-4);
    for (UILabel *lbl in arrButton) {
        lbl.hidden = YES;
    }
    if ([[arrTemp objectAtIndex:1] isEqualToString:@"0"]) {
       _lblNoData.hidden = YES;
        for (int i = 5; i<arrTemp.count; i++) {
            
            NSString *strTepm =[arrTemp objectAtIndex:i];
            NSArray *strCapso = [strTepm componentsSeparatedByString:@":"];
            NSString *strfist =[strCapso objectAtIndex:0];
            [dictCauBD setObject:[strTepm substringFromIndex:strfist.length+1] forKey:strfist];
        }

//        float orgY = 100;
//        fo
        
       
        for (NSString*str in dictCauBD.allKeys) {
            for (UILabel *lbl in arrButton) {
                if ([str intValue] == lbl.tag) {
                    lbl.hidden = NO;
                }
            }
        }
     
    }
    else
    {
        _lblNoData.hidden = NO;
//        if (alertNoData==nil) {
//              alertNoData = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Không có dữ liệu cho yêu cầu này" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        }
//       
//        
//
//                     [alertNoData show];
//        
    }
   }

#pragma mark
#pragma mark - IBOulet
- (IBAction)showNgay:(id)sender {
    chonLichView.hidden = NO;

    _tblTinh2.hidden = YES;
}

- (IBAction)showTInh:(id)sender {
       _tblTinh.hidden = NO;
    
}
- (IBAction)showTInh2:(id)sender {
     _tblTinh2.hidden = NO;
    chonLichView.hidden = YES;
    [_viCauNgay bringSubviewToFront:_tblTinh2];
}

- (IBAction)soiCau:(id)sender {
    
    
    _btnSoiCau.backgroundColor = RGB(181, 28, 28);
    _btnCauNgay.backgroundColor =RGB(106, 0, 0);
    _imgSelectBut.frame = CGRectMake(71, 0, 9, 9);
    
//    _btnSoiCau.backgroundColor = RGB(181, 28, 28);
//    _btnCauNgay.backgroundColor = RGB(106, 0, 0);
//    _hoiChuyenGia.backgroundColor = RGB(106, 0, 0);
//    _imgSelectBut.frame = CGRectMake(45, 0, 11, 11);
    
    _viSoiCau.hidden = NO;
    _viCauNgay.hidden  = YES;
    _viHoiChuyenGia.hidden = YES;
    
}

- (IBAction)cauNgay:(id)sender {
    
    [self.view endEditing:YES];
    _btnSoiCau.backgroundColor = RGB(106, 0, 0);
    _btnCauNgay.backgroundColor = RGB(181, 28, 28);
    _hoiChuyenGia.backgroundColor = RGB(106, 0, 0);
  _imgSelectBut.frame = CGRectMake(236, 0, 9, 9);
    
    _viSoiCau.hidden = YES;
    _viCauNgay.hidden  = NO;
    _viHoiChuyenGia.hidden = YES;
}


- (IBAction)cauTheoBienDo:(id)sender {
    if ([_tfBienDo.text integerValue]>2 && [_tfBienDo.text integerValue]<15) {
        [dictCauBD removeAllObjects];
        
        [arrKetQuaBD removeAllObjects];
        for (UILabel *lbl in arrButton) {
            lbl.hidden = YES;
        }
        
        SlaveServerConnect *slaveConnect=[Utility getSlaveConect];
        if (slaveConnect==NULL) {
            slaveConnect=[SlaveServerConnect alloc];
            [Utility setSlaveConect:slaveConnect];
        }
        slaveConnect.delegate = self;
        NSString *requestContent=[NSString stringWithFormat:@"TKCAU2_REQ|%@|%@|%@|%@",[AppDelegate sharedDelegate].appID,[Utility getTodayFullMin],maVungChon,_tfBienDo.text ];
        
        [slaveConnect mainconnect:requestContent];
        
        [self getKQTheoDB];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Biên độ cầu nên lớn hơn 2" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
        [alert show];
    }
}

- (void)getKQTheoDB {
//    XS_CAU_REQ |<appid>|<code>>|<biên_độ_cầu>||

        SlaveServerConnect *slaveConnect=[Utility getSlaveConect];
        if (slaveConnect==NULL) {
            slaveConnect=[SlaveServerConnect alloc];
            [Utility setSlaveConect:slaveConnect];
        }
        slaveConnect.delegate = self;
        NSString *requestContent=[NSString stringWithFormat:@"XS_CAU_REQ|%@|%@|%@",[AppDelegate sharedDelegate].appID,maVungChon,_tfBienDo.text ];
        
        [slaveConnect mainconnect:requestContent];
  }


- (IBAction)hoiChuyenGia:(id)sender {
    _btnSoiCau.backgroundColor = RGB(106, 0, 0);
    _btnCauNgay.backgroundColor = RGB(106, 0, 0);
    _hoiChuyenGia.backgroundColor = RGB(181, 28, 28);
    _imgSelectBut.frame = CGRectMake(261, 0, 11, 11);
    
    _viSoiCau.hidden = YES;
    _viCauNgay.hidden  = YES;
    _viHoiChuyenGia.hidden = NO;
    
//     |1368|15/03/2013 16:50:24||
   

}
#pragma mark
#pragma mark Request ket qua
- (void)requestSoiCaue:(NSString*)nameSoiCau withRegion:(NSString*)region
{
    [[AppDelegate sharedDelegate].hudRequest show:YES];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://appsrv.sms.vn/app/xs?action=%@&code=%@",nameSoiCau,maVungChon]];
    NSLog(@"requset :%@",url);
    request = [ASIHTTPRequest requestWithURL:url];
    
    [request setCompletionBlock:^{
        
        
        
        [[AppDelegate sharedDelegate].hudRequest hide:YES];
        // Use when fetching text data
        NSString *responseString = [request responseString];
        NSLog(@"%@",responseString);
        //          NSData *responseData = [request responseData];
        NSDictionary *responseDict = [[responseString JSONValue] objectForKey:@"items"];
        if (![responseDict isKindOfClass:[NSString class]]) {
           
            PopUpSoiCau *popSC = [[PopUpSoiCau alloc] initWithNibName:@"PopUpSoiCau" bundle:nil];
            popSC.dictSoiCau = responseDict;
            popSC.typeSoiCau = typeSoiCau;
            popSC.tenTinh = tenTinh;
            popSC.maTinh = maVungChon;
            popSC.delegate = self;
             [self presentPopupViewController:popSC animationType:MJPopupViewAnimationFade];
         
        }
        else
        {
            
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Chưa có dữ liệu cho ngày này" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
            [alert show];
        }
        
    }];
    [request setFailedBlock:^{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Không thể kết nối" delegate:self cancelButtonTitle:@"Kết nối lại" otherButtonTitles:@"Huỷ", nil] ;
        [alert show];
        //        [UIHelper showMessage:@"Không thể kết nối" withTitle:@""];
    }];
    [request startAsynchronous];
    
}


-(void)dongPop
{
    [self.navigationController dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

#pragma mark
#pragma mark - tableview delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == _tblVip) {
        if ([maVungChon isEqualToString:@"XSTD"]) {

            return 9;
        }
        return 4;
    }
    else if (tableView == _tblTinh || tableView == _tblTinh2)
    {
        return arrMaTinh.count;
    }
    else
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    return 40;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor whiteColor];
    }
    [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    if (tableView==_tblVip) {
        
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 20, 20)];
        imgView.contentMode = UIViewContentModeScaleAspectFit;
        UIImage *imgItem =[ Utility resizeImg:[UIImage imageNamed:@"mn_chonve.png"] withSize:CGSizeMake(20, 20)] ;
        imgView.image = imgItem;
        
        
        UIImage *imgItem2 =[ Utility resizeImg:[UIImage imageNamed:@"menu-arrow.png"] withSize:CGSizeMake(7, 18)] ;
        UIImageView *imageView = [[UIImageView alloc] initWithImage:imgItem2];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        cell.accessoryView = imageView;
        
        UILabel *lblVeSo = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 200, 40)];
        lblVeSo.backgroundColor = [UIColor clearColor];
        lblVeSo.textColor = RGB(77, 77, 77);
        lblVeSo.textAlignment = NSTextAlignmentLeft;
        lblVeSo.font = [UIFont fontWithName:XSFontCondensed size:17];

        if ([maVungChon isEqualToString:@"XSTD"]) {
            
        switch (indexPath.row) {
            case 0:
                lblVeSo.text = @"Soi Cầu";
                break;
            case 1:
                lblVeSo.text = @"Cặp Số Đẹp";
                break;
            case 2:
                lblVeSo.text = @"Lô Bạch Thủ";
                break;
            case 3:
                lblVeSo.text = @"Lô Xiên";
                break;
            case 4:
                lblVeSo.text = @"Lô Xiên Quay";
                break;
            case 5:
                lblVeSo.text = @"Lô Âm Dương";
                break;
            case 6:
                lblVeSo.text = @"Lô Ngũ Hành";
                break;
            case 7:
                lblVeSo.text = @"Dàn Đặc Biệt Ngày";
                break;
            case 8:
                lblVeSo.text = @"Dàn Đặc Biệt Tuần";
                break;
            default:
                break;
        }
        
      
        }
        else
        {
            switch (indexPath.row) {
                case 0:
                    lblVeSo.text = @"Soi Cầu";
                    break;
                case 1:
                    lblVeSo.text = @"Cặp Số Đẹp";
                    break;
                case 2:
                    lblVeSo.text = @"Lô Bạch Thủ";
                    break;
                case 3:
                    lblVeSo.text = @"Lô Xiên";
                    break;
                default:
                    break;
            }
        }
        [cell.contentView addSubview:lblVeSo];
        [cell.contentView addSubview:imgView];
    }
    else if(tableView == _tblTinh || tableView == _tblTinh2)
    {
//        if (tableView==_tblTinh) {
            UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, 92, 40)];
            lbl.text = [arrTinh objectAtIndex:indexPath.row];
            lbl.font = [UIFont fontWithName:XSFontCondensed size:17];
            lbl.textColor = [UIColor whiteColor];
            cell.backgroundColor = RGB(106, 0, 0);
        UIImage *img = [Utility resizeImg:[UIImage imageNamed:@"arr_chontinh.png"] withSize:CGSizeMake(8, 12)];
        cell.imageView.image = img;
            [cell.contentView addSubview:lbl];
//        }

    }
   
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tblTinh || tableView == _tblTinh2) {
        maVungChon = [arrMaTinh objectAtIndex:indexPath.row];
        if (![maVungChon isEqualToString:@"XSTD"]) {
            isNotMB = YES;
        }
        else
            isNotMB = NO;
        tenTinh = [arrTinh objectAtIndex:indexPath.row];
        _tblTinh.hidden = YES;
        _tblTinh2.hidden = YES;
        [_btnTinh setTitle:[NSString stringWithFormat:@"+  %@",[arrTinh objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
         [_btnTinh2 setTitle:[NSString stringWithFormat:@"+  %@",[arrTinh objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
       
        [_tblVip reloadData];
            [_btnSendSMS setTitle: [NSString stringWithFormat:@"Nhận KQXS %@",tenTinh] forState:UIControlStateNormal];
        if ([maVungChon isEqualToString:@"XSBDH"] || [maVungChon isEqualToString:@"XSTD"]) {
            _lblGiaCuoc1.text = [NSString stringWithFormat:@"%@ (1.000đ/ngày)",[Utility getToday]];
            
        }
        else
        {
            _lblGiaCuoc1.text = [NSString stringWithFormat:@"%@ (2.000đ/ngày)",[Utility getToday]];
        }

    }
    else if (tableView == _tblVip)
    {
        _tblTinh.hidden =YES;
        typeSoiCau = indexPath.row;
        NSString *type = @"";
        switch (indexPath.row) {
            case 0:
                type = @"SC";
                break;
            case 1:
                type = @"CSD";
                break;
            case 2:
                type = @"LBT";
                break;
            case 3:
                type = @"LX";
                break;
            case 4:
                type = @"LTXQ";
                break;
            case 5:
                type = @"LTAD";
                break;
            case 6:
                type = @"LTNH";
                break;
            case 7:
                type = @"DDBN";
                break;
            case 8:
                type = @"DDBT";
                break;
                
                
                
            default:
                break;
        }
          [self requestSoiCaue:type withRegion:maVungChon];
    }
   
    
    
}

- (IBAction)sendSMS:(id)sender {
    
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
    controller.delegate = self;
    
    if([MFMessageComposeViewController canSendText])
    {
        NSString *toNumber = nil;
        if ([Utility isViettel]) {
            toNumber = DS_VIETTEL_XS;
            if ([maVungChon isEqualToString:@"XSTD"]) {
                controller.body = [NSString stringWithFormat:@"%@",@"XSMB"];
            }
            else
                controller.body = [NSString stringWithFormat:@"%@",maVungChon];
            controller.recipients = [NSArray arrayWithObjects:toNumber, nil];
            
        }
        else
        {
            toNumber = DS_NOT_VIETTEL;
            NSString *strOr = [Utility dsMaTinhMienTrung];
            if ([strOr rangeOfString:maVungChon].location!=NSNotFound) {
                controller.body = [NSString stringWithFormat:@"DK %@",@"XSMT"];
                
            }
            else
            {
                controller.body = [NSString stringWithFormat:@"DK %@",@"XSMN"];
            }
            if ([maVungChon isEqualToString:@"XSTD"]) {
                controller.body = [NSString stringWithFormat:@"DK %@",@"XSMB"];
            }
            if ([maVungChon isEqualToString:@"XSBDH"]) {
                controller.body = [NSString stringWithFormat:@"DK %@",@"XSBDH"];
            }
            controller.recipients = [NSArray arrayWithObjects:toNumber, nil];
            
        }
        controller.messageComposeDelegate = self;
        [self presentModalViewController:controller animated:YES];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
	switch (result) {
		case MessageComposeResultCancelled:
			NSLog(@"Cancelled");
			break;
		case MessageComposeResultFailed:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Không gửi được tin nhắn" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
        }
			break;
		case MessageComposeResultSent:
        {UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thành công" message:@"Gửi tin nhắn thành công" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
        }
			break;
		default:
			break;
	}
    
	[self dismissModalViewControllerAnimated:YES];
}
- (void)sendSMSXIEN {
    
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
    controller.delegate = self;
    
    if([MFMessageComposeViewController canSendText])
    {
        NSString *toNumber = nil;
        if ([Utility isViettel]) {
            toNumber = DS_VIETTEL_CX;
            if ([maVungChon isEqualToString:@"XSTD"]) {
                controller.body = [NSString stringWithFormat:@"%@",@"XIEN"];
            }
            else
            {
                NSString*matinh = maVungChon;
                
                controller.body = [matinh stringByReplacingOccurrencesOfString:@"XS" withString:@"XIEN "];
            }
            controller.recipients = [NSArray arrayWithObjects:toNumber, nil];
            
        }
        else
        {
            toNumber = DS_NOT_VIETTEL;
            NSString *strOr = [Utility dsMaTinhMienTrung];
            if ([strOr rangeOfString:maVungChon].location!=NSNotFound) {
                controller.body = [NSString stringWithFormat:@"DK TK %@",@"MT"];
                
            }
            else
            {
                controller.body = [NSString stringWithFormat:@"DK %@",@"MN"];
            }
            if ([maVungChon isEqualToString:@"XSTD"]) {
                controller.body = [NSString stringWithFormat:@"DK %@",@"MB"];
            }
            controller.recipients = [NSArray arrayWithObjects:toNumber, nil];
            
            
        }
        controller.messageComposeDelegate = self;
        [self presentModalViewController:controller animated:YES];
    }
}
#pragma ---
#pragma mark

- (IBAction)thongKeFrom3:(id)sender {
    
    ThongKeVC *mainView = [[ThongKeVC alloc] initWithNibName:@"ThongKeVC" bundle:nil];            CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = mainView;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    _tblTinh.hidden =YES;
    _tblTinh2.hidden =YES;
    [self.viCauNgay endEditing:YES];
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    _tblTinh2.hidden = YES;
    return YES;
}

@end
